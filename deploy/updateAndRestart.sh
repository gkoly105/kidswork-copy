echo "create db hard link"
cd /home/ubuntu/kw/
ln persist/db.sqlite3 deploy/backend/db.sqlite3
ln -s /home/ubuntu/kw/persist/media deploy/backend/media 
cd deploy/backend/ 
mkdir log/
echo "migrate"
python3 -u manage.py migrate &> log/migration_log.txt

netstat -tulpn | grep "0.0.0.0:8000" || (
    echo "start server"
    python3 -u manage.py runserver 0.0.0.0:8000 &> log/run_log.txt &
)

echo "deploy finished"