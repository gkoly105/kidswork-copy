import datetime

from django.core.management import BaseCommand
from django.db.models import Q
from django.db.models.functions import Now
from django.utils import timezone

from api.models import Task, Notif


def repeat_tasks():
    tasks = Task.objects.filter(repeat__isnull=False)
    for task in tasks:
        delta = datetime.timedelta(days=task.repeat)
        repeat_time = task.creation_time + delta
        now = timezone.now()
        if repeat_time < now:

            new_task = Task(title=task.title,
                            desc=task.desc,
                            child=task.child,
                            parent=task.parent,
                            reward=task.reward,
                            require_image=task.require_image,
                            repeat=task.repeat,
                            notify_repeat=task.notify_repeat)
            if task.time_limit:
                new_task.time_limit = task.time_limit + delta
            if task.notify_time:
                new_task.notify_time = task.notify_time + delta

            new_task.save()

            task.repeat = None
            task.save()


def repeat_notifs():
    tasks = Task.objects.filter(notify_time__lt=Now(),
                                finish_time__isnull=True,
                                confirm_time__isnull=True)\
                        .filter(Q(time_limit__gt=Now())
                                | Q(time_limit__isnull=True))
    for task in tasks:
        if task.notify_repeat:
            delta = datetime.timedelta(days=task.notify_repeat)
            task.notify_time += delta
        else:
            task.notify_time = None

        task.save()

        Notif.objects.create(type=Notif.Type.C_REMIND_TASK,
                             user_from=task.parent.user,
                             user_to=task.child.user,
                             data={'task_id': task.id})


def expired_tasks():
    tasks = Task.objects.filter(time_limit__lt=Now(),
                                finish_time__isnull=True,
                                confirm_time__isnull=True)
    for task in tasks:
        task_data = {'title': task.title}
        notifs = Notif.objects.filter(type=Notif.Type.TASK_EXPIRED,
                                      data={'task_id': task.id,
                                            'task': task_data})
        if not notifs.exists():


            Notif.objects.create(type=Notif.Type.TASK_EXPIRED,
                                 user_from=task.parent.user,
                                 user_to=task.child.user,
                                 data={'task_id': task.id,
                                       'task': task_data})

            Notif.objects.create(type=Notif.Type.TASK_EXPIRED,
                                 user_from=task.child.user,
                                 user_to=task.parent.user,
                                 data={'task_id': task.id,
                                       'task': task_data})


class Command(BaseCommand):

    def handle(self, *args, **options):
        print('Start kw worker')
        repeat_tasks()
        repeat_notifs()
        expired_tasks()
        print('kw worker done')
