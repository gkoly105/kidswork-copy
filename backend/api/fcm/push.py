import os

import firebase_admin
from firebase_admin import credentials, messaging

from backend.settings import BASE_DIR

cred = credentials.Certificate(os.path.join(BASE_DIR, "api/fcm/fcm_creds.json"))
fcm = firebase_admin.initialize_app(cred)
