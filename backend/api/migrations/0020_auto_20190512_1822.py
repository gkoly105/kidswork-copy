# Generated by Django 2.1.7 on 2019-05-12 18:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0019_auto_20190507_1255'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gift',
            name='confirmed',
        ),
        migrations.AddField(
            model_name='gift',
            name='confirm_time',
            field=models.DateTimeField(default=None, null=True),
        ),
    ]
