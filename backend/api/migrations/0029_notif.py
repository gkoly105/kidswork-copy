# Generated by Django 2.1.7 on 2019-05-27 23:07

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0028_gift_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notif',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.SmallIntegerField()),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('read', models.BooleanField(default=False)),
                ('data', jsonfield.fields.JSONField(default=dict)),
                ('user_from', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifs_from', to=settings.AUTH_USER_MODEL)),
                ('user_to', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifs_to', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
