# Generated by Django 2.1.7 on 2019-04-29 20:26

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0016_auto_20190427_2008'),
    ]

    operations = [
        migrations.AddField(
            model_name='child',
            name='birthdate',
            field=models.DateField(default=datetime.datetime(2019, 4, 29, 20, 26, 9, 39918, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parent',
            name='birthdate',
            field=models.DateField(default=datetime.datetime(2019, 4, 29, 20, 26, 23, 891401, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
