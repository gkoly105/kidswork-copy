from PIL import Image
import time


def resize_image(image_file, dest_file=None, max_size=1024):
    img = Image.open(image_file)
    w, h = img.size

    if w > max_size or h > max_size:
        if w > h:
            img = img.resize((max_size, int(h*(max_size/w))))
        else:
            img = img.resize((int(w*max_size/h), max_size))

        if dest_file:
            img.save(dest_file)
        else:
            img.save(image_file)


def resize_avatar(image_file, dest_file=None):
    img = Image.open(image_file)
    w, h = img.size
    # print(img.size)
    # print(img.format)
    if w != h:
        if w < h:
            left = 0
            top = int(h/2 - w/2)
            right = w
            bottom = int(h/2 + w/2)
        else:
            left = int(w/2 - h/2)
            top = 0
            right = int(w/2 + h/2)
            bottom = h
        img = img.crop((left, top, right, bottom))

        if dest_file:
            img.save(dest_file)
            resize_image(dest_file)
        else:
            img.save(image_file)
            resize_image(image_file)


if __name__ == "__main__":
    t = time.time()
    resize_image("image1.jpg", max_size=128)
    print(time.time()-t)
