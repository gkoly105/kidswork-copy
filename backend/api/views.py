import json
import datetime

from django import forms
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db.models.functions import Now
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
import phonenumbers

from backend.settings import EMAIL_HOST_USER, SUPPORT_EMAIL, API_INFO
from .image_resizing.main import resize_avatar
from api.models import Parent, Task, Gift, Child, Message, Q, ParentChild, DisplayPrefs, Gender, Notif, get_user_person, \
    random_sms_token, UserToken, TempImage, SubType, Sub
from .sms.main import send_sms_code

ISO_MASK = "%Y-%m-%dT%H:%M:%S.%fZ"


class API_ERRORS:
    NOTFOUND = 'Not found'
    METHOD = 'Invalid method'
    FORMAT = 'Invalid format'
    ARGS = 'Invalid args'
    CREDS = 'Invalid credentials'
    AUTH = 'Auth required'
    PERM = 'Permission denied'


def index(request):
    return HttpResponse("Welcome to the API!")


# @csrf_exempt
# def login_view(request):
#     error = ''
#     if request.method == 'POST':
#         data = json.loads(request.body)
#         if 'login' in data and 'password' in data:
#             user = authenticate(request,
#                                 username=data['login'],
#                                 password=data['password'])
#             if user is not None:
#                 login(request, user)
#                 return JsonResponse({'status': 'ok'})
#             else:
#                 error = 'Invalid credentials'
#         else:
#             error = 'Invalid args'
#     else:
#         error = 'Invalid method'
#
#     return JsonResponse({''})


@method_decorator(csrf_exempt, 'dispatch')
class JsonApiView(View):
    http_method_names = ['get', 'post']
    data_fields = []
    auth_required = False

    def dispatch(self, request, *args, **kwargs):
        if request.method.lower() not in self.http_method_names:
            return self.set_error(API_ERRORS.METHOD)
        return self.check_auth(request)

    def check_auth(self, request):
        if self.auth_required and not request.user.is_authenticated:
            return self.set_error(API_ERRORS.AUTH)
        return self.check_args(request)

    def check_args(self, request):
        if request.method != 'GET':
            try:
                data = json.loads(request.body)
                if self.data_fields is not None:
                    for arg in self.data_fields:
                        if arg not in data:
                            return self.set_error(API_ERRORS.ARGS)
            except json.JSONDecodeError:
                return self.set_error(API_ERRORS.FORMAT)
        else:
            data = None
        return self.process(request, data)

    def process(self, request, data):
        return self.set_result()

    def set_result(self, data=None):
        return JsonResponse({'status': 'ok', 'data': data})

    def set_error(self, msg, data=None):
        return JsonResponse({'status': 'error', 'reason': msg, 'data': data})


class LoginView(JsonApiView):
    http_method_names = ['post']
    args = ['login', 'password']

    def process(self, request, data):
        user = authenticate(request,
                            username=get_phone(data['login']),
                            password=data['password'])

        if user is not None:
            if Parent.objects.filter(user=user).exists():
                parent = Parent.objects.filter(user=user).first()
                if parent.confirmed:
                    login(request, user)
                    return self.set_result({'role': 'parent'})
            elif Child.objects.filter(user=user).exists():
                child = Child.objects.filter(user=user).first()
                if child.confirmed:
                    login(request, user)
                    return self.set_result({'role': 'child'})
        return self.set_error(API_ERRORS.CREDS)


class ApiInfo(JsonApiView):
    http_method_names = ['get']

    def process(self, request, data):
        return self.set_result(API_INFO)


class LogoutView(JsonApiView):
    def process(self, request, data):
        logout(request)
        return self.set_result()


class CheckAuth(JsonApiView):
    auth_required = True

    def process(self, request, data):
        if Parent.objects.filter(user=request.user).exists():
            parent = Parent.objects.filter(user=request.user).first()
            if not parent.confirmed:
                return self.set_error(API_ERRORS.AUTH)
            return self.set_result({'role': 'parent'})
        if Child.objects.filter(user=request.user).exists():
            child = Child.objects.filter(user=request.user).first()
            if not child.confirmed:
                return self.set_error(API_ERRORS.AUTH)
            return self.set_result({'role': 'child'})
        logout(request)
        return self.set_error(API_ERRORS.AUTH)


class GetProfile(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'parent'):
            res = {
                'role': 'parent',
                'user': user.parent.to_dict_profile(),
            }
            return self.set_result(res)
        if hasattr(user, 'child'):
            res = {
                'role': 'child',
                'user': user.child.to_dict_profile(),
            }
            return self.set_result(res)
        else:
            return self.set_error(API_ERRORS.PERM)


class ChildList(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        parents = Parent.objects.filter(user=request.user)
        if parents.count():
            parent = parents[0]
            child_list = parent.get_child_list()
            return self.set_result(child_list)
        return self.set_error(API_ERRORS.PERM)


class ParentList(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        children = Child.objects.filter(user=request.user)
        if children.count():
            child = children[0]
            parent_list = child.get_parent_list()
            return self.set_result(parent_list)
        return self.set_error(API_ERRORS.PERM)


class AddTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['title', 'child_id', 'reward', 'require_image']

    def process(self, request, data):
        if not hasattr(request.user, 'parent'):
            return self.set_error(API_ERRORS.PERM)
        parent = request.user.parent

        try:
            reward = int(data['reward'])
            if reward <= 0:
                raise ValueError()
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        time_limit_str = data.get('time_limit', None)
        if time_limit_str:
            try:
                time_limit = datetime.datetime.strptime(time_limit_str, "%Y-%m-%dT%H:%M:%S.%fZ")
            except ValueError:
                return self.set_error(API_ERRORS.FORMAT)
        else:
            time_limit = None

        notify_time_str = data.get('notify_time', None)
        if notify_time_str:
            try:
                notify_time = datetime.datetime.strptime(notify_time_str, "%Y-%m-%dT%H:%M:%S.%fZ")
            except ValueError:
                return self.set_error(API_ERRORS.FORMAT)
        else:
            notify_time = None

        notify_repeat = data.get('notify_repeat', None)
        if notify_repeat is not None and notify_repeat <= 0:
            return self.set_error(API_ERRORS.FORMAT)

        repeat = data.get('repeat', None)
        if repeat is not None and repeat <= 0:
            return self.set_error(API_ERRORS.FORMAT)

        try:
            child = Child.objects.get(id=data['child_id'])
        except Child.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        task = Task(title=data['title'],
                    desc=data.get('desc', ''),
                    child=child,
                    parent=parent,
                    reward=reward,
                    require_image=data['require_image'],
                    time_limit=time_limit,
                    repeat=repeat,
                    notify_time=notify_time,
                    notify_repeat=notify_repeat)
        task.save()
        Notif.objects.create(type=Notif.Type.C_ADD_TASK,
                             user_from=parent.user,
                             user_to=child.user,
                             data={'task_id': task.id})

        notif_id = data.get('notif_id')
        if notif_id:
            try:
                notif = Notif.objects.get(id=notif_id)
                notif.delete()
            except:
                pass

        return self.set_result()


class EditTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id', 'title', 'reward', 'require_image']

    def process(self, request, data):
        try:
            reward = int(data['reward'])
            if reward <= 0:
                raise ValueError()
        except ValueError:
            return self.set_error(API_ERRORS.ARGS)

        user = request.user
        if hasattr(user, 'parent'):

            time_limit_str = data.get('time_limit', None)
            if time_limit_str:
                try:
                    time_limit = datetime.datetime.strptime(time_limit_str, ISO_MASK)
                except ValueError:
                    return self.set_error(API_ERRORS.FORMAT)
            else:
                time_limit = None

            notify_time_str = data.get('notify_time', None)
            if notify_time_str:
                try:
                    notify_time = datetime.datetime.strptime(notify_time_str, ISO_MASK)
                except ValueError:
                    return self.set_error(API_ERRORS.FORMAT)
            else:
                notify_time = None

            notify_repeat = data.get('notify_repeat', None)
            if notify_repeat is not None and notify_repeat <= 0:
                return self.set_error(API_ERRORS.FORMAT)

            repeat = data.get('repeat', None)
            if repeat is not None and repeat <= 0:
                return self.set_error(API_ERRORS.FORMAT)

            parent = user.parent
            task = parent.tasks.get(id=data['task_id'])
            task.title = data['title']
            task.desc = data.get('desc', '')
            task.reward = reward
            task.require_image = data['require_image']
            task.time_limit = time_limit
            task.repeat = repeat
            task.notify_time = notify_time
            task.notify_repeat = notify_repeat
            task.save()
            Notif.objects.create(type=Notif.Type.C_EDIT_TASK,
                                 user_from=parent.user,
                                 user_to=task.child.user,
                                 data={'task_id': task.id})
            return self.set_result()
        else:
            return self.set_error(API_ERRORS.PERM)


class OfferTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['title', 'parent_id', 'reward', 'require_image']

    def process(self, request, data):
        if not hasattr(request.user, 'child'):
            return self.set_error(API_ERRORS.PERM)
        child = request.user.child

        try:
            reward = int(data['reward'])
            if reward <= 0:
                raise ValueError()
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        time_limit_str = data.get('time_limit', None)
        if time_limit_str:
            try:
                time_limit = datetime.datetime.strptime(time_limit_str, "%Y-%m-%dT%H:%M:%S.%fZ")
            except ValueError:
                return self.set_error(API_ERRORS.FORMAT)
        else:
            time_limit = None

        notify_time_str = data.get('notify_time', None)
        if notify_time_str:
            try:
                notify_time = datetime.datetime.strptime(notify_time_str, "%Y-%m-%dT%H:%M:%S.%fZ")
            except ValueError:
                return self.set_error(API_ERRORS.FORMAT)
        else:
            notify_time = None

        notify_repeat = data.get('notify_repeat', None)
        if notify_repeat is not None and notify_repeat <= 0:
            return self.set_error(API_ERRORS.FORMAT)

        repeat = data.get('repeat', None)
        if repeat is not None and repeat <= 0:
            return self.set_error(API_ERRORS.FORMAT)

        try:
            parent = Parent.objects.get(id=data['parent_id'])
        except Parent.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        task_data = dict(title=data['title'],
                         desc=data.get('desc', ''),
                         child_id=child.id,
                         parent_id=parent.id,
                         reward=reward,
                         require_image=data['require_image'],
                         time_limit=data.get('time_limit'),
                         repeat=repeat,
                         notify_time=data.get('notify_time'),
                         notify_repeat=notify_repeat)

        Notif.objects.create(type=Notif.Type.P_OFFER_TASK,
                             user_from=child.user,
                             user_to=parent.user,
                             data={'task': task_data})

        return self.set_result()


class RejectTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['notif_id']

    def process(self, request, data):
        if not hasattr(request.user, 'parent'):
            return self.set_error(API_ERRORS.PERM)
        parent = request.user.parent

        try:
            notif = Notif.objects.get(id=data['notif_id'])
        except Notif.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        task = notif.data['task']

        try:
            child = Child.objects.get(id=task['child_id'])
        except Child.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        notif.delete()

        task_data = dict(title=task['title'])

        Notif.objects.create(type=Notif.Type.C_REJECT_TASK,
                             user_from=parent.user,
                             user_to=child.user,
                             data={'task': task_data})

        return self.set_result()


class RequestTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['parent_id']

    def process(self, request, data):
        if not hasattr(request.user, 'child'):
            return self.set_error(API_ERRORS.PERM)
        child = request.user.child

        try:
            parent = Parent.objects.get(id=data['parent_id'])
        except Parent.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        Notif.objects.create(type=Notif.Type.P_REQUEST_TASK,
                             user_from=child.user,
                             user_to=parent.user)

        return self.set_result()


class RemoveTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id']

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'parent'):
            parent = user.parent
            task = parent.tasks.get(id=data['task_id'])
            Notif.objects.create(type=Notif.Type.C_DELETE_TASK,
                                 user_from=parent.user,
                                 user_to=task.child.user,
                                 data={'task_id': task.id,
                                       'task': {
                                           'title': task.title,
                                       }})
            task.delete()
            return self.set_result()
        else:
            return self.set_error(API_ERRORS.PERM)


class RequestRemoveTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id', 'reason']

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'child'):
            child = user.child
            task = child.tasks.get(id=data['task_id'])
            Notif.objects.create(type=Notif.Type.P_REMOVE_TASK,
                                 user_from=child.user,
                                 user_to=task.parent.user,
                                 data={'task': {
                                     'title': task.title,
                                 },
                                     'task_id': task.id,
                                     'reason': data['reason']})

            return self.set_result()
        else:
            return self.set_error(API_ERRORS.PERM)


class RequestProlongTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id', 'time_limit']

    def process(self, request, data):
        if not hasattr(request.user, 'child'):
            return self.set_error(API_ERRORS.PERM)

        child = request.user.child

        time_limit_str = data['time_limit']
        try:
            time_limit = datetime.datetime.strptime(time_limit_str, "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        try:
            task = child.tasks.get(id=data['task_id'])
        except:
            return self.set_error(API_ERRORS.NOTFOUND)

        Notif.objects.create(type=Notif.Type.P_PROLONG_TASK,
                             user_from=child.user,
                             user_to=task.parent.user,
                             data={'task_id': task.id,
                                   'old_limit': task.time_limit,
                                   'new_limit': time_limit_str})

        return self.set_result()


class ProlongConfirm(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['notif_id']

    def process(self, request, data):
        if not hasattr(request.user, 'parent'):
            return self.set_error(API_ERRORS.PERM)
        parent = request.user.parent

        try:
            notif = Notif.objects.get(id=data['notif_id'])
        except Notif.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        try:
            task = parent.tasks.get(id=notif.data['task_id'])
        except:
            return self.set_error(API_ERRORS.NOTFOUND)

        time_limit_str = notif.data.get('new_limit', None)
        try:
            time_limit = datetime.datetime.strptime(time_limit_str, "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        Notif.objects.create(type=Notif.Type.C_PROLONG_CONFIRM,
                             user_from=parent.user,
                             user_to=task.child.user,
                             data={'task_id': task.id,
                                   'old_limit': task.time_limit,
                                   'new_limit': time_limit_str})

        task.time_limit = time_limit
        task.save()

        notif.delete()

        return self.set_result()


class ProlongReject(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['notif_id']

    def process(self, request, data):
        if not hasattr(request.user, 'parent'):
            return self.set_error(API_ERRORS.PERM)
        parent = request.user.parent

        try:
            notif = Notif.objects.get(id=data['notif_id'])
        except Notif.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        try:
            task = parent.tasks.get(id=notif.data['task_id'])
        except:
            return self.set_error(API_ERRORS.NOTFOUND)

        time_limit_str = notif.data.get('new_limit', None)
        try:
            time_limit = datetime.datetime.strptime(time_limit_str, "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        Notif.objects.create(type=Notif.Type.C_PROLONG_REJECT,
                             user_from=parent.user,
                             user_to=task.child.user,
                             data={'task_id': task.id,
                                   'old_limit': task.time_limit,
                                   'new_limit': time_limit_str})

        notif.delete()

        return self.set_result()


class GiftForm(forms.Form):
    data = forms.CharField()
    photo = forms.ImageField(required=False)


class AddGift(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['title', 'child_id', 'cost']

    def check_args(self, request):
        form = GiftForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                data = json.loads(form.cleaned_data['data'])
                if self.data_fields is not None:
                    for arg in self.data_fields:
                        if arg not in data:
                            return self.set_error(API_ERRORS.ARGS)
            except json.JSONDecodeError:
                return self.set_error(API_ERRORS.FORMAT)

            return self.process_with_photo(request, data, form.cleaned_data.get('photo'))
        else:
            return self.set_error(API_ERRORS.ARGS)

    def process_with_photo(self, request, data, photo):
        parents = Parent.objects.filter(user=request.user)
        if parents.count():
            parent = parents[0]
            # time_limit_str = data.get('time_limit', None)
            # if time_limit_str:
            #     time_limit = datetime.datetime.strptime(time_limit_str, "%Y-%m-%dT%H:%M:%S.%fZ")
            # else:
            time_limit = None
            gift = Gift(title=data['title'],
                        desc=data.get('desc', ''),
                        child_id=data['child_id'],
                        parent=parent,
                        cost=data['cost'],
                        time_limit=time_limit,
                        image=photo)
            gift.save()
            if photo is not None:
                resize_avatar(gift.image.path)
            else:
                image_id = data.get('image_id')
                if image_id is not None:
                    try:
                        tmpimage = TempImage.objects.get(id=image_id)
                        gift.image = tmpimage.image
                        gift.save()
                        tmpimage.delete()
                    except TempImage.DoesNotExist:
                        pass
            Notif.objects.create(type=Notif.Type.C_ADD_GIFT,
                                 user_from=parent.user,
                                 user_to=gift.child.user,
                                 data={'gift_id': gift.id})

            notif_id = data.get('notif_id')
            if notif_id:
                try:
                    notif = Notif.objects.get(id=notif_id)
                    notif.delete()
                except:
                    pass

            return self.set_result()
        else:
            return self.set_error(API_ERRORS.PERM)


class EditGift(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['gift_id', 'title', 'cost']

    def check_args(self, request):
        form = GiftForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                data = json.loads(form.cleaned_data['data'])
                if self.data_fields is not None:
                    for arg in self.data_fields:
                        if arg not in data:
                            return self.set_error(API_ERRORS.ARGS)
            except json.JSONDecodeError:
                return self.set_error(API_ERRORS.FORMAT)

            return self.process_with_photo(request, data, form.cleaned_data.get('photo'))
        else:
            return self.set_error(API_ERRORS.ARGS)

    def process_with_photo(self, request, data, photo):
        try:
            cost = int(data['cost'])
            if cost <= 0:
                raise ValueError()
        except ValueError:
            return self.set_error(API_ERRORS.ARGS)

        user = request.user
        if hasattr(user, 'parent'):
            parent = user.parent
            gift = parent.gifts.get(id=data['gift_id'])
            gift.title = data['title']
            gift.desc = data.get('desc', '')
            gift.cost = cost
            if photo is not None:
                gift.image = photo

            gift.save()
            if photo:
                resize_avatar(gift.image.path)
            Notif.objects.create(type=Notif.Type.C_EDIT_GIFT,
                                 user_from=parent.user,
                                 user_to=gift.child.user,
                                 data={'gift_id': gift.id})
            return self.set_result()
        else:
            return self.set_error(API_ERRORS.PERM)


class OfferGift(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['title', 'parent_id', 'cost']

    def check_args(self, request):
        form = GiftForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                data = json.loads(form.cleaned_data['data'])
                if self.data_fields is not None:
                    for arg in self.data_fields:
                        if arg not in data:
                            return self.set_error(API_ERRORS.ARGS)
            except json.JSONDecodeError:
                return self.set_error(API_ERRORS.FORMAT)

            return self.process_with_photo(request, data, form.cleaned_data.get('photo'))
        else:
            return self.set_error(API_ERRORS.ARGS)

    def process_with_photo(self, request, data, photo):
        if not hasattr(request.user, 'child'):
            return self.set_error(API_ERRORS.PERM)

        child = request.user.child

        try:
            parent = Parent.objects.get(id=data['parent_id'])
        except Parent.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        if photo is not None:
            tmpimage = TempImage.objects.create(image=photo)
            resize_avatar(tmpimage.image.path)
        else:
            tmpimage = None

        time_limit = None
        gift_data = dict(title=data['title'],
                         desc=data.get('desc', ''),
                         child_id=child.id,
                         parent=parent.id,
                         cost=data['cost'],
                         time_limit=time_limit,
                         image_id=tmpimage.id if tmpimage else None,
                         image_uri=tmpimage.image.url if tmpimage else None)

        Notif.objects.create(type=Notif.Type.P_OFFER_GIFT,
                             user_from=child.user,
                             user_to=parent.user,
                             data={'gift': gift_data})
        return self.set_result()


class RejectGift(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['notif_id']

    def process(self, request, data):
        if not hasattr(request.user, 'parent'):
            return self.set_error(API_ERRORS.PERM)
        parent = request.user.parent

        try:
            notif = Notif.objects.get(id=data['notif_id'])
        except Notif.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        gift = notif.data['gift']

        try:
            child = Child.objects.get(id=gift['child_id'])
        except Child.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        notif.delete()

        gift_data = dict(title=gift['title'])

        Notif.objects.create(type=Notif.Type.C_REJECT_GIFT,
                             user_from=parent.user,
                             user_to=child.user,
                             data={'gift': gift_data})

        return self.set_result()


class RemoveGift(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['gift_id']

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'parent'):
            parent = user.parent
            gift = parent.gifts.get(id=data['gift_id'])
            Notif.objects.create(type=Notif.Type.C_DELETE_GIFT,
                                 user_from=parent.user,
                                 user_to=gift.child.user,
                                 data={'gift': {
                                     'title': gift.title
                                 }})
            gift.delete()
            return self.set_result()
        else:
            return self.set_error(API_ERRORS.PERM)


class ConfirmReg(JsonApiView):
    http_method_names = ['post']
    auth_required = False
    data_fields = ['login', 'password', 'token']

    def process(self, request, data):
        user = authenticate(request,
                            username=get_phone(data['login']),
                            password=data['password'])
        if user is None:
            return self.set_error(API_ERRORS.AUTH)

        person = get_user_person(user)
        if person is None:
            return self.set_error(API_ERRORS.PERM)
        try:
            token = user.tokens.get(token=data['token'],
                                    type=UserToken.Type.ConfirmReg)
        except UserToken.DoesNotExist:
            user.tokens.all().delete()
            return self.set_error(API_ERRORS.NOTFOUND)
        except UserToken.MultipleObjectsReturned:
            user.tokens.all().delete()
            return self.set_error(API_ERRORS.NOTFOUND)
        person.confirmed = True
        person.save()
        user.tokens.all().delete()
        login(request, user)
        return self.set_result()


class SendRegSMS(JsonApiView):
    http_method_names = ['post']
    auth_required = False
    data_fields = ['login', 'password']

    def process(self, request, data):
        phone = data['login']
        phone = get_phone(phone)
        if not phone:
            return self.set_error(API_ERRORS.FORMAT)

        user = authenticate(request,
                            username=phone,
                            password=data['password'])
        if user is None:
            return self.set_error(API_ERRORS.AUTH)

        user.tokens.all().delete()
        token = random_sms_token()
        UserToken.objects.create(user=user, token=token,
                                 type=UserToken.Type.ConfirmReg)

        person = get_user_person(user)
        send_sms_code(person.phone, token)

        return self.set_result()


class SendPasswordSMS(JsonApiView):
    http_method_names = ['post']
    auth_required = False
    data_fields = ['phone']

    def process(self, request, data):
        phone = data['phone']
        phone = get_phone(phone)
        if not phone:
            return self.set_error(API_ERRORS.FORMAT)

        try:
            child = Child.objects.get(phone=phone)
            user = child.user
        except Child.DoesNotExist:
            try:
                parent = Parent.objects.get(phone=phone)
                user = parent.user
            except:
                return self.set_error(API_ERRORS.NOTFOUND)

        user.tokens.all().delete()
        token = random_sms_token()
        UserToken.objects.create(user=user, token=token,
                                 type=UserToken.Type.ResetPassword)

        person = get_user_person(user)
        send_sms_code(person.phone, token)

        return self.set_result()


class ResetPassword(JsonApiView):
    http_method_names = ['post']
    auth_required = False
    data_fields = ['phone', 'password', 'token']

    def process(self, request, data):
        phone = data['phone']
        phone = get_phone(phone)
        if not phone:
            return self.set_error(API_ERRORS.FORMAT)

        try:
            child = Child.objects.get(phone=phone)
            user = child.user
        except Child.DoesNotExist:
            try:
                parent = Parent.objects.get(phone=phone)
                user = parent.user
            except:
                return self.set_error(API_ERRORS.NOTFOUND)

        try:
            token = user.tokens.get(token=data['token'],
                                    type=UserToken.Type.ResetPassword)
        except UserToken.DoesNotExist:
            user.tokens.all().delete()
            return self.set_error(API_ERRORS.NOTFOUND)
        except UserToken.MultipleObjectsReturned:
            user.tokens.all().delete()
            return self.set_error(API_ERRORS.NOTFOUND)

        user.set_password(data['password'])
        user.tokens.all().delete()
        user.save()
        return self.set_result()


class RegForm(forms.Form):
    data = forms.CharField()
    photo = forms.ImageField(required=False)


class ParentReg(JsonApiView):
    http_method_names = ['post']
    data_fields = ['name', 'phone', 'role', 'password']

    def check_args(self, request):
        form = RegForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                data = json.loads(form.cleaned_data['data'])
                if self.data_fields is not None:
                    for arg in self.data_fields:
                        if arg not in data:
                            return self.set_error(API_ERRORS.ARGS)
            except json.JSONDecodeError:
                return self.set_error(API_ERRORS.FORMAT)

            return self.process_with_photo(request, data, form.cleaned_data.get('photo'))
        else:
            return self.set_error(API_ERRORS.ARGS)

    def process_with_photo(self, request, data, photo):
        errors = {}

        name = data['name']
        if not name:
            errors['name'] = True

        try:
            date = data.get('date')
            if date:
                date = datetime.datetime.strptime(date, '%d.%m.%Y').date()
            # if datetime.datetime.now().date().year - date.year > 22:
            #     role = 'parent'
            # else:
            #     role = 'child'
            else:
                date = None
        except ValueError:
            errors['date'] = True

        phone = data['phone']
        phone = get_phone(phone)
        if not phone or phone_taken(phone):
            errors['phone'] = True

        gender = data.get('gender')
        if gender and gender not in Gender.List:
            errors['gender'] = True

        role = data['role']
        if role not in ('parent', 'child'):
            errors['role'] = True

        # username = data['username']
        # if not username or username_taken(username):
        #     errors['username'] = True
        username = phone

        password = data['password']
        if not password:
            errors['password'] = True

        email = data.get('email', '')
        if email and not valid_email(email):
            errors['email'] = True

        if len(errors.keys()) == 0:

            user = User.objects.create_user(username=username,
                                            email=email,
                                            password=password)

            if role == 'parent':
                parent = Parent.objects.create(user=user,
                                               name=data['name'],
                                               birthdate=date,
                                               phone=phone,
                                               gender=gender,
                                               confirmed=False)
                if photo:
                    parent.image = photo
                    parent.save()
                    resize_avatar(parent.image.path)
            else:
                child = Child.objects.create(user=user,
                                             name=data['name'],
                                             birthdate=date,
                                             phone=phone,
                                             gender=gender,
                                             confirmed=False)
                if photo:
                    child.image = photo
                    child.save()
                    resize_avatar(child.image.path)

            return self.set_result()
        else:
            return self.set_error(API_ERRORS.FORMAT, {'field_errors': errors})


class EditProfile(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['name']

    def check_args(self, request):
        form = RegForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                data = json.loads(form.cleaned_data['data'])
                if self.data_fields is not None:
                    for arg in self.data_fields:
                        if arg not in data:
                            return self.set_error(API_ERRORS.ARGS)
            except json.JSONDecodeError:
                return self.set_error(API_ERRORS.FORMAT)

            return self.process_with_photo(request, data, form.cleaned_data.get('photo'))
        else:
            return self.set_error(API_ERRORS.ARGS)

    def process_with_photo(self, request, data, photo):
        user = request.user
        person = get_user_person(user)
        if person is None:
            return self.set_error(API_ERRORS.PERM)

        errors = {}

        name = data['name']
        if not name:
            errors['name'] = True

        try:
            date = data.get('date')
            if date:
                date = datetime.datetime.strptime(date, '%d.%m.%Y').date()
            else:
                date = None
        except ValueError:
            errors['date'] = True

        # phone = data['phone']
        # phone = get_phone(phone)
        # if not phone or (phone_taken(phone) and person.phone != phone):
        #     errors['phone'] = True

        gender = data.get('gender')
        if gender and gender not in Gender.List:
            errors['gender'] = True

        # username = data['username']
        # if not username or \
        #         (username_taken(username) and username != user.username):
        #     errors['username'] = True
        # username = phone

        password = data.get('password')
        # if password:
        #     errors['password'] = True

        email = data.get('email', '')
        if email and not valid_email(email):
            errors['email'] = True

        if len(errors.keys()) == 0:

            if hasattr(user, 'parent'):
                parent = user.parent
                parent.name = name
                parent.birthdate = date
                # parent.phone = phone
                parent.gender = gender
                parent.save()

                user.email = email
                # user.username = username
                if password:
                    user.set_password(password)
                if photo:
                    parent.image = photo
                    parent.save()
                    resize_avatar(parent.image.path)
                user.save()

                return self.set_result()
            elif hasattr(user, 'child'):
                child = user.child
                child.name = name
                child.birthdate = date
                # child.phone = phone
                child.gender = gender
                child.save()

                user.email = email
                # user.username = username
                if password:
                    user.set_password(password)
                if photo:
                    child.image = photo
                    child.save()
                    resize_avatar(child.image.path)
                user.save()

                return self.set_result()
            else:
                return self.set_error(API_ERRORS.PERM)
        else:
            return self.set_error(API_ERRORS.FORMAT, {'field_errors': errors})


class ChildData(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        children = Child.objects.filter(user=request.user)
        if children.count():
            child = children[0]
            child_data = child.to_dict()
            return self.set_result(child_data)
        return self.set_error(API_ERRORS.PERM)


class DoTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id']

    def process(self, request, data):
        children = Child.objects.filter(user_id=request.user.id)
        if children.count() != 1:
            return self.set_error(API_ERRORS.PERM)

        child = children[0]
        task_id = data['task_id']
        tasks = child.tasks.filter(id=task_id)

        if tasks.count() != 1:
            return self.set_error(API_ERRORS.ARGS)

        task = tasks[0]

        task.finish_time = Now()
        task.save()

        Notif.objects.create(type=Notif.Type.P_DO_TASK,
                             user_from=child.user,
                             user_to=task.parent.user,
                             data={'task_id': task.id})

        return self.set_result()


class ConfirmTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id']

    def process(self, request, data):
        parents = Parent.objects.filter(user_id=request.user.id)
        if parents.count() != 1:
            return self.set_error(API_ERRORS.PERM)

        parent = parents[0]
        task_id = data['task_id']
        tasks = parent.tasks.filter(id=task_id)

        if tasks.count() != 1:
            return self.set_error(API_ERRORS.ARGS)

        task = tasks[0]

        if task.confirm_time is not None:
            return self.set_error(API_ERRORS.ARGS)

        task.confirm_time = Now()
        task.save()

        points = task.child.points.get(parent=parent)
        points.points += task.reward
        points.save()

        Notif.objects.create(type=Notif.Type.C_CONFIRM_TASK,
                             user_from=parent.user,
                             user_to=task.child.user,
                             data={'task_id': task.id,
                                   'task': {
                                       'title': task.title,
                                   }})

        return self.set_result()


class TaskImageForm(forms.Form):
    data = forms.CharField()
    photo = forms.ImageField(required=True)


class DoTaskImage(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id']

    def check_args(self, request):
        form = RegForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                data = json.loads(form.cleaned_data['data'])
                if self.data_fields is not None:
                    for arg in self.data_fields:
                        if arg not in data:
                            return self.set_error(API_ERRORS.ARGS)
            except json.JSONDecodeError:
                return self.set_error(API_ERRORS.FORMAT)

            return self.process_with_photo(request, data, form.cleaned_data['photo'])
        else:
            return self.set_error(API_ERRORS.ARGS)

    def process_with_photo(self, request, data, photo):
        user = request.user

        if not hasattr(user, 'child'):
            return self.set_error(API_ERRORS.PERM)

        child = user.child
        task_id = data['task_id']
        tasks = child.tasks.filter(id=task_id)

        if tasks.count() != 1:
            return self.set_error(API_ERRORS.ARGS)

        task = tasks[0]

        task.finish_image = photo
        task.save()

        return self.set_result()


class RemindTask(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['task_id']

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'parent'):
            parent = user.parent
            task = parent.tasks.get(id=data['task_id'])
            Notif.objects.create(type=Notif.Type.C_REMIND_TASK,
                                 user_from=parent.user,
                                 user_to=task.child.user,
                                 data={'task_id': task.id})
            return self.set_result()
        else:
            return self.set_error(API_ERRORS.PERM)


class BuyGift(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['gift_id']

    def process(self, request, data):
        children = Child.objects.filter(user_id=request.user.id)
        if children.count() != 1:
            return self.set_error(API_ERRORS.PERM)

        child = children[0]
        gift_id = data['gift_id']
        gifts = child.gifts.filter(id=gift_id)

        if gifts.count() != 1:
            return self.set_error(API_ERRORS.ARGS)

        gift = gifts[0]

        if gift.buying_time is not None:
            return self.set_error(API_ERRORS.ARGS)

        points = gift.parent.points.get(child=child)
        if points.points < gift.cost:
            return self.set_error(API_ERRORS.ARGS)

        points.points -= gift.cost
        points.save()

        gift.buying_time = Now()
        gift.save()

        Notif.objects.create(type=Notif.Type.P_BUY_GIFT,
                             user_from=child.user,
                             user_to=gift.parent.user,
                             data={'gift_id': gift.id})

        return self.set_result()


class ConfirmGift(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['gift_id']

    def process(self, request, data):
        parents = Parent.objects.filter(user_id=request.user.id)
        if parents.count() != 1:
            return self.set_error(API_ERRORS.PERM)

        parent = parents[0]
        gift_id = data['gift_id']
        gifts = parent.gifts.filter(id=gift_id)

        if gifts.count() != 1:
            return self.set_error(API_ERRORS.ARGS)

        gift = gifts[0]

        if gift.confirm_time is not None:
            return self.set_error(API_ERRORS.ARGS)

        gift.confirm_time = Now()
        gift.save()

        Notif.objects.create(type=Notif.Type.C_CONFIRM_GIFT,
                             user_from=parent.user,
                             user_to=gift.child.user,
                             data={'gift': {
                                 'title': gift.title
                             }})

        return self.set_result()


class Chat(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['user_id']

    def process(self, request, data):
        user1_id = request.user.id
        user2_id = data['user_id']
        messages = Message.objects \
                       .filter(Q(user_from_id=user1_id) & Q(user_to_id=user2_id) |
                               Q(user_from_id=user2_id) & Q(user_to_id=user1_id)) \
                       .order_by('creation_time')[:100]

        messages = [m.to_dict() for m in messages]

        return self.set_result({'messages': messages})


class SetRead(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['user_id']

    def process(self, request, data):
        user1_id = request.user.id
        user2_id = data['user_id']
        messages = Message.objects \
            .filter(Q(user_from_id=user2_id) & Q(user_to_id=user1_id))

        messages.update(read=True)

        Notif.objects.filter(type=Notif.Type.MESSAGE,
                             user_from_id=user2_id,
                             user_to_id=user1_id).delete()

        return self.set_result()


class ChatList(JsonApiView):
    http_method_names = ['get']
    auth_required = True
    data_fields = []

    def process(self, request, data):
        user = request.user
        person = get_user_person(user)

        parents = list(person.parents.all())
        children = list(person.children.all())
        family = parents + children
        res = [{'user': u.to_dict_short(user.id),
                'unread': Message.get_unread_count(user, u.user),
                'key': str(u.user.id)} for u in family]

        return self.set_result(res)


class SendMessage(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['user_id', 'text']

    def process(self, request, data):
        try:
            user_from = request.user
            user_to = User.objects.get(id=data['user_id'])
            text = data['text']
            Message.objects.create(text=text,
                                   user_from=user_from,
                                   user_to=user_to)

            notifs = Notif.objects.filter(type=Notif.Type.MESSAGE,
                                          user_from=user_from,
                                          user_to=user_to).delete()

            Notif.objects.create(type=Notif.Type.MESSAGE,
                                 user_from=user_from,
                                 user_to=user_to)

            return self.set_result()

        except Message.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)


class Family(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'child'):
            child = user.child
            parents = [par.to_dict_short(user.id) for par in child.parents.all()
                       if par.get_perms()['family']]
            if parents:
                children = [ch.to_dict_short(user.id) for ch in child.children.all()]
            else:
                children = []
            user_data = child.to_dict_short(user.id)
            role = 'child'
        elif hasattr(user, 'parent'):
            parent = user.parent
            if parent.get_perms()['family']:
                children = [ch.to_dict_short(user.id) for ch in parent.children.all()]
                parents = [par.to_dict_short(user.id) for par in parent.parents.all()]
            else:
                children = []
                parents = []
            user_data = parent.to_dict_short(user.id)
            role = 'parent'
        else:
            return self.set_error(API_ERRORS.PERM)

        res = {
            'role': role,
            'self': user_data,
            'children': children,
            'parents': parents,
        }

        return self.set_result(res)


class FindUser(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['phone']

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'parent'):
            child_list = user.parent.children
            parent_list = user.parent.parents
            own_role = 'parent'
            has_perm = user.parent.has_family_perm()
        elif hasattr(user, 'child'):
            child_list = user.child.children
            parent_list = user.child.parents
            own_role = 'child'
            has_perm = user.child.has_family_perm()
        else:
            return self.set_error(API_ERRORS.PERM)

        phone = data['phone']
        phone = get_phone(phone)
        if not phone:
            return self.set_error(API_ERRORS.FORMAT)

        children = Child.objects.filter(phone=phone)
        parents = Parent.objects.filter(phone=phone)
        if children.count() == 1 and parents.count() == 0:
            if not child_list.filter(id=children[0].id).exists()\
               and (own_role == 'parent' or has_perm):

                return self.set_result({
                    'role': 'child',
                    'data': children[0].to_dict_short(user.id)
                })

        elif children.count() == 0 and parents.count() == 1:
            if not parent_list.filter(id=parents[0].id).exists()\
               and (own_role == 'child' or has_perm):

                return self.set_result({
                    'role': 'parent',
                    'data': parents[0].to_dict_short(user.id)
                })

        return self.set_error(API_ERRORS.NOTFOUND)


class OfferLinkUser(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['role', 'user_id']

    def process(self, request, data):
        user = request.user

        role = data['role']
        user_id = data['user_id']

        try:
            if role == 'parent':
                person = Parent.objects.get(id=user_id)
            elif role == 'child':
                person = Child.objects.get(id=user_id)
            else:
                return self.set_error(API_ERRORS.ARGS)
        except Parent.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)
        except Child.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        from_person = get_user_person(user)
        user_data = from_person.to_dict_show()

        Notif.objects.create(type=Notif.Type.OFFER_LINK,
                             user_from=user,
                             user_to=person.user)

        return self.set_result()


class RejectLinkUser(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['role', 'user_id']

    def process(self, request, data):
        user = request.user

        role = data['role']
        user_id = data['user_id']

        try:
            if role == 'parent':
                person = Parent.objects.get(id=user_id)
            elif role == 'child':
                person = Child.objects.get(id=user_id)
            else:
                return self.set_error(API_ERRORS.ARGS)
        except Parent.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)
        except Child.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        Notif.objects.create(type=Notif.Type.REJECT_LINK,
                             user_from=user,
                             user_to=person.user)
        Notif.objects.filter(type=Notif.Type.OFFER_LINK,
                             user_from=person.user,
                             user_to=user).delete()

        return self.set_result()


class LinkUser(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['role', 'user_id']

    def process(self, request, data):
        user = request.user
        person = get_user_person(user)

        role = data['role']
        user_id = data['user_id']

        try:
            if role == 'parent':
                parent = Parent.objects.get(id=user_id)
                if hasattr(user, 'child'):
                    if not ParentChild.objects. \
                            filter(parent=parent, child=user.child).exists():
                        ParentChild.objects.create(parent=parent, child=user.child)
                        Notif.objects.create(type=Notif.Type.ACCEPT_LINK,
                                             user_from=user,
                                             user_to=parent.user)
                        Notif.objects.filter(type=Notif.Type.OFFER_LINK,
                                             user_from=parent.user,
                                             user_to=user).delete()
                        return self.set_result()
                    else:
                        return self.set_error(API_ERRORS.ARGS)
                elif hasattr(user, 'parent'):
                    if not user.parent.parents.filter(id=parent.id).exists():
                        user.parent.parents.add(parent)

                        Notif.objects.create(type=Notif.Type.ACCEPT_LINK,
                                             user_from=user,
                                             user_to=parent.user)
                        Notif.objects.filter(type=Notif.Type.OFFER_LINK,
                                             user_from=parent.user,
                                             user_to=user).delete()
                        return self.set_result()
                    else:
                        return self.set_error(API_ERRORS.ARGS)
                else:
                    return self.set_error(API_ERRORS.PERM)

            elif role == 'child':
                child = Child.objects.get(id=user_id)
                if hasattr(user, 'parent'):
                    if not ParentChild.objects. \
                            filter(parent=user.parent, child=child).exists():
                        ParentChild.objects.create(parent=user.parent, child=child)
                        Notif.objects.create(type=Notif.Type.ACCEPT_LINK,
                                             user_from=user,
                                             user_to=child.user)
                        Notif.objects.filter(type=Notif.Type.OFFER_LINK,
                                             user_from=child.user,
                                             user_to=user).delete()
                        return self.set_result()
                    else:
                        return self.set_error(API_ERRORS.ARGS)
                elif hasattr(user, 'child'):
                    if not user.child.children.filter(id=child.id).exists():
                        user.child.children.add(child)
                        Notif.objects.create(type=Notif.Type.ACCEPT_LINK,
                                             user_from=user,
                                             user_to=child.user)
                        Notif.objects.filter(type=Notif.Type.OFFER_LINK,
                                             user_from=child.user,
                                             user_to=user).delete()
                        return self.set_result()
                    else:
                        return self.set_error(API_ERRORS.ARGS)
                else:
                    return self.set_error(API_ERRORS.PERM)
            else:
                return self.set_error(API_ERRORS.ARGS)
        except Parent.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)
        except Child.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)


class UnlinkUser(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['user_id']

    def process(self, request, data):
        user = request.user

        user_id = data['user_id']

        try:
            user2 = User.objects.get(id=user_id)
            if hasattr(user2, 'parent'):
                parent = user2.parent
                if hasattr(user, 'child'):
                    ParentChild.objects.filter(parent=parent, child=user.child).delete()
                    return self.set_result()
                elif hasattr(user, 'parent'):
                    user.parent.parents.remove(parent)
                    return self.set_result()
                else:
                    return self.set_error(API_ERRORS.PERM)

            elif hasattr(user2, 'child'):
                child = user2.child
                if hasattr(user, 'parent'):
                    ParentChild.objects.filter(parent=user.parent, child=child).delete()
                    return self.set_result()
                elif hasattr(user, 'child'):
                    user.child.children.remove(child)
                    return self.set_result()
                else:
                    return self.set_error(API_ERRORS.PERM)
            else:
                return self.set_error(API_ERRORS.ARGS)
        except Parent.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)
        except Child.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)


class Award(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['child_id', 'amount']

    def process(self, request, data):
        if not hasattr(request.user, 'parent'):
            return self.set_error(API_ERRORS.PERM)

        parent = request.user.parent

        child_id = data['child_id']
        try:
            amount = int(data['amount'])
            if amount <= 0:
                raise ValueError()
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        try:
            points = parent.points.get(child_id=child_id)
            points.points += amount
            points.save()
        except ParentChild.DoesNotExist:
            return self.set_error(API_ERRORS.NOTFOUND)

        task_id = data.get('task_id')

        Notif.objects.create(type=Notif.Type.C_AWARD,
                             user_from=parent.user,
                             user_to=points.child.user,
                             data={'amount': amount,
                                   'task_id': task_id})

        return self.set_result()


class Penalty(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['child_id', 'amount']

    def process(self, request, data):
        if not hasattr(request.user, 'parent'):
            return self.set_error(API_ERRORS.PERM)

        parent = request.user.parent

        child_id = data['child_id']
        try:
            amount = int(data['amount'])
            if amount <= 0:
                raise ValueError()
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        try:
            points = parent.points.get(child_id=child_id)
            points.points -= amount
            if points.points < 0:
                points.points = 0
            points.save()

            task_id = data.get('task_id')
            if task_id:
                try:
                    task = parent.tasks.get(id=task_id,
                                            child_id=child_id,
                                            penalty_time__isnull=True)
                    task.penalty_time = Now()
                    task.save()
                except Task.DoesNotExist:
                    pass

        except ParentChild.DoesNotExist:
            return self.set_error(API_ERRORS.NOTFOUND)

        Notif.objects.create(type=Notif.Type.C_PENALTY,
                             user_from=parent.user,
                             user_to=points.child.user,
                             data={'amount': amount,
                                   'task_id': task_id})

        return self.set_result()


class SetDisplayPrefs(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['user_id', 'name']

    def process(self, request, data):
        try:
            prefs = request.user.display_prefs_to.get(for_user__id=data['user_id'])
        except DisplayPrefs.DoesNotExist:
            prefs = DisplayPrefs(for_user_id=data['user_id'],
                                 to_user=request.user,
                                 name='')

        prefs.name = data['name']
        prefs.save()

        return self.set_result()


class DeleteAccount(JsonApiView):
    http_method_names = ['get']
    auth_required = True
    data_fields = []

    def process(self, request, data):
        request.user.delete()
        return self.set_result()


class NotifList(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        user = request.user
        notifs = Notif.objects.filter(user_to=user) \
            .order_by('-creation_time')[:20]
        res = [n.to_dict(user.id) for n in notifs]
        return self.set_result(res)


class SetNotifRead(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        user = request.user
        notifs = Notif.objects.filter(user_to=user)
        notifs.update(read=True)
        return self.set_result()


class NotifCount(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        user = request.user
        notifs = Notif.objects.filter(user_to=user, read=False)
        return self.set_result({'count': notifs.count()})


class RemoveNotif(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['notif_id']

    def process(self, request, data):
        user = request.user
        try:
            notif = Notif.objects.get(id=data['notif_id'])
        except Notif.DoesNotExist:
            return self.set_error(API_ERRORS.ARGS)

        if notif.user_from == user or notif.user_to == user:
            notif.delete()
            return self.set_result()
        else:
            return self.set_error(API_ERRORS.NOTFOUND)


class ContactSupport(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['text']

    def process(self, request, data):
        user = request.user
        person = get_user_person(user)
        role = 'Родитель' if isinstance(person, Parent) else 'Ребенок'

        text = data['text']
        full_text = 'Обратная связь от пользователя\n' + \
                    'Роль: ' + role + '\n' + \
                    'Имя: ' + person.name + '\n' + \
                    'Телефон: ' + person.phone + '\n' + \
                    'Сообщение:\n' + text
        send_mail('Обратная связь',
                  full_text,
                  EMAIL_HOST_USER,
                  [SUPPORT_EMAIL])

        return self.set_result()


class TaskHistory(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        user = request.user
        if hasattr(user, 'child'):
            child = user.child
            person = child
            persons = child.parents.all()

            if True not in map(lambda par: par.get_perms()['history'], persons):
                return self.set_error(API_ERRORS.PERM)

            res = []
            for pers in persons:
                if not pers.get_perms()['history']:
                    continue

                doneTasks = person.tasks.filter(parent=pers,
                                                confirm_time__isnull=False) \
                                .order_by('creation_time')[:20]

                undoneTasks = person.tasks.filter(parent=pers,
                                                  confirm_time__isnull=True) \
                    .filter(Q(time_limit__gt=Now()) | Q(time_limit__isnull=True)) \
                    .order_by('creation_time')

                expiredTasks = person.tasks.filter(parent=pers,
                                                   confirm_time__isnull=True) \
                                   .filter(time_limit__lte=Now()) \
                                   .order_by('creation_time')[:20]

                res.append({
                    'user': pers.to_dict_short(user.id),
                    'done': [task.to_dict() for task in doneTasks],
                    'undone': [task.to_dict() for task in undoneTasks],
                    'expired': [task.to_dict() for task in expiredTasks],
                })

            return self.set_result(res)

        elif hasattr(user, 'parent'):
            parent = user.parent
            person = parent
            persons = parent.children.all()

            if not person.get_perms()['history']:
                return self.set_error(API_ERRORS.PERM)

            res = []
            if person.get_perms()['history']:
                for pers in persons:
                    doneTasks = person.tasks.filter(child=pers,
                                                    confirm_time__isnull=False) \
                        .order_by('creation_time')

                    undoneTasks = person.tasks.filter(child=pers,
                                                      confirm_time__isnull=True) \
                        .filter(Q(time_limit__gt=Now()) | Q(time_limit__isnull=True)) \
                        .order_by('creation_time')

                    expiredTasks = person.tasks.filter(child=pers,
                                                       confirm_time__isnull=True) \
                        .filter(time_limit__lte=Now()) \
                        .order_by('creation_time')

                    res.append({
                        'user': pers.to_dict_short(user.id),
                        'done': [task.to_dict() for task in doneTasks],
                        'undone': [task.to_dict() for task in undoneTasks],
                        'expired': [task.to_dict() for task in expiredTasks],
                    })

            return self.set_result(res)
        else:
            return self.set_error(API_ERRORS.PERM)


class AddFcmDevice(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['token']

    def process(self, request, data):
        user = request.user
        if not user.tokens.filter(token=data['token'],
                                  type=UserToken.Type.FcmDevice).exists():

            UserToken.objects.create(user=user,
                                     token=data['token'],
                                     type=UserToken.Type.FcmDevice)
        return self.set_result()


class RemoveFcmDevice(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['token']

    def process(self, request, data):
        user = request.user
        UserToken.objects.filter(user=user,
                                 token=data['token'],
                                 type=UserToken.Type.FcmDevice).delete()
        return self.set_result()


class GetPerms(JsonApiView):
    http_method_names = ['get']
    auth_required = True

    def process(self, request, data):
        user = request.user
        person = get_user_person(user)
        if person is None:
            return self.set_error(API_ERRORS.PERM)

        perms = person.get_perms()
        role = 'parent' if hasattr(user, 'parent') else 'child'
        res = {
            'role': role,
            'perms': perms,
        }
        return self.set_result(res)


class SetSub(JsonApiView):
    http_method_names = ['post']
    auth_required = True
    data_fields = ['type', 'start']

    def process(self, request, data):
        user = request.user
        if not hasattr(user, 'parent'):
            return self.set_error(API_ERRORS.PERM)

        parent = user.parent

        type = data['type']
        if type not in SubType.List:
            return self.set_error(API_ERRORS.ARGS)

        start_str = data['start']
        try:
            start = datetime.datetime.strptime(start_str, ISO_MASK)
        except ValueError:
            return self.set_error(API_ERRORS.FORMAT)

        if type == SubType.FULL_ALL:
            expires = None
        else:
            expires = datetime.datetime.now() + datetime.timedelta(days=7)

        checked = datetime.datetime.now()
        try:
            sub = parent.subs.get(type=type)
            sub.expires = expires
            sub.checked = checked
            sub.save()
        except Sub.DoesNotExist:
            sub = Sub(type=type,
                      checked=checked,
                      expires=expires)
            sub.save()
            parent.subs.add(sub)
        except Sub.MultipleObjectsReturned:
            parent.subs.filter(type=type).delete()

            sub = Sub(type=type,
                      checked=checked,
                      expires=expires)
            sub.save()
            parent.subs.add(sub)
        return self.set_result()


def get_phone(number):
    format = phonenumbers.PhoneNumberFormat.E164
    try:
        parsed = phonenumbers.parse(number, 'RU')
        if phonenumbers.is_valid_number(parsed):
            return phonenumbers.format_number(parsed, format)
    except phonenumbers.phonenumberutil.NumberParseException:
        if number.startswith('8'):
            number = '+7' + number[1:]
            return get_phone(number)

    return None


def valid_email(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def phone_taken(phone):
    return Child.objects.filter(phone=phone).exists() \
           or Parent.objects.filter(phone=phone).exists()


def username_taken(username):
    return User.objects.filter(username=username).exists()

# MIGRATE FROM LOGIN TO PHONE
#
# for user in User.objects.all():
#     p = get_user_person(user)
#     user.username = p.phone
#     user.save()
