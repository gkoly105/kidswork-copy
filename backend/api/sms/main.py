from .config import ALLOW_SEND
from .smsc_api import *
import random


def send_sms_code(phone_number, code):
    text = gen_message(code)
    send_message(phone_number, text)


def send_message(phone_number, message_text):
    # print(phone_number, " : ", message_text)
    if ALLOW_SEND:
        smsc = SMSC()
        r = smsc.send_sms(phone_number, message_text, sender="sms")


def gen_message(code):
    prefix = "Код подтверждения KidsWork: "
    return prefix + str(code)


if __name__ == "__main__":
    smsc = SMSC()
    number = "79165949838"
    text = ""

    send_message(smsc, number, text)
