import datetime
import random

from django.contrib.auth.models import User
from django.db.models import *
from django.db.models.functions import Now
from django.utils import timezone
from firebase_admin import messaging
from firebase_admin.messaging import Notification
from jsonfield import JSONField

from api.fcm.push import fcm

SHORT_LEN = 50
MID_LEN = 255
LONG_LEN = 1000
PHONE_LEN = 15

TASK_LIMIT = 4  # task count per limit period
TASK_LIMIT_PERIOD = 7  # days


# def format_date(dt):
#     now = timezone.now().date()
#     date = dt.date()
#     if date.year == now.year and date.month == now.month:
#         if (date - now).days == -2:
#             date_str = 'позавчера'
#         elif (date - now).days == -1:
#             date_str = 'вчера'
#         elif (date - now).days == 0:
#             date_str = 'сегодня'
#         elif (date - now).days == 1:
#             date_str = 'завтра'
#         elif (date - now).days == 2:
#             date_str = 'послезавтра'
#         else:
#             date_str =

def random_sms_token():
    return random.randint(100000, 999999)


class UserToken(Model):
    class Type:
        ConfirmReg = 1
        ResetPassword = 2
        FcmDevice = 3

    user = ForeignKey(User, related_name='tokens', on_delete=CASCADE)
    token = CharField(max_length=256)
    type = SmallIntegerField()
    creation_time = DateTimeField(auto_now_add=True)


class Gender:
    Male = 'M'
    Female = 'F'
    Choices = [
        (Male, 'Male'),
        (Female, 'Female'),
    ]
    List = [Male, Female]
    Length = 1


class SubType:
    FULL_1M = '1m'
    FULL_6M = '6m'
    FULL_1Y = '1y'
    FULL_ALL = 'all'
    TASKS = 'tsk'
    CALENDAR = 'cal'
    FAMILY = 'fam'
    HISTORY = 'hst'

    Choices = [
        (FULL_1M, FULL_1M),
        (FULL_6M, FULL_6M),
        (FULL_1Y, FULL_1Y),
        (FULL_ALL, FULL_ALL),
        (TASKS, TASKS),
        (CALENDAR, CALENDAR),
        (FAMILY, FAMILY),
        (HISTORY, HISTORY),
    ]

    List = [row[0] for row in Choices]

    MAX_LEN = max(len(row[0]) for row in Choices)


class Sub(Model):
    type = CharField(choices=SubType.Choices,
                     max_length=SubType.MAX_LEN)
    expires = DateTimeField(null=True)
    checked = DateTimeField()
    started = DateTimeField(auto_now_add=True)


class Child(Model):
    user = OneToOneField(User, on_delete=CASCADE)
    name = CharField(max_length=SHORT_LEN)
    image = ImageField(null=True)
    birthdate = DateField(null=True)
    phone = CharField(max_length=PHONE_LEN)
    gender = CharField(max_length=Gender.Length,
                       choices=Gender.Choices,
                       null=True)
    confirmed = BooleanField(default=True)

    children = ManyToManyField('self')

    def get_perms(self):
        return {par.id: par.get_perms(self) for par in self.parents.all()}

    def has_family_perm(self):
        for par in self.parents.all():
            if par.has_family_perm():
                return True
        return False

    def to_dict(self, parent_id):
        task_list = [task.to_dict()
                     for task in self.tasks.filter(parent_id=parent_id,
                                                   confirm_time__isnull=True)
                         .filter(Q(time_limit__gt=Now()) | Q(time_limit__isnull=True))]
        gift_list = [gift.to_dict()
                     for gift in self.gifts.filter(parent_id=parent_id,
                                                   confirm_time__isnull=True)]
        points = ParentChild.objects.get(parent_id=parent_id,
                                         child_id=self.id).points

        tasks = self.tasks.filter(parent_id=parent_id,
                                  confirm_time=None) \
            .filter(Q(time_limit__gt=Now()) | Q(time_limit__isnull=True)) \
            .count()

        tasks_done = self.tasks.filter(parent_id=parent_id,
                                       confirm_time__isnull=True,
                                       finish_time__isnull=False).count()
        gifts = self.gifts.filter(parent_id=parent_id,
                                  confirm_time__isnull=True).count()

        parent = Parent.objects.get(id=parent_id)

        task_limit = None if parent.get_perms()['tasks'] \
                         else get_task_limit(parent, self)

        return {
            'id': self.id,
            'key': str(self.id),
            'name': self.name,
            'gender': self.gender,
            'display_name': self.get_display_name(parent.user.id),
            'username': self.user.username,
            'user_id': self.user.id,
            'points': points,
            'tasks': tasks,
            'tasksDone': tasks_done,
            'gifts': gifts,
            'image': self.image.url if self.image else None,
            'taskList': task_list,
            'giftList': gift_list,
            'taskLimit': task_limit,
        }

    def to_dict_short(self, user_id):
        return {
            'id': self.id,
            'key': str(self.id),
            'name': self.name,
            'gender': self.gender,
            'display_name': self.get_display_name(user_id),
            'phone': self.phone,
            'user_id': self.user.id,
            'image': self.image.url if self.image else None,
            'role': 'child',
        }

    def to_dict_profile(self):
        return {
            'id': self.id,
            'key': str(self.id),
            'name': self.name,
            'date': self.birthdate and format_date(self.birthdate),
            'phone': self.phone,
            'gender': self.gender,
            'username': self.user.username,
            'email': self.user.email,
            'user_id': self.user.id,
            'image': self.image.url if self.image else None,
            'role': 'child',
            'perms': self.get_perms(),
        }

    def to_dict_show(self):
        return {
            'id': self.id,
            'name': self.name,
            'phone': self.phone,
            'image': self.image.url if self.image else None,
            'role': 'parent',
        }

    def get_parent_list(self):
        return [par.to_dict(self.id) for par in self.parents.all()]

    def get_display_name(self, user_id):
        try:
            prefs = self.user.display_prefs_for.get(to_user__id=user_id)
            return prefs.name
        except DisplayPrefs.DoesNotExist:
            return ''


class Parent(Model):
    user = OneToOneField(User, on_delete=CASCADE)
    name = CharField(max_length=SHORT_LEN)
    image = ImageField(null=True)
    birthdate = DateField(null=True)
    phone = CharField(max_length=PHONE_LEN)
    gender = CharField(max_length=Gender.Length,
                       choices=Gender.Choices,
                       null=True)
    confirmed = BooleanField(default=True)

    children = ManyToManyField(Child,
                               through='ParentChild',
                               related_name='parents')

    parents = ManyToManyField('self')

    subs = ManyToManyField(Sub, related_name='parents')

    def get_cur_subs(self):
        return self.subs.filter(Q(expires__gt=Now()) | Q(expires__isnull=True))

    def get_perms(self, child=None):
        subs = self.get_cur_subs()
        full_types = [SubType.FULL_1M, SubType.FULL_6M,
                      SubType.FULL_1Y, SubType.FULL_ALL]

        has_trial = not self.subs.exists()

        sub_types = [sub.type for sub in subs]

        if child is not None:
            task_limit = get_task_limit(self, child)
        else:
            task_limit = {ch.id: get_task_limit(self, ch) for ch in self.children.all()}

        return {
            'tasks': subs.filter(type__in=full_types + [SubType.TASKS]).exists(),
            'calendar': subs.filter(type__in=full_types + [SubType.CALENDAR]).exists(),
            'family': subs.filter(type__in=full_types + [SubType.FAMILY]).exists(),
            'history': subs.filter(type__in=full_types + [SubType.HISTORY]).exists(),
            'task_limit': task_limit,
            'hasTrial': has_trial,
            'subs': sub_types,
        }

    def has_family_perm(self):
        return self.get_perms()['family']

    def to_dict(self, child_id):
        task_list = [task.to_dict()
                     for task in self.tasks.filter(child_id=child_id,
                                                   confirm_time__isnull=True)
                         .filter(Q(time_limit__gt=Now()) | Q(time_limit__isnull=True))]
        gift_list = [gift.to_dict()
                     for gift in self.gifts.filter(child_id=child_id,
                                                   confirm_time__isnull=True)]
        points = ParentChild.objects.get(parent_id=self.id,
                                         child_id=child_id).points

        tasks = self.tasks.filter(child_id=child_id,
                                  confirm_time=None) \
            .filter(Q(time_limit__gt=Now()) | Q(time_limit__isnull=True)) \
            .count()
        tasks_done = self.tasks.filter(child_id=child_id,
                                       confirm_time__isnull=True,
                                       finish_time__isnull=False).count()
        gifts = self.gifts.filter(child_id=child_id,
                                  confirm_time__isnull=True).count()

        child = Child.objects.get(id=child_id)

        task_limit = None if self.get_perms()['tasks'] \
                         else get_task_limit(self, child)

        return {
            'id': self.id,
            'key': str(self.id),
            'name': self.name,
            'gender': self.gender,
            'display_name': self.get_display_name(child.user.id),
            'username': self.user.username,
            'user_id': self.user.id,
            'points': points,
            'tasks': tasks,
            'tasksDone': tasks_done,
            'gifts': gifts,
            'image': self.image.url if self.image else None,
            'taskList': task_list,
            'giftList': gift_list,
        }

    def to_dict_short(self, user_id):
        return {
            'id': self.id,
            'key': str(self.id),
            'name': self.name,
            'gender': self.gender,
            'display_name': self.get_display_name(user_id),
            'phone': self.phone,
            'user_id': self.user.id,
            'image': self.image.url if self.image else None,
            'role': 'parent',
        }

    def to_dict_profile(self):
        return {
            'id': self.id,
            'key': str(self.id),
            'name': self.name,
            'date': self.birthdate and format_date(self.birthdate),
            'phone': self.phone,
            'gender': self.gender,
            'username': self.user.username,
            'email': self.user.email,
            'user_id': self.user.id,
            'image': self.image.url if self.image else None,
            'role': 'parent',
            'perms': self.get_perms(),
        }

    def to_dict_show(self):
        return {
            'id': self.id,
            'name': self.name,
            'phone': self.phone,
            'image': self.image.url if self.image else None,
            'role': 'parent',
        }

    def get_child_list(self):
        return [ch.to_dict(self.id) for ch in self.children.all()]

    def get_display_name(self, user_id):
        try:
            prefs = self.user.display_prefs_for.get(to_user__id=user_id)
            return prefs.name
        except DisplayPrefs.DoesNotExist:
            return ''


class DisplayPrefs(Model):
    # display this user
    for_user = ForeignKey(User, related_name='display_prefs_for', on_delete=CASCADE)
    # to this user
    to_user = ForeignKey(User, related_name='display_prefs_to', on_delete=CASCADE)

    name = CharField(max_length=SHORT_LEN)


class ParentChild(Model):
    parent = ForeignKey(Parent, related_name='points', on_delete=CASCADE)
    child = ForeignKey(Child, related_name='points', on_delete=CASCADE)
    points = PositiveIntegerField(default=0)


class Task(Model):
    title = CharField(max_length=MID_LEN)
    desc = CharField(max_length=LONG_LEN, blank=True)
    child = ForeignKey(Child, related_name='tasks', on_delete=CASCADE)
    parent = ForeignKey(Parent, related_name='tasks', on_delete=CASCADE)
    reward = PositiveIntegerField()
    creation_time = DateTimeField(auto_now_add=True)
    time_limit = DateTimeField(null=True)
    finish_time = DateTimeField(null=True, default=None)
    confirm_time = DateTimeField(null=True, default=None)
    finish_image = ImageField(null=True)
    require_image = BooleanField(default=False)
    penalty_time = DateTimeField(null=True, default=None)
    repeat = PositiveSmallIntegerField(null=True, default=None)
    notify_time = DateTimeField(null=True, default=None)
    notify_repeat = PositiveSmallIntegerField(null=True, default=None)

    def to_dict(self):
        return {
            'id': self.id,
            'key': str(self.id),
            'title': self.title,
            'desc': self.desc,
            'child_id': self.child.id,
            'parent_id': self.parent.id,
            'reward': self.reward,
            'creation_time': self.creation_time,
            'time_limit': self.time_limit,
            'finish_time': self.finish_time,
            'done': self.finish_time is not None,
            'confirmed': self.confirm_time is not None,
            'finish_image': self.finish_image.url if self.finish_image else None,
            'require_image': self.require_image,
            'penalty_time': self.penalty_time,
            'repeat': self.repeat,
            'notify_time': self.notify_time,
            'notify_repeat': self.notify_repeat,
        }


class Gift(Model):
    title = CharField(max_length=MID_LEN)
    desc = CharField(max_length=LONG_LEN, blank=True)
    child = ForeignKey(Child, related_name='gifts', on_delete=CASCADE)
    parent = ForeignKey(Parent, related_name='gifts', on_delete=CASCADE)
    cost = PositiveIntegerField()
    creation_time = DateTimeField(auto_now_add=True)
    time_limit = DateTimeField(null=True)
    buying_time = DateTimeField(null=True, default=None)
    confirm_time = DateTimeField(null=True, default=None)
    image = ImageField(null=True)

    def to_dict(self):
        return {
            'id': self.id,
            'key': str(self.id),
            'title': self.title,
            'desc': self.desc,
            'child_id': self.child.id,
            'parent_id': self.parent.id,
            'cost': self.cost,
            'creation_time': self.creation_time,
            'time_limit': self.time_limit,
            'buying_time': self.buying_time,
            'bought': self.buying_time is not None,
            'confirmed': self.confirm_time is not None,
            'image': self.image.url if self.image else None,
        }


class Message(Model):
    text = CharField(max_length=LONG_LEN)
    user_from = ForeignKey(User,
                           related_name='messages_from',
                           on_delete=CASCADE)
    user_to = ForeignKey(User,
                         related_name='messages_to',
                         on_delete=CASCADE)
    creation_time = DateTimeField(auto_now_add=True)
    read = BooleanField(default=False)

    def to_dict(self):
        return {
            'id': self.id,
            'key': str(self.id),
            'from_id': self.user_from.id,
            'to_id': self.user_to.id,
            'text': self.text,
            'creation_time': self.creation_time,
            'read': self.read,
        }

    @staticmethod
    def get_unread_count(user_to, user_from):
        return Message.objects \
            .filter(user_to=user_to, user_from=user_from, read=False) \
            .count()


class Notif(Model):
    class Type:
        C_ADD_TASK = 1
        C_EDIT_TASK = 2
        C_REMIND_TASK = 3
        C_DELETE_TASK = 4
        C_CONFIRM_TASK = 5
        C_ADD_GIFT = 6
        C_EDIT_GIFT = 7
        C_DELETE_GIFT = 8
        C_CONFIRM_GIFT = 9
        P_DO_TASK = 10
        P_BUY_GIFT = 11
        P_OFFER_TASK = 12
        C_REJECT_TASK = 13
        P_REQUEST_TASK = 14
        C_AWARD = 15
        C_PENALTY = 16
        P_OFFER_GIFT = 17
        C_REJECT_GIFT = 18
        P_PROLONG_TASK = 19
        C_PROLONG_CONFIRM = 20
        C_PROLONG_REJECT = 21
        P_REMOVE_TASK = 22
        MESSAGE = 23
        TASK_EXPIRED = 24
        OFFER_LINK = 25
        ACCEPT_LINK = 26
        REJECT_LINK = 27

    user_from = ForeignKey(User,
                           related_name='notifs_from',
                           on_delete=CASCADE)
    user_to = ForeignKey(User,
                         related_name='notifs_to',
                         on_delete=CASCADE)
    type = SmallIntegerField()
    creation_time = DateTimeField(auto_now_add=True)
    read = BooleanField(default=False)
    data = JSONField(default=dict)

    def to_dict(self, user_id):
        sender = get_user_person(self.user_from)
        return {
            'id': self.id,
            'key': str(self.id),
            'from_id': self.user_from.id,
            'to_id': self.user_to.id,
            'type': self.type,
            'creation_time': self.creation_time,
            'read': self.read,
            'data': self.data,
            'from_user': sender.to_dict_short(user_id),
        }

    def save(self, *args, **kwargs):
        is_create = self.pk is None
        super().save(*args, **kwargs)
        if is_create:
            send_push(self)


def create_push(notif):
    person = get_user_person(notif.user_from)
    name = person.get_display_name(notif.user_to_id) or person.name
    if notif.type == Notif.Type.C_ADD_TASK:
        title = name + ' добавил задание'
        body = ''
    elif notif.type == Notif.Type.C_EDIT_TASK:
        title = name + ' изменил задание'
        body = ''
    elif notif.type == Notif.Type.C_REMIND_TASK:
        title = name + ' напоминает о задании'
        body = ''
    elif notif.type == Notif.Type.C_DELETE_TASK:
        title = name + ' отменил задание'
        body = ''
    elif notif.type == Notif.Type.C_CONFIRM_TASK:
        title = name + ' подтвердил выполнение задания'
        body = ''
    elif notif.type == Notif.Type.C_ADD_GIFT:
        title = name + ' добавил подарок'
        body = ''
    elif notif.type == Notif.Type.C_EDIT_GIFT:
        title = name + ' изменил подарок'
        body = ''
    elif notif.type == Notif.Type.C_CONFIRM_GIFT:
        title = name + ' подтвердил вручение подарка'
        body = ''
    elif notif.type == Notif.Type.C_DELETE_GIFT:
        title = name + ' удалил подарок'
        body = ''
    elif notif.type == Notif.Type.P_DO_TASK:
        title = name + ' выполнил задание'
        body = ''
    elif notif.type == Notif.Type.P_BUY_GIFT:
        title = name + ' купил подарок'
        body = ''
    elif notif.type == Notif.Type.P_OFFER_TASK:
        title = name + ' предложил задание'
        body = ''
    elif notif.type == Notif.Type.C_REJECT_TASK:
        title = name + ' отклонил добавление задания'
        body = ''
    elif notif.type == Notif.Type.P_REQUEST_TASK:
        title = name + ' просит придумать задание'
        body = ''
    elif notif.type == Notif.Type.C_AWARD:
        title = name + ' наградил Вас'
        body = ''
    elif notif.type == Notif.Type.C_PENALTY:
        title = name + ' оштрафовал Вас'
        body = ''
    elif notif.type == Notif.Type.P_OFFER_GIFT:
        title = name + ' предложил подарок'
        body = ''
    elif notif.type == Notif.Type.C_REJECT_GIFT:
        title = name + ' отклонил добавление подарка'
        body = ''
    elif notif.type == Notif.Type.P_PROLONG_TASK:
        title = name + ' просит продлить выполнение задания'
        body = ''
    elif notif.type == Notif.Type.C_PROLONG_CONFIRM:
        title = name + ' продлил выполнение задания'
        body = ''
    elif notif.type == Notif.Type.C_PROLONG_REJECT:
        title = name + ' отклонил продление задания'
        body = ''
    elif notif.type == Notif.Type.P_REMOVE_TASK:
        title = name + ' просит отменить задание'
        body = ''
    elif notif.type == Notif.Type.MESSAGE:
        title = name + ' отправил сообщение'
        body = ''
    elif notif.type == Notif.Type.OFFER_LINK:
        title = name + ' приглашает Вас в семью'
        body = ''
    elif notif.type == Notif.Type.ACCEPT_LINK:
        title = name + ' принял приглашение'
        body = ''
    elif notif.type == Notif.Type.REJECT_LINK:
        title = name + ' отклонил приглашение'
        body = ''
    else:
        return None
    return Notification(title=title, body=body)


def send_push(notif):
    print('send push', fcm)
    tokens = UserToken.objects.filter(user=notif.user_to,
                                      type=UserToken.Type.FcmDevice)
    tokens = [t.token for t in tokens]
    if not len(tokens):
        return
    push = create_push(notif)
    if not push:
        return

    msg_list = []
    for token in tokens:
        msg_list.append(messaging.Message(
            notification=push,
            token=token,
        ))
    res = messaging.send_all(msg_list)
    print(res)


class TempImage(Model):
    image = ImageField()


def format_date(d):
    return d.strftime('%d.%m.%Y')


def get_user_person(user):
    if hasattr(user, 'parent'):
        return user.parent
    elif hasattr(user, 'child'):
        return user.child
    else:
        return None


def get_task_limit(parent, child):
    limit_start = timezone.now() - timezone.timedelta(days=TASK_LIMIT_PERIOD)

    return TASK_LIMIT - parent.tasks.filter(child=child,
                                            creation_time__gt=limit_start) \
        .count()
