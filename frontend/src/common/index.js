import React, {Component} from 'react';
import {Alert, Text as DefaultText, Image, View, StyleSheet,
        TouchableOpacity, Platform, CheckBox, Switch} from 'react-native';
import { imageUrl } from '../backend/data';
import moment from 'moment';
import momentRu from 'moment/locale/ru';
moment.locale('ru');

export function cutOne(x){
  return Math.min(1, Math.max(0, x))
}

export function getRandInt(min, max) {
  return Math.floor(Math.random() * (max - min) ) + min;
}

export function networkAlert(){
  Alert.alert(
    'Ошибка сети',
    'Не удалось подключиться к серверу',
    [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );
}

export var GemIcon = (props) => (
  <Image source={require('res/icons/gem_1.png')}
    style={props.style}
  />
)

export const backImage = require('res/drawable/bg_kids.png');

export var colors = {
  background: '#fff',
  button: '#b9d84e',
  topBar: '#b4cd7c',
  menuButton: '#a6da30',
  underline: '#b9d84e',
  separator: '#ccc',
  red: '#f83a2c',
  yellow: '#f9b81c',
  bottomMenu: '#fafafa',
  message: '#CBF180',
  hyperlink: '#aaa',
  errorText: '#ff0000',
  checkbox: {
    active: '#f9b81c',
    inactive: '#ccc',
  },
  childCard: {
    background: '#fff',
    font1: 'black',
    font2: '#753906',
  },
  task: {
    background: '#CBF180',
  },
  gift: {
    background: '#CBF180',
  },
  chat: {
    background: '#CBF180',
  },
  bottomMenu: {
    background: '#fafafa',
  },
  textInput: {
    border: '#ccc'
  },
  dialog: {
    border: '#aaa'
  }
}

export var styles = {
  textInput: {
    borderWidth: 2,
    borderColor: colors.textInput.border,
    borderRadius: 17,
    fontSize: 16,
    paddingLeft: 6,
    paddingRight: 6,
    paddingTop: 2,
    paddingBottom: 2,
    marginTop: 5,
    marginBottom: 10,
  },
  textInputReg: {
    borderWidth: 2,
    borderColor: colors.textInput.border,
    borderRadius: 100,
    fontSize: 15,
    paddingLeft: 6,
    paddingRight: 6,
    // height: 22,
    paddingBottom: 2,
    paddingTop: 2,
  },
  fieldText: {
    marginLeft: 5,
    marginTop: 5,
    marginBottom: 5,
    color: 'black',
  },
  fieldTextReg: {
    marginLeft: 5,
    marginTop: 5,
    marginBottom: 5,
    color: 'black',
  },
  dlgButton: {
    borderWidth: 3,
    borderColor: colors.menuButton,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    paddingLeft: 12,
    paddingRight: 12,
    paddingTop: 8,
    paddingBottom: 8,
  },
  settings: {
    button: {
      width: '100%',
      height: 50,
      borderWidth: 3,
      borderColor: colors.menuButton,
      borderRadius: 25,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      paddingLeft: 10,
      paddingRight: 10,
      marginTop: 10,
      marginBottom: 10,
    }
  }
}

export var Text = props => {
  let fontFamily;
  
  if (props.style && props.style.fontWeight == 'bold'){
    fontFamily = 'Rotonda Bold'
  }
  else {
    fontFamily = 'DIN Round Pro'
  }
  return (
    <DefaultText {...props}
      style={[
        {
          color: '#222',
          fontSize: 14,
        },
        props.style,
        {
          fontFamily: fontFamily,
          fontWeight: "normal",
        }
      ]}
    >
      {props.children}
    </DefaultText>
  )
}

export class TaskProgress extends Component{
  render(){
    let width = this.props.width || 50
    let height = 8
    let fillRate = this.props.fillRate != undefined ? 
      this.props.fillRate : 0.5

    return (
      <View style={[this.props.style, {
        width: width,
        height: height,
        borderRadius: height / 2,
        backgroundColor: '#fff',
        borderColor: '#333',
      }]}>
        <View style={{
          width: width * fillRate,
          height: '100%',
          borderRadius: height / 2,
          backgroundColor: '#9ED62F',

        }}>

        </View>
      </View>
    )
  }
}

export class LogoTitleBar extends Component {
  render() {
    let hasMenu = this.props.menu !== false

    return (
      <View style={{height: 55}}>
        <View style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          padding: 7,
          backgroundColor: colors.topBar,
        }}>
          <Image source={require('res/drawable/LogoTitle.png')}
            style={{height: '100%', width: '100%', resizeMode: 'contain'}}
          />
        </View>
        { hasMenu && 
          <TouchableOpacity style={{height: '100%', width: 28, borderWidth: 0}}
            onPress={() => this.props.navigation.openDrawer()}
          >
            <Image source={require('res/icons/Menu/Button_menu_2.png')}
              style={{
                position: 'absolute',
                height: '100%', width: '100%',
                resizeMode: 'contain'
              }}
            />
          </TouchableOpacity>
        }
      </View>
    )
  }
}

export const getEmptyAvatar = (role, gender, white) => {
  if (role === 'parent') {
    if (white) {
      if (gender === 'M'){
        return require('res/icons/Avatars/User_white.png')
      } else {
        return require('res/icons/Avatars/User_white_woman.png')
      }
    } else {
      if (gender === 'M'){
        return require('res/icons/Avatars/User_green.png')
      } else {
        return require('res/icons/Avatars/User_green_woman.png')
      }
    }
  } else {
    if (white) {
      if (gender === 'M'){
        return require('res/icons/Avatars/User_child_white.png')
      } else {
        return require('res/icons/Avatars/User_child_white_girl.png')
      }
    } else {
      if (gender === 'M'){
        return require('res/icons/Avatars/User_child_green.png')
      } else {
        return require('res/icons/Avatars/User_child_green_girl.png')
      }
    }
  }
}

export const getAvatar = (uri, role, gender, white) => {
  if (uri){
    return {uri: imageUrl(uri)}
  } else {
    return getEmptyAvatar(role, gender, white)
  }
}

export const BoolInput = ({style, ...props}) => {
  return Platform.select({
    android:  
      <CheckBox
        style={style}
        {...props}
      />,
              
    ios:
      <Switch
        style={[{transform: [{ scaleX: .8 }, { scaleY: .8 }] }, style]}
        {...props}
      />,
  })
}

const isIos = Platform.OS == "ios";
const isAndroidLollipop = Platform.Version >= 21 && Platform.Version < 23;

export const RoundImage = (props) => {
  if (isIos || !isAndroidLollipop) {
    return <Image {...props}/>; 
  } else {
    const style = StyleSheet.flatten(props.style);

    return (
      <View style={StyleSheet.flatten([style, {overflow: "hidden", alignItems: "center", justifyContent: "center"}])}>
        <Image source={props.source} style={StyleSheet.flatten({width: style.width, height: style.height})}/>
      </View>
    );
  }
}

export const dateInputAssist = (prevDate, date) => {
  if ([1, 2, 4, 5, 7, 8, 9, 10].includes(date.length) &&
        isNaN(parseInt(date.slice(-1), 10))){
    date = date.slice(0, -1);
  }
  if ([2, 5].includes(date.length)){
    if (prevDate.length < date.length){
      date += '.';
    } else {
      date = date.slice(0, -1)
    }
  }
  if (date.length > 10){
    date = date.slice(0, 10);
  }

  return date;
}

export const timeInputAssist = (prevDate, date) => {
  if ([1, 2, 4, 5].includes(date.length) &&
        isNaN(parseInt(date.slice(-1), 10))){
    date = date.slice(0, -1);
  }
  if ([2].includes(date.length)){
    if (prevDate.length < date.length){
      date += ':';
    } else {
      date = date.slice(0, -1)
    }
  }
  if (date.length > 5){
    date = date.slice(0, 5);
  }

  return date;
}

export const datetimeInputAssist = (prevDate, date) => {
  if (date.length === 10) {
    if (prevDate.length < date.length){
      date += ' ';
    } else {
      date = date.slice(0, -1)
    }
  }
  if (date.length > 16){
    date = date.slice(0, 16);
  }
  let datepart = date.slice(0, 10);
  let prevdatepart = prevDate.slice(0, 10);
  datepart = dateInputAssist(prevdatepart, datepart)
  let timepart = date.slice(11, 16);
  if (timepart) {
    prevtimepart = prevDate.slice(11, 16);
    timepart = timeInputAssist(prevtimepart, timepart)
  }
  return datepart + (date.length > 10 ? ' ' + timepart : '');
}

export const asUTC = (datetime) => {
  if (datetime.includes('Z')){
    return datetime;
  } else {
    return datetime + 'Z'
  }
}

export const formatDate = (datetime) => {
  return moment.utc(datetime).local().format('DD.MM.YYYY')
}

export const formatTime = (datetime) => {
  return moment.utc(datetime).local().format('HH:mm')
}

export const formatDateTime = (datetime) => {
  return moment.utc(datetime).local().format('DD.MM.YYYY HH:mm')
}

export const timeDiff = (t1, t2) => {
  let d = moment.utc(t1).diff(moment.utc(t2))
  d = moment.duration(d)
  if (d.days() === 0 && d.hours() !== 0){
    return moment.duration(d.hours(), 'hours').humanize()
  }
  else if (d.days() !== 0 && d.hours() === 0){
    return moment.duration(d.days(), 'days').humanize()
  }
  else {
    return moment.duration(d.days(), 'days').humanize() + ' ' + 
            moment.duration(d.hours(), 'hours').humanize()
  }
}

export const tasksText = (n) => {
  if (n >= 2 && n <= 4) {
    return 'задания'
  } else if (n === 1) {
    return 'задание'
  } else {
    return 'заданий'
  }
}
