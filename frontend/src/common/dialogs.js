import React, {Component, useState, useEffect} from 'react';
import {Platform, StyleSheet, View, Modal, StatusBar, Linking,
  FlatList, TextInput, Image, TouchableOpacity, ImageBackground,
  Dimensions} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';
import IconAnt from 'react-native-vector-icons/AntDesign';
import { Text, colors, styles, networkAlert } from 'common';
import { awardChild, penaltyChild, setDisplayPrefs } from 'backend/data';
import { imageUrl, unlinkUser, requestProlongTask, requestRemoveTask, linkUser, rejectLinkUser, LINKS, getStoreLink } from '../backend/data';
import { confirmReg, sendRegSMS } from '../backend/auth';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { RoundImage, getRandInt } from '.';
import {getMainSubPrice, buyMainSub} from '../screens/shared/IAP';
import { ScrollView } from 'react-native-gesture-handler';

export const AwardDialog = ({visible, setVisible, child_id, like, task_id}) => {
  let [amount, setAmount] = useState('');
  let [amountError, setAmountError] = useState(false);

  let markAmountError = () => (
    amountError ? {borderColor: 'red', borderWidth: 2} : {}
  )

  let onSubmit = () => {
    
    let numAmount = Number(amount);
    
    if (!isNaN(numAmount) && numAmount > 0){
      onClose()
      setAmountError(false);
      if (like) {
        awardChild(child_id, numAmount, task_id, (res, error) => {
          
        })
      }
      else {
        penaltyChild(child_id, numAmount, task_id, (res, error) => {
          
        })
      }
    }
    else {
      setAmountError(true)
    }
  }

  let onClose = () => {
    setVisible(false);
    setAmount('');
    setAmountError(false);
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onDismiss={onClose}
      onRequestClose={() => {
        console.log('request close')
        onClose();
    }}>
      <KeyboardAwareScrollView
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}
        contentContainerStyle={{flex: 1}}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={true}
        keyboardShouldPersistTaps="handled"
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 20,
            borderRadius: 12,
            backgroundColor: colors.background,
            elevation: 2,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
          }}>
            <View style={{alignItems: 'center'}}>
              {like ?
                <Image source={require('res/icons/Like.png')}
                  style={{width: 85, height: 85}}
                />
                :
                <Image source={require('res/icons/Dislike.png')}
                  style={{width: 85, height: 85}}
                />
              }
              <View style={{flexDirection: 'row',
                alignItems: 'center', width: '100%',
                marginTop: 15,
              }}>
                <Text style={{marginBottom: 5}}>
                  {like ? 'Премия' : 'Штраф'}
                </Text>
                <TextInput
                  style={[styles.textInput,
                        {marginLeft: 15, flex: 1},
                        markAmountError()]}
                  onChangeText={setAmount}
                >
                  {amount}
                </TextInput>
              </View>
              <View style={{flexDirection: 'row',
                alignItems: 'center', width: '100%',
                marginTop: 15,
                justifyContent: 'space-around',
                paddingLeft: 20, paddingRight: 20,
              }}>
                <TouchableOpacity
                  style={[styles.dlgButton, {width: 65}]}
                  onPress={onClose}
                >
                  <Text style={{
                    color: 'black',
                  }}>Нет</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.dlgButton, {width: 65}]}
                  onPress={onSubmit}
                >
                  <Text style={{
                    color: 'black',
                  }}>Да</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </Modal>
  )
}

export const TextAlertDialog = ({visible, setVisible, onConfirm, text, singleButton, onReject}) => {

  let onClose = () => {
    setVisible(false);
  }

  let onSubmit = () => {
    onClose();
    if (onConfirm){
      onConfirm();
    }
  }

  let reject = () => {
    onClose();
    if (onReject){
      onReject();
    }
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onDismiss={onClose}
      onRequestClose={() => {
        console.log('request close')
        onClose();
      }}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{
          padding: 20,
          borderRadius: 12,
          backgroundColor: colors.background,
          elevation: 2,
          margin: 20,
          borderWidth: 2,
          borderColor: colors.dialog.border,
        }}>
          <View style={{alignItems: 'center'}}>
            <Text style={{textAlign: 'center'}}>
              {text}
            </Text>
            
            <View style={{flexDirection: 'row',
              alignItems: 'center', width: '100%',
              marginTop: 20,
              justifyContent: 'space-around',
              paddingLeft: 20, paddingRight: 20,
            }}>
              { !singleButton &&
                <TouchableOpacity
                  style={[styles.dlgButton, {width: 65}]}
                  onPress={reject}
                >
                  <Text style={{
                    color: 'black',
                  }}>Нет</Text>
                </TouchableOpacity>
              }
              <TouchableOpacity
                style={[styles.dlgButton, {width: 65}]}
                onPress={onSubmit}
              >
                <Text style={{
                  color: 'black',
                }}>
                  { singleButton ? 'OK' : 'Да' }
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  )
}

export const UpdateDialog = ({visible}) => {
  const onSubmit = () => {
    Linking.openURL(getStoreLink());
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
    >
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{
          padding: 20,
          borderRadius: 12,
          backgroundColor: colors.background,
          elevation: 2,
          margin: 20,
          borderWidth: 2,
          borderColor: colors.dialog.border,
        }}>
          <View style={{alignItems: 'center'}}>
            <Text style={{textAlign: 'center', marginBottom: 10}}>
              Пожалуйста, обновите версию приложения
            </Text>
            <TouchableOpacity
              style={[styles.dlgButton]}
              onPress={onSubmit}
            >
              <Text style={{
                color: 'black',
              }}>Обновить</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  )
}

export const EarnDialog = ({visible, setVisible, onOffer, onRequest}) => {

  let onClose = () => {
    setVisible(false);
  }

  let onSubmitOffer = () => {
    onClose();
    onOffer();
  }

  let onSubmitRequest = () => {
    onClose();
    onRequest();
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onDismiss={onClose}
      onRequestClose={() => {
        console.log('request close')
        onClose();
      }}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{
          paddingLeft: 10, paddingRight: 10,
          paddingTop: 20, paddingBottom: 20,

          borderRadius: 12,
          backgroundColor: colors.background,
          elevation: 2,
          margin: 20,
          borderWidth: 2,
          borderColor: colors.dialog.border,
        }}>
          <View style={{alignItems: 'center'}}>
            <Image source={require('res/icons/Add.png')}
              style={{width: 85, height: 85, marginBottom: 15}}
            />
            <Text style={{textAlign: 'center'}}>
              Как Вы хотите заработать?
            </Text>
            
            <View style={{flexDirection: 'row',
              alignItems: 'center', width: '100%',
              marginTop: 20,
              justifyContent: 'space-around',
            }}>
              <TouchableOpacity
                style={[styles.dlgButton, {flex: 1}]}
                onPress={onSubmitRequest}
              >
                <Text style={{
                  color: 'black',
                  textAlign: 'center',
                  paddingLeft: 3, paddingRight: 3,
                  paddingTop: 5, paddingBottom: 5,
                }}>
                  Придумай мне задание
                </Text>
              </TouchableOpacity>
              
              <TouchableOpacity
                style={[styles.dlgButton, {
                  flex: 1, marginLeft: 10,
                  paddingLeft: 0, paddingRight: 0,
                }]}
                onPress={onSubmitOffer}
              >
                <Text style={{
                  color: 'black',
                  textAlign: 'center',
                  paddingLeft: 4, paddingRight: 4,
                  paddingTop: 5, paddingBottom: 5,
                }}>
                  Я придумал задание
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity onPress={onClose}
            style={{position: 'absolute', right: 14, top: 14}}
          >
            <IconAnt name='close' size={20} color='#555' />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  )
}

export const PhotoDialog = ({visible, setVisible, setPhoto}) => {

  const onCamera = () => {
    setVisible(false);

    const options = {
      title: 'Select Avatar',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      // storageOptions: {
      //   skipBackup: true,
      //   path: 'images',
      // },
      mediaType: 'photo',
      maxWidth: 1024,
      maxHeight: 1024,
      // noData: true,
      quality: 0.5,
    };

    setTimeout(() => {

      ImagePicker.launchCamera(options, (res) => {
        // console.log('take photo', res)
        if (res.uri) {
          // console.log('photo taken');
          // console.log(res.uri);
          setPhoto(res);
        }
        
      })
    }, 50)
  }

  const onGalery = () => {
    setVisible(false);

    const options = {
      title: 'Select Avatar',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      // storageOptions: {
      //   skipBackup: true,
      //   path: 'images',
      // },
      mediaType: 'photo',
      maxWidth: 1024,
      maxHeight: 1024,
      // noData: true,
      quality: 0.5,
    };

    setTimeout(() => {

      ImagePicker.launchImageLibrary(options, (res) => {
        // console.log('take photo', res)
        if (res.uri) {
          // console.log('photo taken');
          // console.log(res.uri);
          setPhoto(res);
        }
        
      })
    }, 50)
  }

  let onClose = () => {
    setVisible(false);
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onDismiss={onClose}
      onRequestClose={() => {
        console.log('request close')
        onClose()
      }
    }>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{
          padding: 30,
          borderRadius: 12,
          backgroundColor: colors.background,
          elevation: 2,
          margin: 20,
          borderWidth: 2,
          borderColor: colors.dialog.border,
          paddingLeft: 10,
          paddingRight: 10,
        }}>
          <View style={{
            flexDirection: 'row', 
          }}>
            <TouchableOpacity onPress={onCamera}
              style={{flex: 1, alignItems: 'center'}}
            >
              <Image source={require('res/icons/Add_photo_confirm.png')}
                style={{width: 100, height: 100, marginBottom: 10}}
              />
              <Text>Камера</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onGalery}
              style={{flex: 1, alignItems: 'center'}}
            >
              <Image source={require('res/icons/Gallery.png')}
                style={{width: 100, height: 100, marginBottom: 10}}
              />
              <Text>Галерея</Text>
            </TouchableOpacity>

          </View>
          <TouchableOpacity onPress={onClose}
            style={{position: 'absolute', right: 14, top: 14}}
          >
            <IconAnt name='close' size={20} color='#555' />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  )
}

export const DisplayPrefsDialog = ({visible, setVisible, user}) => {

  const [name, setName] = useState(user.display_name || user.name);
  const [unlinkVisible, setUnlinkVisible] = useState(false);

  onClose = () => {
    setVisible(false)
  }

  onSubmit = () => {
    setDisplayPrefs(user.user_id, name, (res, error) => {
      setVisible(false)
    })
  }

  onUnlink = () => {
    unlinkUser(user.user_id, (res, error) => {
      if (!error) {
        setVisible(false);
      }
    })
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onRequestClose={() => {
        console.log('request close')
        setVisible(false);
      }
    }>
      <KeyboardAwareScrollView
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}
        contentContainerStyle={{flex: 1}}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={true}
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 30,
            borderRadius: 12,
            backgroundColor: colors.background,
            elevation: 2,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
          }}>
            <View style={{
              alignItems: 'center'
            }}>
              <RoundImage source={{uri: imageUrl(user.image)}}
                style={{width: 100, height: 100, borderRadius: 50}}
              />
              <Text style={{fontSize: 16, margin: 10}}>
                {user.name}
              </Text>
              <Text style={{fontSize: 14, margin: 10}}>
                Отображать как
              </Text>
              <TextInput
                style={[styles.textInput,
                      {width: '100%'}
                      ]}
                onChangeText={setName}
              >
                {name}
              </TextInput>
              <View style={{
                width: '100%',
                flexDirection: 'row', justifyContent: 'space-between'
              }}>
                <TouchableOpacity
                  style={[styles.dlgButton]}
                  onPress={() => setUnlinkVisible(true)}
                >
                  <Text style={{
                    color: 'black',
                  }}>Удалить</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.dlgButton]}
                  onPress={onSubmit}
                >
                  <Text style={{
                    color: 'black',
                  }}>Сохранить</Text>
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity onPress={onClose}
              style={{position: 'absolute', right: 14, top: 14}}
            >
              <IconAnt name='close' size={20} color='#555' />
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
      <TextAlertDialog visible={unlinkVisible}
        setVisible={setUnlinkVisible}
        onConfirm={onUnlink}
        text={'Удалить пользователя из семьи?'}
      />
    </Modal>
  )
}

export const ImageShowDialog = ({visible, setVisible, image}) => {

  let onClose = () => {
    setVisible(false);
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onRequestClose={() => {
        console.log('request close')
        onClose();
      }}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{
          padding: 20,
          paddingLeft: 10,
          paddingRight: 10,
          borderRadius: 12,
          backgroundColor: colors.background,
          elevation: 2,
          margin: 20,
          borderWidth: 2,
          borderColor: colors.dialog.border,
        }}>
          <View style={{alignItems: 'center'}}>
            <Image source={{uri: imageUrl(image)}}
              style={{width: '100%', height: 400, resizeMode: 'contain'}}
            />
            
            <View style={{flexDirection: 'row',
              alignItems: 'center', width: '100%',
              marginTop: 15,
              justifyContent: 'space-around',
              paddingLeft: 20, paddingRight: 20,
            }}>
              <TouchableOpacity
                style={[styles.dlgButton, {}]}
                onPress={onClose}
              >
                <Text style={{
                  color: 'black',
                }}>Закрыть</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  )
}

export const ConfirmRegDialog = ({visible, setVisible, login, password, onDone}) => {

  const [token, setToken] = useState('');
  const [wrong, setWrong] = useState(false);

  onSubmit = () => {
    let _token = token.trim();
    if (_token.length > 0){
      confirmReg(login, password, _token, (res, error) => {
        if (!error){
          setVisible(false)
          onDone()
        }
        else {
          setWrong(true);
          setToken('');
        }
      })
    }
  }

  onSend = () => {
    sendRegSMS(login, password, (res, error) => {
      setWrong(false)
    })
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onRequestClose={() => {
      }
    }>
      <KeyboardAwareScrollView
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}
        contentContainerStyle={{flex: 1}}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={true}
        keyboardShouldPersistTaps="handled"
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 30,
            borderRadius: 12,
            backgroundColor: colors.background,
            elevation: 2,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
          }}>
            <View style={{
              alignItems: 'center'
            }}>
              { !wrong && 
                <Text style={{fontSize: 14, margin: 10}}>
                  Введите код из СМС
                </Text>
              }
              { !wrong && 
                <TextInput
                  style={[styles.textInput,
                        {width: '100%'}
                        ]}
                  onChangeText={setToken}
                >
                  {token}
                </TextInput>
              
              }
              { wrong &&
                <Text>Код введен неверно</Text>
              }
              <View style={{
                width: '100%', justifyContent: 'space-between'
              }}>
                {!wrong && 
                  <TouchableOpacity
                    style={[styles.dlgButton, {}]}
                    onPress={onSubmit}
                  >
                    <Text style={{
                      color: 'black',
                    }}>Подтвердить</Text>
                  </TouchableOpacity>
                }
                <TouchableOpacity
                  style={[{
                    marginTop: 15,
                  }]}
                  onPress={onSend}
                >
                  <Text style={{
                    color: '#aaa', 
                    textDecorationLine: 'underline',
                    textAlign: 'center',
                  }}>Отправить повторно</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </Modal>
  )
}

export const ProlongDialog = ({visible, setVisible, task}) => {
  let [days, setDays] = useState('');
  let [errors, setErrors] = useState({});
  let [hours, setHours] = useState('');

  let markError = (name) => (
    errors[name] ? {borderColor: 'red', borderWidth: 2} : {}
  )

  let onSubmit = () => {
    let errors = {}

    let h = hours.trim();
    if (h){
      h = Number(h)
      if (isNaN(h) || h < 0 || h > 24){
        errors.hours = true;
      }
    }
    else {
      h = 0
    }
    let d = days.trim();
    if (d){
      d = Number(d)
      if (isNaN(d) || d < 0 || d > 1000){
        errors.days = true;
      }
    }
    else {
      d = 0
    }
    
    if (!h && !d && JSON.stringify(errors) === '{}'){
      errors.hours = true;
      errors.days = true;
    }

    setErrors(errors)

    if (JSON.stringify(errors) === '{}'){
      let time = moment.utc(task.time_limit)
      time.add(d, 'days');
      time.add(h, 'hours');

      let data = {
        task_id: task.id,
        time_limit: time.utc().toISOString(),
      }

      requestProlongTask(data, (res, error) => {
        if (!error){
          onClose()
        }
      })
    }
  }

  let onClose = () => {
    setVisible(false);
    setDays('');
    setHours('');
    setErrors({})
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onDismiss={onClose}
      onRequestClose={() => {
        console.log('request close')
        onClose();
    }}>
      <KeyboardAwareScrollView
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}
        contentContainerStyle={{flex: 1}}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={true}
        keyboardShouldPersistTaps="handled"
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 20,
            borderRadius: 12,
            backgroundColor: colors.background,
            elevation: 2,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
          }}>
            <View style={{alignItems: 'center'}}>
              <Text style={{textAlign: 'center'}}>
                На какое время Вы хотите продлить выполнение задания?
              </Text>
              <View style={{
                marginTop: 10,
                flexDirection: 'row'
              }}>
                <TextInput
                  style={[styles.textInput,
                        {flex: 1},
                        markError('days')]}
                  onChangeText={setDays}
                  placeholder='Дни'
                >
                  {days}
                </TextInput>
                <TextInput
                  style={[styles.textInput,
                        {flex: 1, marginLeft: 10},
                        markError('hours')]}
                  onChangeText={setHours}
                  placeholder='часы'
                >
                  {hours}
                </TextInput>
              </View>
              <View style={{flexDirection: 'row',
                alignItems: 'center', width: '100%',
                marginTop: 10,
                justifyContent: 'space-around',
              }}>
                <TouchableOpacity
                  style={[styles.dlgButton, {flex: 1}]}
                  onPress={onClose}
                >
                  <Text style={{
                    color: 'black',
                  }}>Закрыть</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.dlgButton, {flex: 1, marginLeft: 10}]}
                  onPress={onSubmit}
                >
                  <Text style={{
                    color: 'black',
                  }}>Подтвердить</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </Modal>
  )
}

export const RequestRemoveDialog = ({visible, setVisible, task}) => {
  let [reason, setReason] = useState('');
  let [errors, setErrors] = useState({});

  let markError = (name) => (
    errors[name] ? {borderColor: 'red', borderWidth: 2} : {}
  )

  let onSubmit = () => {
    let errors = {}

    let r = reason.trim();
    if (!r){
      errors.reason = true;
    }

    setErrors(errors)

    if (JSON.stringify(errors) === '{}'){

      let data = {
        task_id: task.id,
        reason: r,
      }

      requestRemoveTask(data, (res, error) => {
        if (!error){
          onClose()
        }
      })
    }
  }

  let onClose = () => {
    setVisible(false);
    setReason('');
    setErrors({});
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onDismiss={onClose}
      onRequestClose={() => {
        console.log('request close')
        onClose();
    }}>
      <KeyboardAwareScrollView
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}
        contentContainerStyle={{flex: 1}}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={true}
        keyboardShouldPersistTaps="handled"
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 20,
            borderRadius: 12,
            backgroundColor: colors.background,
            elevation: 2,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
          }}>
            <View style={{alignItems: 'center'}}>
              <Text style={{textAlign: 'center'}}>
                Укажите причину отмены задания
              </Text>
              <View style={{
                marginTop: 10,
                flexDirection: 'row'
              }}>
                <TextInput
                  style={[styles.textInput,
                        {flex: 1},
                        markError('reason')]}
                  onChangeText={setReason}
                >
                  {reason}
                </TextInput>
                
              </View>
              <View style={{flexDirection: 'row',
                alignItems: 'center', width: '100%',
                marginTop: 10,
                justifyContent: 'space-around',
              }}>
                <TouchableOpacity
                  style={[styles.dlgButton, {flex: 1}]}
                  onPress={onClose}
                >
                  <Text style={{
                    color: 'black',
                  }}>Закрыть</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.dlgButton, {flex: 1, marginLeft: 10}]}
                  onPress={onSubmit}
                >
                  <Text style={{
                    color: 'black',
                  }}>Подтвердить</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </Modal>
  )
}

export const AcceptLinkDialog = ({visible, setVisible, user}) => {

  let onSubmit = () => {
    linkUser(user.role, user.id, (res, error) => {
      onClose();
    })
  }

  let onReject = () => {
    rejectLinkUser(user.role, user.id, (res, error) => {
      onClose();
    })
  }

  let onClose = () => {
    setVisible(false);
  }

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onDismiss={onClose}
      onRequestClose={() => {
        console.log('request close')
        onClose();
    }}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 20,
            borderRadius: 12,
            backgroundColor: colors.background,
            elevation: 2,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
            alignItems: 'center',
          }}>
            {/* <RoundImage
              style={{width: 110, height: 110,
                borderRadius: 55,
              }}
              source={{ uri: imageUrl(user.image)}}
            /> */}
            <Text style={{color: 'black', fontSize: 16, marginTop: 10}}>
              {user.name}
            </Text>
            <Text style={{color: 'black', fontSize: 16, marginTop: 10}}>
              {user.phone}
            </Text>
            <View style={{flexDirection: 'row',
              alignItems: 'center', width: '100%',
              marginTop: 15,
              justifyContent: 'space-around',
            }}>
              <TouchableOpacity
                style={[styles.dlgButton, {flex: 1}]}
                onPress={onReject}
              >
                <Text style={{
                  color: 'black',
                }}>Отклонить</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.dlgButton, {flex: 1, marginLeft: 10}]}
                onPress={onSubmit}
              >
                <Text style={{
                  color: 'black',
                }}>Добавить</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  )
}

export const ParentalGateDialog = ({visible, setVisible, onConfirm, onReject}) => {

  let [expr, setExpr] = useState('')
  let [input, setInput] = useState('')
  let [answer, setAnswer] = useState('')

  let checkAnswer = () => {
    return answer === input
  }

  let onClose = () => {
    setVisible(false);
  }

  let onSubmit = () => {
    if (input.length) {
      onClose();
      if (checkAnswer()) {
        if (onConfirm){
          onConfirm();
        }
      } else {
        if (onReject){
          onReject();
        }
      }
    }
  }

  let reject = () => {
    onClose();
    if (onReject){
      onReject();
    }
  }

  useEffect(() => {
    if (visible){
      let a = getRandInt(10, 100);
      let b = getRandInt(-a + 11, 100);
      setAnswer(a + b + '')
      let sign = b >= 0 ? '+' : '-';
      b = Math.abs(b)
      let str = a + ' ' + sign + ' ' + b;
      setExpr(str);
    }
    else {
      setExpr('')
      setAnswer('')
      setInput('')
    }
  }, [visible])

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      // onDismiss={reject}
      // onRequestClose={() => {
      //   // reject();
      // }}
      >
      <KeyboardAwareScrollView
        style={{
          flex: 1,
        }}
        contentContainerStyle={{flex: 1}}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={true}
        keyboardShouldPersistTaps='handled'
      >
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 20,
            borderRadius: 12,
            backgroundColor: colors.background,
            elevation: 2,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
          }}>
            <View style={{alignItems: 'center'}}>
              <Text style={{textAlign: 'center', fontSize: 18}}>
                Спросите у родителя
              </Text>
              <Text style={{textAlign: 'center', fontSize: 12, marginTop: 20}}>
                Уважаемый родитель! Для регистрации ребенка в приложении KidsWork необходимо Ваше подтверждение.
                Вы разрешаете ребенку использовать приложение согласно <Text style={{color: colors.hyperlink, fontSize: 12}}
                  onPress={() => Linking.openURL(LINKS.RULES)}
                >
                  правилам пользования приложением KidsWork
                </Text> и даете согласие на <Text style={{color: colors.hyperlink, fontSize: 12}}
                  onPress={() => Linking.openURL(LINKS.AGREEMENT)}
                >
                  обработку персональных данных
                </Text> ребенка
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'center',
                marginTop: 20,
              }}>
                <Text style={{fontSize: 18}}>
                  {expr + ' = '}
                </Text>
                <TextInput
                  style={[styles.textInput,
                          {width: 100, marginTop: 0, marginBottom: 0},
                  ]}
                  value={input} onChangeText={setInput}
                />
              </View>
              <View style={{flexDirection: 'row',
                alignItems: 'center', width: '100%',
                marginTop: 20,
                justifyContent: 'space-around',
              }}>
                <TouchableOpacity
                  style={[styles.dlgButton, {flex: 1}]}
                  onPress={reject}
                >
                  <Text style={{
                    color: 'black',
                  }}>Отмена</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.dlgButton, {flex: 1, marginLeft: 10}]}
                  onPress={onSubmit}
                >
                  <Text style={{
                    color: 'black',
                  }}>
                    OK
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </Modal>
  )
}

export class PromoSubDialog extends Component {
  constructor(props){
    super(props);

    this.state = {
      price: '99 р',
    }

    this.onClose = this.onClose.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    const fetchPrice = () => {
      let price = getMainSubPrice()
      console.log('promo price', price)
      let p = price.replace(',00 ', ' ')
      this.setState({price: p})
    }

    fetchPrice()
    this._interval = setInterval(fetchPrice, 500)
  }

  componentWillUnmount() {
    clearInterval(this._interval)
  }

  onClose() {
    this.props.setVisible(false);
  }

  onSubmit() {
    buyMainSub();
    this.onClose();
  }

  render() {
    let {visible} = this.props;
    let imageWidth = Dimensions.get('window').width - 40;
    let imageHeight = imageWidth * 667 / 1000;

    let text = '\u2022 Неограниченные задания\n'+
              '\u2022 Ограничения времени\n'+
              '\u2022 Автоповтор заданий\n'+
              '\u2022 Раздел семьи\n'+
              '\u2022 История заданий\n'

    return (
      <Modal
        animationType='slide'
        transparent={true}
        visible={visible}
        onDismiss={this.onClose}
        onRequestClose={() => {
          console.log('request close')
          this.onClose();
        }}>
        <View style={{flex: 1, justifyContent: 'center', backgroundColor: '#00000055'}}>
          <View style={{
            borderRadius: 12,
            backgroundColor: 'white',
            // elevation: 2,
            margin: 20,
            // borderWidth: 2,
            // borderColor: colors.dialog.border,
          }}>
            <Image source={require('res/drawable/promo_bg.jpg')}
              style={{width: imageWidth, height: imageHeight, borderRadius: 12}}
            />
            <ScrollView>
              <View style={{
                alignItems: 'center',
                paddingLeft: 10, paddingRight: 10, paddingBottom: 10,
                // marginTop: -10,
              }}>

                <Image source={require('res/drawable/LogoTitle.png')}
                  style={{width: '60%', height: 40, resizeMode: 'contain',
                }}/>
                <Text style={{textAlign: 'center', fontSize: 22, color: '#e8a32a'}}>
                  Премиум!
                </Text>
              
                <Text style={{marginTop: 10, width: '100%', paddingLeft: 20, paddingRight: 20
                }}>
                  {text}
                </Text>
                <View style={{paddingLeft: 10, paddingRight: 10, paddingBottom: 10,
                  width: '100%'
                }}>
                  <TouchableOpacity
                    style={{width: '100%', backgroundColor: '#b4cc80',
                      borderRadius: 9, alignItems: 'center',
                      paddingLeft: 5, paddingRight: 5,
                      paddingTop: 6, paddingBottom: 8,
                    }}
                    onPress={this.onSubmit}
                  >
                    <Text style={{
                      color: 'white',
                      fontSize: 18,
                    }}>
                      7 дней бесплатно
                    </Text>
                    <Text style={{
                      color: 'white',
                      fontSize: 14,
                    }}>
                      далее {this.state.price} / месяц
                    </Text>
                  </TouchableOpacity>
                </View>

                {/* { Platform.OS === 'ios' &&
                  <View>
                    <Text style={{marginTop: 10, padding: 10, fontSize: 12, textAlign: 'justify'}}>
                      Оплата производится через аккаунт iTunes при подтверждении покупки.
                      Подписки автоматически продлеваются в конце срока действия и с учетной записи iTunes списывается плата.
                      Подписку можно отменить не позднее чем за 24 часа до окончания текущего периода.
                      Можно отключить автоматическое продление и отказаться от подписки в настройках учетной записи iTunes.
                      Плата за неиспользованный срок подписки не возвращается.

                      Продолжив вы соглашаетесь с
                      <Text style={{color: colors.hyperlink}}
                        onPress={() => Linking.openURL(LINKS.RULES)}
                      >
                        {" Правилами пользования "}
                      </Text>
                      и
                      <Text style={{color: colors.hyperlink}}
                        onPress={() => Linking.openURL(LINKS.POLICY)}
                      >
                        {" Политикой конфиденциальности персональных данных "}
                      </Text>
                    </Text>
                  </View>
                } */}

              </View>
            </ScrollView>
            <TouchableOpacity onPress={() => this.onClose()}
              style={{position: 'absolute', right: 14, top: 14,
                  backgroundColor: '#ccc', borderRadius: 12, padding: 2,
              }}
            >
              <IconAnt name='close' size={20} color='#555' />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
}
