
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import FamilyScreen from './FamilyScreen';
import NumberInputScreen from './NumberInput';
import SearchResScreen from './SearchResScreen';
import ChatFullScreen from './ChatFullScreen';

export default createStackNavigator(
  {
    FamilyMain: {
      screen: FamilyScreen,
      navigationOptions: {
        header: null,
      }
    },
    FamilyNumberInput: {
      screen: NumberInputScreen,
      navigationOptions: {
        header: null,
      }
    },
    FamilySearchRes: {
      screen: SearchResScreen,
      navigationOptions: {
        header: null,
      }
    },
    ChatFullScreen: {
      screen: ChatFullScreen,
      navigationOptions: {
        header: null,
      }
    },
  },
  {
    initialRouteName: 'FamilyMain'
  }
)
