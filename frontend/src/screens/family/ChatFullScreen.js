import React from 'react';
import { View, ImageBackground, Image, TouchableOpacity, Platform,
        FlatList, TextInput, Keyboard, KeyboardAvoidingView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Text, colors, LogoTitleBar, backImage, getAvatar, RoundImage } from '../../common';
import { getChat, sendMessage } from 'backend/data';
import { NavigationEvents } from 'react-navigation';
import { setRead } from '../../backend/data';

export default class ChatFullScreen extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      messages: [],
      text: '',
    }

    this.refreshData(true);
  }

  getUserId() {
    return this.props.navigation.getParam('user').user_id
  }

  refreshData(mustScroll){
    let user_id = this.getUserId()

    getChat(user_id, (data) => {
      if (data){
        // console.log('set state')

        let scrollDown = this.state.isDown;

        // console.log('is down', scrollDown, mustScroll);

        if (this.mounted){
          this.setState({messages: data.messages}, () => {
            if (mustScroll){
              console.log('must scroll')
              this.scrollDownFast();
            }
            else if (scrollDown){
              this.scrollDown();
            }
          })
        }
        else {
          console.log('set state directly')
          this.state.messages = data.messages;
        }
      }
    })
  }

  scrollDown() {
    const func = () => {
      if (this.mounted){
        this.refs.msglist.scrollToEnd({animated: true});
      }
    }
    setTimeout(func, 50);
    setTimeout(func, 500);
  }

  scrollDownFast() {
    const func = () => {
      if (this.mounted){
        this.refs.msglist.scrollToEnd({animated: false});
      }
    }
    setTimeout(func, 50);
    setTimeout(func, 500);
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 1000);

    this.refreshData(true);

    this._kbWillShow = Keyboard.addListener('keyboardDidShow', () => {
      this.setState({hideTop: true})
      this.scrollDownFast();
    })

    this._kbWillHide = Keyboard.addListener('keyboardDidHide', () => {
      this.setState({hideTop: false})
      this.scrollDownFast();
    })

    // this.scrollDown();
  }
  
  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this._interval);
  }

  send(){
    console.log('send')
    let user_id = this.getUserId()

    let text = this.state.text;
    text = text.trim();

    if (text.length !== 0){
      this.setState({text: ''})
      sendMessage(user_id, text, (res, error) => {
        if (!error){
          this.refreshData(true);
        }
      })
    }
  }

  onWillFocus() {
    let user = this.props.navigation.getParam('user');
    this._readInterval = setInterval(() => {
      setRead(user.user_id, (res, error) => {

      })
    }, 1000);
  }

  onWillBlur() {
    if (this._readInterval){
      clearInterval(this._readInterval);
    }
  }

  render () {
    let user = this.props.navigation.getParam('user');

    drawItem = ({item}) => {
      let align = item.from_id === user.user_id ? 'flex-start' : 'flex-end'

      return (
        <View style={{
          backgroundColor: colors.message,
          margin: 4,
          padding: 8,
          borderRadius: 15,
          alignSelf: align,
        }}>
          <Text>{item.text}</Text>
        </View>
      )
    }

    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        {/* <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            flex: 1,
            backgroundColor: 'transparent',
          }}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{flex: 1}}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
        > */}
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS === 'ios' ? "height" : ""}
          keyboardVerticalOffset={55}
        >
          <View style={{
            flex: 1,
            borderRadius: 7,
            margin: 8,
            backgroundColor: colors.childCard.background,
            shadowColor: '#777',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            elevation: 5,
          }}>
            <View style={{alignItems: 'center', marginTop: 10}}>
              { !this.state.hideTop &&
                <RoundImage
                  style={{width: 110, height: 110, borderRadius: 55}}
                  source={getAvatar(user.image, user.role, user.gender)}
                />
              }
              <Text style={{textAlign: 'center', marginTop: 6, marginLeft: 10, marginRight: 10}}>
              { user.display_name || user.name}
              </Text>
            </View>

            <View style={{ flex: 1,
              minHeight: 100,
            }}>
              <FlatList style={{flex: 1, marginTop: 10}}
                data={this.state.messages}
                renderItem = {drawItem}
                ref='msglist'
                onEndReached={() => this.setState({isDown: true})}
                onScroll={() => {this.setState({isDown: false})}}
              />
              <View style={{flexDirection: 'row', alignItems: 'center', 
                paddingLeft: 10, paddingRight: 10, paddingBottom: 6, 
                paddingTop: 6,
              }}>
                <TextInput style={{flex: 1, borderRadius: 10,
                  borderWidth: 2,
                  borderColor: colors.textInput.border,
                  borderRadius: 17,
                  fontSize: 16,
                  paddingLeft: 6,
                  paddingRight: 6,
                  paddingTop: 2,
                  paddingBottom: 2,
                  marginRight: 5}}
                  onChangeText={text => this.setState({text})}
                >
                  {this.state.text}
                </TextInput>
                <TouchableOpacity onPress={() => this.send()}>
                  {/* <IconMat name='send' size={30} /> */}
                  <Image source={require('res/icons/Send_invitation.png')}
                    style={{width: 38, height: 38, resizeMode: 'contain'}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
        {/* </KeyboardAwareScrollView> */}
        <NavigationEvents onWillFocus={() => this.onWillFocus()}
          onWillBlur={() => this.onWillBlur()}
        />
      </ImageBackground>
    )
  }
}
