import React, {Component} from 'react';
import { View, FlatList, TouchableHighlight, TouchableOpacity,
        Image } from 'react-native';
import {Text, colors, styles} from 'common'
import { imageUrl, getChatList } from 'backend/data';
import { getAvatar, RoundImage } from '../../common';

export default class ChatListTab extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      chats: [],
    }

    this.refreshData();
  }

  refreshData() {
    getChatList((res, error) => {
      if (!error && res){
        if (this.mounted){
          this.setState({chats: res})
        } else {
          this.state.chats = res
        }
      }
    })
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 1000);
  }
  
  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this._interval);
  }

  onItemPress(item, index) {
    this.props.navigation.navigate('ChatFullScreen', {user: item})
  }
  
  drawItem ({item, index}) {
    let user = item.user;

    return (
      <View style={{
        padding: 10, marginLeft: 10,
        marginRight: 10,
      }}>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center'}}
          onPress={() => this.onItemPress(user, index)}
        >
          <RoundImage
            style={{width: 70, height: 70,
              borderRadius: 35,
              marginRight: 20,
            }}
            source={getAvatar(user.image, user.role, user.gender)}
          />
          <Text style={{flex: 1}}>
            {user.display_name || user.name}
          </Text>
          <Text style={{marginLeft: 'auto'}}>
            {item.unread !== 0 ? item.unread : ''}
          </Text>
          <Image
            style={{width: 45, height: 45,
              marginRight: 15,
              marginLeft: 10,
            }}
            source={require('res/icons/Message.png')}
          />
          
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    
    return (
      <View style={{ flex: 1}}>
        <FlatList style={{marginTop: 10}}
          data={this.state.chats}
          renderItem={(x) => this.drawItem(x)}
        />
      </View>
    );
  }
}
