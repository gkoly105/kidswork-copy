import React, {Component} from 'react';
import { View, FlatList, TouchableHighlight, TouchableOpacity,
        Image } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Collapsible from 'react-native-collapsible';
import {Text, colors, GemIcon} from 'common'
import { imageUrl } from 'backend/data';
import { DisplayPrefsDialog } from 'common/dialogs';
import { getAvatar, RoundImage } from '../../common';

class ChildListElem extends Component {
  constructor (props) {
    super(props)

    this.state = {
      prefsVisible: false,
    }
  }

  onItemPress(item, index) {
    // console.log('go to child')
    if (this.props.screenProps.role == 'parent') {
      this.props.navigation.navigate('ChildProfile', {child_i: index});
    }
  }

  onChatPress(item, index) {
    if (this.props.screenProps.role == 'parent') {
      this.props.navigation.navigate('ChildProfile', {child_i: index});
      this.props.navigation.navigate('ChatScreen');
    } else if (this.props.screenProps.role == 'child') {
      this.props.navigation.navigate('ChatFullScreen', {user: item});
    }
  }

  render() {
    let {item, index} = this.props;

    return (
      <View style={{
        padding: 10, marginLeft: 10,
        marginRight: 10,
      }}>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center'}}
          onPress={() => this.onItemPress(item, index)}
        >
          <RoundImage
            style={{width: 70, height: 70,
              borderRadius: 35,
              marginRight: 20,
            }}
            source={getAvatar(item.image, 'child', item.gender)}
          />
          <Text style={{flex: 1}}>
            {item.display_name || item.name}
          </Text>
          <TouchableOpacity onPress={() => this.setState({prefsVisible: true})}
            style={{marginRight: 15,
                marginLeft: 'auto',
            }}
          >
            <Image
              style={{width: 45, height: 45,}}
              source={require('res/icons/Settings.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.onChatPress(item, index)}
            style={{
            }}
          >
            <Image
              style={{width: 45, height: 45,
              }}
              source={require('res/icons/Message.png')}
            />
          </TouchableOpacity>
        </TouchableOpacity>
        <DisplayPrefsDialog visible={this.state.prefsVisible}
          setVisible={(v) => this.setState({prefsVisible: v})}
          user={item}
        />
      </View>
    )
  }
}

export default class ChildListTab extends React.Component {
  constructor(props){
    super(props)
  }
  
  drawItem = ({item, index}) => (
    <ChildListElem item={item} index={index} {...this.props} />
  )

  render() {
    let children = this.props.screenProps.children
    
    return (
      <View style={{ flex: 1}}>
        { children && children.length ? (
          <FlatList style={{marginTop: 10}}
            data={children}
            renderItem = {this.drawItem}
          />
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={{textAlign: 'center'}}>
              Нет добавленных детей
            </Text>
          </View>
        )}
      </View>
    );
  }
}
