import React from 'react';
import { View, ImageBackground, Image, TouchableOpacity,
        Share } from 'react-native';
import { Text, colors, LogoTitleBar, backImage } from '../../common';
import { getFamily } from 'backend/data';
import FamilyTabsNavigator from './FamilyTabsNavigator';
import { SHARE_TEXT } from '../../backend/data';

export default class FamilyScreen extends React.Component {
  static router = FamilyTabsNavigator.router;

  constructor(props){
    super(props);

    this.state = {
      children: [],
      parents: [],
      self: {},
      role: '',
    }
    
    this.refreshData();
  }

  refreshData() {
    getFamily((res, error) => {
      if (!error && res){
        if (this.mounted){
          this.setState(res)
        } else {
          this.state = res
        }
      }
    })
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 1000);
  }
  
  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this._interval);
  }

  onShare() {
    Share.share({
      message: SHARE_TEXT,
    }).then(res => {

    })
  }

  actionPanel() {
    return (
      <View style={{
        padding: 14,
        flexDirection: 'row',
      }}>
        <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
          onPress={() => this.props.navigation.navigate('FamilyNumberInput')}
        >
          <Image source={require('res/icons/Add_user.png')}
            style={{width: 80, height: 90, resizeMode: 'contain'}}
          />
          <Text style={{fontSize: 14, textAlign: 'center'}}>
            Добавить{'\n'}пользователя
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
          onPress={() => this.onShare()}
        >
          <Image source={require('res/icons/Send_invitation.png')}
            style={{width: 80, height: 90, resizeMode: 'contain'}}
          />
          <Text style={{fontSize: 14, textAlign: 'center'}}>
            Отправить{'\n'}приглашение
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

  emptyFamilyPanel() {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{
          padding: 16,
          borderRadius: 3,
          backgroundColor: colors.background,
          elevation: 8,
          margin: 20,
          borderWidth: 2,
          borderColor: colors.dialog.border,
        }}>
          {this.actionPanel()}
        </View>
      </View>
    )
  }

  filledFamilyPanel() {
    return (
      <View style={{
        flex: 1,
        borderRadius: 7,
        margin: 8,
        backgroundColor: colors.childCard.background,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
      }}>
        {this.actionPanel()}
        <FamilyTabsNavigator
          navigation={this.props.navigation}
          screenProps={{
            parents: this.state.parents,
            children: this.state.children,
            role: this.state.role,
          }}
        />
      </View>
    )
  }

  isEmpty() {
    return this.state.children.length === 0 &&
            this.state.parents.length === 0
  }

  render () {
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        {this.isEmpty() ? this.emptyFamilyPanel() : this.filledFamilyPanel()}
      </ImageBackground>
    )
  }
}
