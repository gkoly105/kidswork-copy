
import React, {Component} from 'react';
import {View, Button, TextInput, KeyboardAvoidingView,
  TouchableHighlight, TouchableOpacity, ImageBackground} from 'react-native';
import {login} from 'backend/auth';
import { Text, LogoTitleBar, backImage, colors, styles } from 'common';
import { findUser, ERRORS } from '../../backend/data';

export default class NumberInputScreen extends Component {

  static navigationOptions = () => ({
    header: null,
  });

  constructor(props){
    super(props);
    this.state = {
      phone: '',
      error: undefined,
    }
  }

  onSubmit(){
    findUser(this.state.phone, (res, error) => {
      if (!error && res && res.data) {
        this.setState({error: undefined})
        this.props.navigation.navigate('FamilySearchRes',
                                      {search_user: res.data,
                                      search_role: res.role})
      }
      else if (error == ERRORS.NOTFOUND) {
        this.setState({error: 'Пользователь не найден'})
      }
      else if (error == ERRORS.FORMAT) {
        this.setState({error: 'Неверный формат'})
      }
    })
  }
  
  render(){
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} menu={false}/>
        <KeyboardAvoidingView enabled
          style={{flex: 1}}
          behavior="height"
          keyboardVerticalOffset={55}
        >
          <View style={{flex: 1, justifyContent: 'center'}}>
            <View style={{
              padding: 16,
              borderRadius: 3,
              backgroundColor: colors.background,
              elevation: 8,
              margin: 20,
              borderWidth: 2,
              borderColor: colors.dialog.border,
            }}>
              <Text style={{textAlign: 'center', color: 'black',
                marginBottom: 10,
              }}>
                Введите номер телефона, указанный пользователем при регистрации
              </Text>
              <TextInput
                style={[styles.textInput, {textAlign:'center'}]}
                textContentType='telephoneNumber'
                onChangeText={phone => this.setState({phone})}
              >
                {this.state.phone}
              </TextInput>
              {this.state.error &&
                <Text style={{textAlign: 'center', color: colors.errorText,
                  marginBottom: 10,
                }}>
                  {this.state.error}
                </Text>
              }
              <TouchableOpacity
                style={{
                  width: 100,
                  height: 50,
                  borderWidth: 3,
                  borderColor: colors.menuButton,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: 20,
                }}
                onPress={() => this.onSubmit()}
              >
                <Text style={{ 
                  padding: 3,
                  color: 'black',
                }}>Найти</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    )
  }
}