
import React, {Component} from 'react';
import {View, Button, TextInput, Image,
  TouchableHighlight, TouchableOpacity, ImageBackground} from 'react-native';
import {login} from 'backend/auth';
import { Text, LogoTitleBar, backImage, colors, styles } from 'common';
import { imageUrl, linkUser, offerLinkUser } from '../../backend/data';
import { RoundImage } from '../../common';

export default class SearchResScreen extends Component {

  static navigationOptions = () => ({
    header: null,
  });

  constructor(props){
    super(props);
    this.state = {
      
    }
  }

  onSubmit(){
    let role = this.props.navigation.getParam('search_role');
    let user = this.props.navigation.getParam('search_user');

    offerLinkUser(role, user.id, (res, error) => {
      if (!error){
        this.props.navigation.navigate('FamilyMain');
      }
    })
  }
  
  render(){
    let item = this.props.navigation.getParam('search_user');

    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} menu={false}/>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            padding: 16,
            borderRadius: 3,
            backgroundColor: colors.background,
            elevation: 8,
            margin: 20,
            borderWidth: 2,
            borderColor: colors.dialog.border,
            alignItems: 'center',
          }}>
            <RoundImage
              style={{width: 110, height: 110,
                borderRadius: 55,
              }}
              source={{ uri: imageUrl(item.image)}}
            />
            <Text style={{color: 'black', fontSize: 16, marginTop: 10}}>
              {item.name}
            </Text>
            <TouchableOpacity
              style={{
                width: 130,
                height: 40,
                borderWidth: 3,
                borderColor: colors.menuButton,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                marginTop: 20,
              }}
              onPress={() => this.onSubmit()}
            >
              <Text style={{ 
                padding: 3,
                color: 'black',
              }}>Добавить</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }
}