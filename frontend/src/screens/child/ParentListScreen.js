import React, {Component} from 'react';
import {Platform, StyleSheet, View,
  FlatList, Button, Image, TouchableOpacity, ImageBackground} from 'react-native';
import { Text, colors, TaskProgress, LogoTitleBar, backImage } from 'common';
import { getAvatar, RoundImage, tasksText } from '../../common';

export default class ParentListScreen extends Component {
  static navigationOptions = {
    header: null
  }

  drawItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          padding: 14,
          paddingLeft: 30, 
          paddingRight: 30,
          borderRadius: 7,
          margin: 4,
          marginBottom: 15,
          backgroundColor: colors.childCard.background,
          shadowColor: '#777',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.5,
          shadowRadius: 5,
          elevation: 5,
        }}
        onPress={() =>
          this.props.navigation.navigate('ParentProfile', {
            parent_i: index,
          })
        }
      >
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
          <View style={{ flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('res/icons/Task.png')}
              style={{width: 21, height: 21, marginRight: 7, resizeMode: 'contain'}}
            />
            <Text >{item.tasksDone} / {item.tasks}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('res/icons/Like.png')}
              style={{width: 26, height: 26, marginRight: 7, resizeMode: 'contain'}}
            />
            <Text style={{textAlign: 'center'}}>{item.points}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('res/icons/Gift.png')}
              style={{width: 24, height: 24, marginRight: 7, resizeMode: 'contain'}}
            />
            <Text style={{textAlign: 'right'}}>{item.gifts}</Text>
          </View>
        </View>
        <View style={{
          backgroundColor: colors.separator,
          width: '100%',
          height: 2,
          alignSelf: 'center',
          marginTop: 10,
          marginBottom: 10,
        }}></View>
        <View style={{flexDirection: 'row',
          alignItems: 'center',
          alignSelf: 'center',
          width: '100%',
          marginTop: 10,
        }}>
          <View style={{alignItems: 'center', flex: 1}}>
            <Text style={{width: 100, textAlign: 'center', fontSize: 30, color: 'red'}}>
              {item.tasks - item.tasksDone}
            </Text>
            <Text style={{textAlign: 'center', marginTop: 0, fontSize: 16}}>
              {tasksText(item.tasks - item.tasksDone)}
            </Text>
          </View>
          <View style={{alignItems: 'center', flex: 1}}>
            <RoundImage
              style={{width: 110, height: 110, borderRadius: 55}}
              source={getAvatar(item.image, 'parent', item.gender)}
            />
            <Text style={{width: 100, textAlign: 'center', marginTop: 6}}>
              { item.display_name || item.name}
            </Text>
          </View>
        </View>
        

        {/* 
        <Image
          style={{width: 110, height: 110, borderRadius: 55}}
          source={{ uri: imageUrl(item.image)}}
        />
        <View style={{marginLeft: 20, flex: 1}}>
          <View style={{marginBottom: 10, flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: colors.childCard.font1}}>
              {item.name}
            </Text>
          </View>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <IconMatCom name='format-list-checkbox' size={25} 
              style={{marginRight: 10, color: colors.childCard.font2}}
            />
            <Text style={{color: colors.childCard.font2}}>{item.tasksDone} / {item.tasks}</Text>
            <TaskProgress style={{marginLeft: 16}}/>
          </View>

          <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <IconAnt name='gift' size={24}
              style={{marginRight: 11, color: colors.childCard.font2}}
            />
            <Text style={{color: colors.childCard.font2}}>{item.gifts}</Text>
          </View>
        </View>  */}
      </TouchableOpacity>
    )
  }

  render(){
    let parentList = this.props.screenProps.parentList
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <View style={{flex: 1}}>
          <LogoTitleBar navigation={this.props.navigation} />
          <FlatList
            data={parentList}
            renderItem = {this.drawItem}
            keyboardShouldPersistTaps="handled"
          />
        </View>
      </ImageBackground>
    )
  }
}