import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import {ScrollView, View, Image, AsyncStorage} from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/FontAwesome';
import {logout} from 'backend/auth';
import {getProfile, imageUrl} from 'backend/data';
import { Text, colors, getAvatar, RoundImage } from '../../common';
import { removeFcmDevice } from '../../backend/data';

export default class DrawerPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      role: '',
      user: {},
    }

    this.refreshData();
  }

  refreshData() {
    getProfile((res, error) => {
      if (!error && res){
        if (this.mount) {
          this.setState(res)
        } else {
          _.merge(this.state, res)
        }
      }
    })
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.navigation.state.isDrawerOpen &&
        !this.props.navigation.state.isDrawerOpen){
      this.refreshData();
    }
  }

  componentDidMount(){
    this.mount = true;
  }

  componentWillUnmount() {
    this.mount = false;
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  onLogout() {
    console.log('logout');
    removeFcmDevice(() => {
      logout(res => {
        this.props.navigation.navigate('Auth')
        AsyncStorage.removeItem('learned');
      })
    })
  }

  // showHistory() {
  //   if (this.state.role === 'parent'){
  //     return this.state.user.perms.history
  //   } else if (this.state.role === 'child'){
  //     for (let par of _.values(this.state.user.perms)){
  //       if (par.history){
  //         return true;
  //       }
  //     }
  //   }

  //   return false;
  // }

  render () {
    let user = this.state.user

    return (
      <View style={{flex: 1}}>
        <View style={{
          paddingTop: 35, paddingBottom: 15,
          alignItems: "center",
          backgroundColor: colors.menuButton
        }}>
          <RoundImage
            style={{width: 110, height: 110,
              borderRadius: 55,
            }}
            source={getAvatar(user.image, 'child', user.gender, true)}
          />
          <Text style={{marginTop: 12}}>
            {user.name}
          </Text>
        </View>
        <ScrollView style={{padding: 20, flex: 1}}>
          <View style={styles.menuItem}>
            <Image source={require('res/icons/Menu/Family.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText} onPress={this.navigateToScreen('FamilyMain')}>
              Семья
            </Text>
          </View>
          {/* { this.showHistory() && */}
          <View style={styles.menuItem}>
            <Image source={require('res/icons/History.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText} onPress={this.navigateToScreen('TaskHistory')}>
              История заданий
            </Text>
          </View>
          {/* } */}
          <View style={styles.menuItem}>
            <Image source={require('res/icons/Menu/Message_menu.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText} onPress={this.navigateToScreen('FamilyChatList')}>
              Сообщения
            </Text>
          </View>
          <View style={styles.menuItem}>
            <Image source={require('res/icons/Gift_choice.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText} onPress={this.navigateToScreen('Catalog')}>
              Выбор подарка
            </Text>
          </View>
          <View style={styles.menuItem}>
            <Image source={require('res/icons/Menu/Settings_menu.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText}  onPress={this.navigateToScreen('Settings')}>
              Настройки
            </Text>
          </View>
          <View style={styles.menuItem}>
            <Image source={require('res/icons/Menu/Information.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText} onPress={this.navigateToScreen('Learn', {role: 'child'})}>
              Справка
            </Text>
          </View>
          <View style={styles.menuItem}>
            <Image source={require('res/icons/Menu/Tech_support.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText} onPress={this.navigateToScreen('Support')}>
              Обратная связь
            </Text>
          </View>
          <View style={styles.menuItem}>
            <Image source={require('res/icons/Menu/Logout.png')}
              style={styles.menuItemIcon}
            />
            <Text style={styles.menuItemText} onPress={() => this.onLogout()}>
              Выход
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  menuItem: {
    marginBottom: 7,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuItemText: {
    fontSize: 14,
    marginLeft: 10,
  },
  menuItemIcon: {
    width: 28,
    height: 28,
    resizeMode: 'contain',
  },
}
