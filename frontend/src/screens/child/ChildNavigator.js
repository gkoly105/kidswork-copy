
import React, {Component} from 'react';
import {createAppContainer, NavigationEvents} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {AsyncStorage, View} from 'react-native';
import ParentListScreen from './ParentListScreen';
import ParentProfileScreen from './ParentProfileScreen';
import {getParentList} from 'backend/data';
import AddTaskScreen from '../main/AddTaskScreen';
import AddGiftScreen from '../main/AddGiftScreen';

const ChildNavStack = createStackNavigator(
  {
    Home: {
      screen: ParentListScreen,
    },
    ParentProfile: {
      screen: ParentProfileScreen,
    },
    AddTask: {
      screen: AddTaskScreen,
    },
    AddGift: {
      screen: AddGiftScreen,
    },
  },
  {
    initialRouteName: 'Home'
  }
)

export default class ChildNavigator extends Component {
  static router = ChildNavStack.router;

  constructor(props){
    super(props)

    this.state = {
      parentList: [],
      empty: false,
    }

    this.refreshData()
    
    AsyncStorage.getItem('learned').then(v => {
      if (!v) {
        this.props.navigation.navigate('Learn', {role: 'child'});
        AsyncStorage.setItem('learned', '1');
      }
    })
  }

  refreshData(){
    getParentList((list) => {
      if (list){
        let empty = list.length === 0;
        if (empty && this.props.navigation.isFocused()){
          this.props.navigation.navigate('FamilyMain')
          return
        }
        
        if (this.mounted){
          // console.log('set state')
          this.setState({parentList: list, empty})
        }
        else {
          // console.log('set state directly')
          this.state.parentList = list;
          this.state.empty = empty;
        }
      }
    })
  }

  componentDidMount(){
    this.mounted = true;
  
    this._interval = setInterval(() => {
      this.refreshData()
    }, 3000);
  }
  
  componentWillUnmount() {
    clearInterval(this._interval);
  }

  onDidFocus() {
    if (this.state.empty){
      this.props.navigation.navigate('FamilyMain')
    }
  }

  render(){
    return (
      <View style={{flex: 1}}>
        <ChildNavStack 
          navigation={this.props.navigation}
          screenProps={{
            parentList: this.state.parentList,
            refreshData: () => this.refreshData(),
          }}
        />
        <NavigationEvents onDidFocus={this.onDidFocus.bind(this)} />
      </View>
    )
  }
}
