import React, {Component} from 'react';
import { View, ImageBackground, Image, TouchableOpacity, KeyboardAvoidingView,
        Keyboard, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import _ from 'lodash';
import {getChildDetails, imageUrl} from 'backend/data';
import IconIon from 'react-native-vector-icons/Ionicons';
import ParentProfileNavigator from './ParentProfileNavigator';
import { Text, colors, TaskProgress, GemIcon, LogoTitleBar, backImage } from 'common';
import { getAvatar } from '../../common';
import { EarnDialog, TextAlertDialog } from '../../common/dialogs';
import { requestTask } from '../../backend/data';
import { ScrollView } from 'react-native-gesture-handler';

// var HeaderBar = ({ navigation, screenProps }) => {
//   let child_i = navigation.state.params.child_i;
//   let item = screenProps.childList[child_i];

//   return (
//     <View style={{flexDirection: 'row', alignItems: 'center'}}>
//       <Text style={{flex: 1, fontSize: 20, fontWeight: 'bold'}}>{item.name}</Text>
//       <View style={{flexDirection: 'row', alignItems: 'center', marginRight: 20}}>
//         <Text style={{marginRight: 7, fontSize: 18, fontWeight: 'bold'}}>{item.points}</Text>
//         <GemIcon style={{width: 28, height: 28}}/>
//       </View>
//     </View>
//   )
// }

export default class ParentProfileScreen extends React.Component {
  static router = ParentProfileNavigator.router;

  static navigationOptions = ({ navigation, screenProps }) => ({
    // title: `${screenProps.childList[navigation.state.params.child_i].name}`,
    // headerTitleStyle : {textAlign: 'center',alignSelf:'center'},
    //   headerStyle:{
    //       backgroundColor:'white',
    //   },
    // headerTitle: HeaderBar({navigation, screenProps}),
    header: null,
  });

  constructor(props){
    super(props)

    this.state = {
      earnDlgVisible: false,
      activeTab: 0,

      alertVisible: false,
      onAlertDone: undefined,
      alertText: '',
      alertSingleButton: false,
    }
  }

  onTabChange(index) {
    console.log('tab changed to', index);
    this.setState({activeTab: index});
  }

  getParentI() {
    let parent_i = this.props.navigation.getParam('parent_i');
    if (parent_i !== undefined){
      return parent_i
    }
    else {
      let parent_id = this.props.navigation.getParam('parent_id');
      return _.findIndex(this.props.screenProps.parentList,
                        (el) => el.id === parent_id)
    }
  }

  onRequest() {
    let parent_i = this.getParentI();
    let parent = this.props.screenProps.parentList[parent_i];
    requestTask(parent.id, (res, error) => {
      
    })
  }

  onOffer() {
    let parent_i = this.getParentI();
    let parent = this.props.screenProps.parentList[parent_i];
    this.props.navigation.navigate('AddTask', {parent_id: parent.id})
  }

  onAdd() {
    let parent_i = this.getParentI();
    let parent = this.props.screenProps.parentList[parent_i];

    if (this.state.activeTab === 0){
      if (parent.taskLimit == undefined || parent.taskLimit > 0){
        this.setState({earnDlgVisible: true})
      } else {
        this.setState({
          alertVisible: true,
          alertText: 'Достигнут лимит на количество заданий. Текущий лимит 4 задания / 7 дней.',
          onAlertDone: undefined,
          alertSingleButton: true,
        })
      }
    } else if (this.state.activeTab === 1) {
      this.props.navigation.navigate('AddGift', {parent_id: parent.id})
    }
  }

  render() {
    let parent_i = this.getParentI();
    let item = this.props.screenProps.parentList[parent_i];

    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation}/>
        {/* <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            flex: 1,
            backgroundColor: 'transparent',
          }}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{flex: 1}}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
          enableOnAndroid={true}
          // extraHeight={0}
          // extraScrollHeight={0}
        > */}
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS === 'ios' ? "height" : ""}
          keyboardVerticalOffset={55}
        >
        {/* <ScrollView style={{flex: 1}}
          scrollEnabled={this.state.keyboard}
          contentContainerStyle={{height: (this.state.keyboard ? 500 : '100%')}}
          // contentContainerStyle={{height: '100%'}}
          ref={el => this.scrollView = el}
        > */}
          <View style={{
            flex: 1,
            borderRadius: 7,
            margin: 8,
            backgroundColor: colors.childCard.background,
            shadowColor: '#777',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            elevation: 5,
          }}>
            { !this.state.hideTop &&
              <View
                style={{
                  padding: 14,
                  paddingLeft: 30, 
                  paddingRight: 30,
                }}
              >
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                  <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center'}}
                    onPress={() => this.props.navigation.navigate('TaskList')}
                  >
                    <Image source={require('res/icons/Task.png')}
                      style={{width: 21, height: 21, marginRight: 7, resizeMode: 'contain'}}
                    />
                    <Text >{item.tasksDone} / {item.tasks}</Text>
                  </TouchableOpacity>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('res/icons/Like.png')}
                      style={{width: 26, height: 26, marginRight: 7, resizeMode: 'contain'}}
                    />
                    <Text style={{textAlign: 'center'}}>{item.points}</Text>
                  </View>
                  <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}}
                    onPress={() => this.props.navigation.navigate('GiftList')}
                  >
                    <Image source={require('res/icons/Gift.png')}
                      style={{width: 24, height: 24, marginRight: 7, resizeMode: 'contain'}}
                    />
                    <Text style={{textAlign: 'right'}}>{item.gifts}</Text>
                  </TouchableOpacity>
                </View>
                <View style={{
                  backgroundColor: colors.separator,
                  width: '100%',
                  height: 2,
                  alignSelf: 'center',
                  marginTop: 10,
                  marginBottom: 10,
                }}></View>
                <View style={{flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'center',
                  width: '100%',
                  marginTop: 10,
                }}>
                  { [0, 1].includes(this.state.activeTab) &&
                    <View style={{alignItems: 'center', marginTop: 10, flex: 1}}>
                      <TouchableOpacity
                        onPress={() => {
                          console.log('press')
                          this.onAdd();
                        }}
                      >
                        {/* <IconIon style={{}} name='ios-add-circle' size={80} color='#A4D73D' /> */}
                        <Image source={require('res/icons/Add.png')}
                          style={{width: 70, height: 70}}
                        />
                      </TouchableOpacity>
                      <Text style={{textAlign: 'center', marginTop: 8}}>
                        { this.state.activeTab === 0 && 
                          'Заработать'
                        }
                        { this.state.activeTab === 1 &&
                          'Выбрать подарок'
                        }
                      </Text>
                    </View>
                  }
                  <View style={{alignItems: 'center', flex: 1}}>
                    <Image
                      style={{width: 100, height: 100, borderRadius: 50}}
                      source={getAvatar(item.image, 'parent', item.gender)}
                    />
                    <Text style={{width: 100, textAlign: 'center', marginTop: 6}}>
                      { item.display_name || item.name}
                    </Text>
                  </View>
                </View>
              </View>
            }
            <ParentProfileNavigator
              navigation={this.props.navigation}
              screenProps={{
                onInputFocus: (v) => this.setState({hideTop: v}),
                parent: item,
                parent_i: parent_i,
                onTabChange: (v) => this.onTabChange(v),
              }}
            />
          </View>
          <EarnDialog visible={this.state.earnDlgVisible}
            setVisible={(earnDlgVisible) => this.setState({earnDlgVisible})}
            onRequest={this.onRequest.bind(this)}
            onOffer={this.onOffer.bind(this)}
          />
          <TextAlertDialog visible={this.state.alertVisible}
            setVisible={(v) => this.setState({alertVisible: v})}
            onConfirm={this.state.onAlertDone}
            text={this.state.alertText}
            singleButton={this.state.alertSingleButton}
          />
        {/* </ScrollView> */}
        </KeyboardAvoidingView>
        {/* </KeyboardAwareScrollView> */}
      </ImageBackground>
    );
  }
}