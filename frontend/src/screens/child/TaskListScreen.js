import React, {Component} from 'react';
import { View, FlatList, TouchableOpacity, Image } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Collapsible from 'react-native-collapsible';
import {GemIcon} from 'common'
import { Text, colors } from 'common';
import { doTask, doTaskImage } from '../../backend/data';
import { PhotoDialog, TextAlertDialog, ImageShowDialog, ProlongDialog, RequestRemoveDialog } from '../../common/dialogs';
import { NavigationEvents } from 'react-navigation';

import moment from 'moment';
import momentRu from 'moment/locale/ru';
moment.locale('ru');

class TaskListElem extends Component {
  constructor(props){
    super(props);

    this.state = {
      photoDlgVisible: false,
      doDlgVisible: false,
      doDlgText: '',
      attachDlgVisible: false,
      prolongDlgVisible: false,
      removeDlgVisible: false,

      alertVisible: false,
      onAlertDone: undefined,
      alertText: '',
      alertSingleButton: false,
    }
  }

  onDoTask(){
    let text = 'Отметить задание как выполненное?'
    if (this.props.item.require_image) {
      text += ' Требуется фото-подтверждение';
    }
    this.setState({doDlgVisible: true, doDlgText: text});
  }

  onDoConfirm() {
    if (this.props.item.require_image) {
      this.setState({photoDlgVisible: true});
    }
    else {
      this.onDoSend()
    }
  }

  onDoSend(photo) {
    if (photo) {
      doTaskImage(this.props.item.id, photo.uri, (res, error) => {

      })
    }
    doTask(this.props.item.id, (res, error) => {

    })
  }

  showAttachImage() {
    if (this.props.item.finish_image){
      this.setState({attachDlgVisible: true});
    }
  }

  onProlongTask() {
    let item = this.props.item;
    if (item.time_limit){
      this.setState({prolongDlgVisible: true})
    }
    else {
      this.setState({
        alertVisible: true,
        alertText: 'Для этого задания лимит времени не установлен',
        onAlertDone: undefined,
        alertSingleButton: true,
      })
    }
  }

  onDeleteTask() {
    this.setState({removeDlgVisible: true})
  }

  render() {
    let item = this.props.item;

    return (
      <View style={{
          padding: 10, marginLeft: 10,
          marginRight: 10,
        }}
      >
        <TouchableOpacity
          onPress={() => this.props.onItemPress(item)}
        >
          <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{flex: 1, fontSize: 14, color: 'black'}}>
                {item.title}
              </Text>
              <View style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                { item.require_image && 
                <TouchableOpacity onPress={this.showAttachImage.bind(this)}>
                  { item.finish_image && 
                    <Image source={require('res/icons/Attach_recieved.png')}
                      style={{width: 26, height: 26, resizeMode: 'contain', marginRight: 12}}
                    />
                  }
                  { !item.finish_image && 
                    <Image source={require('res/icons/Attach_not_send.png')}
                      style={{width: 26, height: 26, resizeMode: 'contain', marginRight: 12}}
                    />
                  }
                </TouchableOpacity>
                }
                {/* <IconAnt name='checksquareo' size={20} color={colors.checkbox.active} /> */}
                <Text style={{fontSize: 14, marginRight: 8}}>
                  {item.reward}
                </Text>
                <Image source={require('res/icons/Like.png')}
                  style={{
                    width: 23, height: 23,
                    marginRight: 10,
                  }}
                />
                { item.done ? 
                  <Image source={require('res/icons/CheckBoxConfirm.png')}
                    style={{width: 22, height: 22, resizeMode: 'contain'}}
                  />
                  : 
                  <Image source={require('res/icons/CheckBox.png')}
                    style={{width: 22, height: 22, resizeMode: 'contain'}}
                  />
                }
              </View>
            </View>

            { !!item.time_limit &&
              <View style={{flexDirection: 'row', marginTop: 5, alignItems: 'center'}}>
              
                <Text style={{marginLeft: 'auto'}}>
                  {moment.utc(item.time_limit).fromNow(true)}
                </Text>
                <Image source={require('res/icons/time.png')}
                  style={{width: 22, height: 22, resizeMode: 'contain', marginLeft: 10}}
                />
              
              </View>
            }
          </View>
        </TouchableOpacity>
        
        <Collapsible collapsed={this.props.openedId != item.id}>
          <View style={{paddingTop: 10}}>
            {!!item.desc &&
              <View style={{marginLeft: 10, marginRight: 10}}>
                <Text>{item.desc}</Text>
              </View>
            }
            <View style={{
              marginTop: 10,
              width: '100%',
              flexDirection: 'row',
            }}>
              <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                onPress={() => this.onDoTask()}
              >
                {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                <Image source={require('res/icons/Confirm_grey.png')}
                  style={{width: 60, height: 50, resizeMode: 'contain'}}
                />
                <Text style={{fontSize: 9, marginTop: 5}}>Выполнено</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                onPress={() => this.onProlongTask()}
              >
                {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                <Image source={require('res/icons/time.png')}
                  style={{width: 60, height: 50, resizeMode: 'contain'}}
                />
                <Text style={{fontSize: 9, marginTop: 5}}>Продлить</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                onPress={() => this.onDeleteTask()}
              >
                {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                <Image source={require('res/icons/delete.png')}
                  style={{width: 60, height: 50, resizeMode: 'contain'}}
                />
                <Text style={{fontSize: 9, marginTop: 5}}>Отменить</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Collapsible>
        <View style={{width: '100%', height: 2,
          backgroundColor: colors.separator,
          marginTop: 17,
        }}></View>
        <TextAlertDialog visible={this.state.doDlgVisible}
          setVisible={(v) => this.setState({doDlgVisible: v})}
          onConfirm={this.onDoConfirm.bind(this)}
          text={this.state.doDlgText}
        />
        <PhotoDialog visible={this.state.photoDlgVisible}
          setVisible={(v) => this.setState({photoDlgVisible: v})}
          setPhoto={this.onDoSend.bind(this)}
        />
        <ImageShowDialog visible={this.state.attachDlgVisible}
          setVisible={(attachDlgVisible) => this.setState({attachDlgVisible})}
          image={item.finish_image}
        />
        <ProlongDialog visible={this.state.prolongDlgVisible}
          setVisible={(v) => this.setState({prolongDlgVisible: v})}
          task={item}
        />
        <TextAlertDialog visible={this.state.alertVisible}
          setVisible={(v) => this.setState({alertVisible: v})}
          onConfirm={this.state.onAlertDone}
          text={this.state.alertText}
          singleButton={this.state.alertSingleButton}
        />
        <RequestRemoveDialog visible={this.state.removeDlgVisible}
          setVisible={(v) => this.setState({removeDlgVisible: v})}
          task={item}
        />
      </View>
    )
  }
}

export default class TaskListScreen extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      openedId: undefined,
    }
  }
  onItemPress = item => {
    this.setState({openedId: this.state.openedId !== item.id ? 
      item.id : undefined})
  }

  drawItem({item}){
    return <TaskListElem item={item}
        openedId={this.state.openedId}
        onItemPress={this.onItemPress.bind(this)}
      />
  }

  getOpenParam(){
    let task_id = this.props.navigation.getParam('task_id')
    this.setState({
      openedId: task_id
    })
  }

  render() {
    let taskList = this.props.screenProps.parent.taskList;
    let parent_i = this.props.screenProps.parent_i
    
    return (
      <View style={{ flex: 1}}>
        <FlatList style={{marginTop: 10}}
          data={taskList}
          renderItem = {this.drawItem.bind(this)}
          keyboardShouldPersistTaps="handled"
        />
        <NavigationEvents onWillFocus={() => this.props.screenProps.onTabChange(0)}
          onDidFocus={this.getOpenParam.bind(this)}
        />
      </View>
    );
  }
}
