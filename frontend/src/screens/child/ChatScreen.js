import React, {Component} from 'react';
import { View, FlatList, TouchableHighlight, TouchableOpacity,
        KeyboardAvoidingView, Image, Keyboard } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import IconMat from 'react-native-vector-icons/MaterialIcons';
import Collapsible from 'react-native-collapsible';
import {Text, colors, GemIcon} from 'common'
import { getChat, sendMessage } from 'backend/data';
import { TextInput } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';
import { setRead } from '../../backend/data';

export default class ChatScreen extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      messages: [],
      text: '',
    }

    this.refreshData(true);
  }

  getUserId() {
    return this.props.screenProps.parent.user_id
  }

  refreshData(mustScroll){
    let user_id = this.getUserId()

    getChat(user_id, (data) => {
      if (data){
        if (this.mounted){
          // console.log('set state')

          let scrollDown = this.state.isDown;

          // console.log('is down', scrollDown, mustScroll);

          this.setState({messages: data.messages}, () => {
            if (mustScroll){
              // console.log('must scroll')
              this.scrollDownFast();
            }
            else if (scrollDown){
              this.scrollDown();
            }
          })
        }
        else {
          // console.log('set state directly')
          this.state.messages = data.messages;
        }
      }
    })
  }

  scrollDown() {
    const func = () => {
      if (this.mounted){
        this.refs.msglist.scrollToEnd({animated: true});
      }
    }
    setTimeout(func, 50);
    setTimeout(func, 500);
  }

  scrollDownFast() {
    const func = () => {
      if (this.mounted){
        this.refs.msglist.scrollToEnd({animated: false});
      }
    }
    setTimeout(func, 50);
    setTimeout(func, 500);
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 1000);

    this.refreshData(true);

    this._kbWillShow = Keyboard.addListener('keyboardDidShow', () => {
      this.props.screenProps.onInputFocus && this.props.screenProps.onInputFocus(true);
      this.scrollDownFast();
    })

    this._kbWillHide = Keyboard.addListener('keyboardDidHide', () => {
      this.props.screenProps.onInputFocus && this.props.screenProps.onInputFocus(false);
      this.scrollDownFast();
    })

    // this.scrollDown();
  }
  
  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this._interval);

    this._kbWillShow.remove()
    this._kbWillHide.remove()
  }

  send(){
    console.log('send')
    let user_id = this.getUserId()

    let text = this.state.text;
    text = text.trim();
    
    if (text.length !== 0){
      this.setState({text: ''})
      sendMessage(user_id, text, (res, error) => {
        if (!error && this.mounted){
          this.refreshData(true);
        }
      })
    }
  }

  onWillFocus() {
    this.props.screenProps.onTabChange(2)
    let parent = this.props.screenProps.parent;
    this._readInterval = setInterval(() => {
      setRead(parent.user_id, (res, error) => {

      })
    }, 1000);
  }

  onWillBlur() {
    if (this._readInterval){
      clearInterval(this._readInterval);
    }
  }

  render() {
    let parent = this.props.screenProps.parent;
    let parent_i = this.props.screenProps.parent_i

    drawItem = ({item}) => {
      let align = item.from_id == parent.user_id ? 'flex-start' : 'flex-end'

      return (
        <View style={{
          backgroundColor: colors.message,
          margin: 4,
          padding: 8,
          borderRadius: 15,
          alignSelf: align,
        }}>
          <Text>{item.text}</Text>
        </View>
      )
    }
    
    return (
      <View style={{ flex: 1,
        minHeight: 100,
      }}>
        <FlatList style={{flex: 1, marginTop: 10}}
          data={this.state.messages}
          renderItem = {drawItem}
          ref='msglist'
          onEndReached={() => this.setState({isDown: true})}
          onScroll={() => {this.setState({isDown: false})}}
        />
        <View style={{flexDirection: 'row', alignItems: 'center', 
          paddingLeft: 10, paddingRight: 10, paddingBottom: 6, 
          paddingTop: 6,
        }}>
          <TextInput style={{flex: 1, borderRadius: 10,
            borderWidth: 2,
            borderColor: colors.textInput.border,
            borderRadius: 17,
            fontSize: 16,
            paddingLeft: 6,
            paddingRight: 6,
            paddingTop: 2,
            paddingBottom: 2,
            marginRight: 5}}
            onChangeText={text => this.setState({text})}
          >
            {this.state.text}
          </TextInput>
          <TouchableOpacity onPress={() => this.send()}>
            {/* <IconMat name='send' size={30} /> */}
            <Image source={require('res/icons/Send_invitation.png')}
              style={{width: 38, height: 38, resizeMode: 'contain'}}
            />
          </TouchableOpacity>
        </View>
        <NavigationEvents onWillFocus={() => this.onWillFocus()}
          onWillBlur={() => this.onWillBlur()}
        />
      </View>
    );
  }
}
