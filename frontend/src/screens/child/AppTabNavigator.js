import React, {Component} from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {NavigationActions} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import ChildNavigator from './ChildNavigator';
import { colors, Text } from '../../common';
import FamilyNavigator from '../family/FamilyNavigator';
import ParentRegScreen from '../intro/ParentRegScreen';
import SettingsScreen from '../shared/SettingsScreen';
import Notifications from '../shared/Notifications';
import { getNotifCount } from '../../backend/data';
import SupportScreen from '../shared/Support';
import CatalogScreen from '../shared/Catalog';
import TaskHistoryScreen from '../shared/TaskHistory/TaskHistory';

const backIcon = require('res/icons/Bottom_menu/Back.png')

class TabBarItem extends Component{
  badgeText(){
    let badge = this.props.badge;
    if (badge > 99) {
      return '99+'
    } else {
      return badge + ''
    }
  }

  badgeSize() {
    let badge = this.props.badge;
    if (badge >= 100) {
      return 7
    } else if (badge >= 10) {
      return 9
    } else {
      return 11
    }
  }

  render() {
    let {icon, onPress, size, badge} = this.props
    size = size || 32
    return (
      <TouchableOpacity
        style={{flex: 1, justifyContent: "center", alignItems: "center" }}
        onPress={onPress}
      >
        <View>
          <Image source={icon} 
            style={{
              width: size, height: size,
              resizeMode: 'contain',
            }}
          />
          {!!badge &&
            <View style={{
              position: 'absolute',
              right: 0,
              width: 14, height: 14,
              backgroundColor: 'red',
              borderRadius: 6,
              justifyContent: 'center',
            }}>
              <Text style={{
                fontSize: this.badgeSize(),
                color: 'white',
                textAlign: 'center',
              }}>
                {this.badgeText()}
              </Text>
            </View>
          }
        </View>
      </TouchableOpacity>
    )
  }
}

class CustomTabs extends Component {
  constructor(props){
    super(props)
  
    this.state = {
      notifCount: 0,
    }

    this.refreshData();
  }

  refreshData() {
    getNotifCount((res, error) => {
      if (!error && res){
        if (this.mounted){
          this.setState({notifCount: res.count})
        } else {
          this.state.notifCount = res.count
        }
      }
    })
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 3000);
  }
  
  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this._interval);
  }

  getCurrentRouteName(navState) {
    if (navState.hasOwnProperty('index')) {
      return this.getCurrentRouteName(navState.routes[navState.index])
    } else {
      return navState.routeName
    }
  }

  onBackPress(){
    // this.props.navigation.goBack()
    let routeName = this.getCurrentRouteName(this.props.navigation.state)
    console.log('route ', routeName);
    if (['FamilyChildList', 'Settings',
        'Notifications', 'Catalog', 'Support', 
        'TaskHistory', 'DoneTasks'].includes(routeName)) {
      this.props.navigation.navigate('Home');
    }
    else if (['EditProfile'].includes(routeName)) {
      this.props.navigation.navigate('Settings');
    }
    else {
      this.props.navigation.dispatch(NavigationActions.back())
    }
  }

  onTabPress(index) {
    if (index === 0){
      this.props.navigation.navigate('Home')
    } else if (index === 1){
      this.props.navigation.navigate('Notifications')
    } 
  }

  render(){
    const {
      renderIcon,
      getLabelText,
      activeTintColor,
      inactiveTintColor,
      onTabPress,
      onTabLongPress,
      getAccessibilityLabel,
      navigation
    } = this.props;

    const { routes, index: activeRouteIndex } = navigation.state;

    return (
      
      <View style={{
          width: '100%',
          flexDirection: "row",
          height: 50,
          paddingTop: 4,
          paddingBottom: 6,
          backgroundColor: colors.bottomMenu.background,
      }}>
        
        <TabBarItem icon={backIcon} size={28} key="back"
          onPress={() => this.onBackPress()}
        />

        {routes.map((route, routeIndex) => {
          const isRouteActive = routeIndex === activeRouteIndex;
          const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

          let icon = {
            0: require('res/icons/Bottom_menu/Home.png'),
            1: require('res/icons/Bottom_menu/Notification.png'),
          }[routeIndex]

          return (
            icon && (
              <TabBarItem icon={icon} key={routeIndex} 
                onPress={() => this.onTabPress(routeIndex)}
                badge={routeIndex === 1 && this.state.notifCount}
                size={routeIndex === 0 && 37}
              />
            )
          );
        })}
      </View>
    )
  }
}

// Нижнее меню скнопками
export default createBottomTabNavigator(
{
  ChildMain: {
    screen: ChildNavigator,
  },
  Notifications: {
    screen: Notifications,
  },
  Family: {
    screen: FamilyNavigator,
  },
  EditProfile: {
    screen: (props) => <ParentRegScreen {...props} edit={true}/>,
  },
  Settings: {
    screen: SettingsScreen,
  },
  Support: {
    screen: SupportScreen,
  },
  Catalog: {
    screen: CatalogScreen,
  },
  TaskHistory: {
    screen: TaskHistoryScreen,
  }
},
{
  initialRouteName: 'ChildMain',
  tabBarComponent: CustomTabs,
  backBehavior: 'none',
  defaultNavigationOptions: ({ navigation }) => ({
    
  }),
});
