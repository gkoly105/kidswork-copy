import React, {Component} from 'react';
import { View, FlatList, TouchableHighlight, TouchableOpacity,
        Image } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Collapsible from 'react-native-collapsible';
import {Text, colors, GemIcon} from 'common'
import { buyGift, imageUrl } from 'backend/data';
import { NavigationEvents } from 'react-navigation';
import { TextAlertDialog } from '../../common/dialogs';
import { RoundImage } from '../../common';

class GiftListElem extends Component {

  constructor(props) {
    super(props);

    this.state = {
      alertVisible: false,
      onAlertDone: undefined,
      alertText: '',
    }
  }

  buyAlert(gift_id){
    this.setState({
      alertVisible: true,
      alertText: 'Купить подарок?',
      onAlertDone: () => this.onBuyGift(gift_id),
    })
  }

  onBuyGift(gift_id) {
    buyGift(gift_id, (res, error) => {

    })
  }

  render() {
    let {item} = this.props;

    return (
      <View style={{
        padding: 10, marginLeft: 10,
        marginRight: 10,
      }}
    >
      <TouchableOpacity
        onPress={() => this.props.onItemPress(item)}
      >
        <View>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
            <View>
              { item.image &&
                <RoundImage source={{uri: imageUrl(item.image)}}
                  style={{width: 70, height: 70,
                    borderRadius: 35,
                    marginRight: 20,
                  }}
                />
              }
              { !item.image &&
                <Image source={require('res/icons/Gift.png')}
                  style={{width: 70, height: 70,
                    marginRight: 20,
                    resizeMode: 'contain',
                  }}
                />
              }
            </View>
            <Text style={{flex: 1, fontSize: 14, color: 'black'}}>
              {item.title}
            </Text>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={{fontSize: 14, marginRight: 8}}>
                {item.cost}
              </Text>
              <Image source={require('res/icons/Like.png')}
                style={{
                  width: 23, height: 23,
                  marginRight: 10,
                }}
              />
              { item.bought ? 
                <Image source={require('res/icons/CheckBoxConfirm.png')}
                  style={{width: 22, height: 22, resizeMode: 'contain'}}
                />
                : 
                <Image source={require('res/icons/CheckBox.png')}
                  style={{width: 22, height: 22, resizeMode: 'contain'}}
                />
              }
            </View>
          </View>
        </View>
      </TouchableOpacity>
      
      <Collapsible collapsed={this.props.openedId != item.id}>
        <View style={{paddingTop: 10}}>
          {!!item.desc &&
            <View style={{marginLeft: 10, marginRight: 10}}>
              <Text>{item.desc}</Text>
            </View>
          }
          <View style={{
            marginTop: 10,
            width: '100%',
            flexDirection: 'row',
          }}>
            <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
              onPress={() => this.buyAlert(item.id)}
            >
              {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
              <Image source={require('res/icons/Confirm_grey.png')}
                style={{width: 60, height: 50, resizeMode: 'contain'}}
              />
              <Text style={{fontSize: 9, marginTop: 5}}>Купить</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Collapsible>
      <View style={{width: '100%', height: 2,
        backgroundColor: colors.separator,
        marginTop: 17,
      }}></View>
      <TextAlertDialog visible={this.state.alertVisible}
        setVisible={(v) => this.setState({alertVisible: v})}
        onConfirm={this.state.onAlertDone}
        text={this.state.alertText}
      />
    </View>
    )
  }
}

export default class GiftListScreen extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      openedId: undefined,
    }
  }

  onItemPress = item => {
    this.setState({openedId: this.state.openedId !== item.id ? 
      item.id : undefined})
  }
  
  drawItem({item}) {
    return <GiftListElem
      item={item}
      openedId={this.state.openedId}
      onItemPress={this.onItemPress}
      navigation={this.props.navigation}
    />
  }

  getOpenParam(){
    let gift_id = this.props.navigation.getParam('gift_id')
    this.setState({
      openedId: gift_id
    })
  }

  render() {
    let giftList = this.props.screenProps.parent.giftList;
    let parent_i = this.props.screenProps.parent_i
    
    return (
      <View style={{ flex: 1}}>
        <FlatList style={{marginTop: 10}}
          data={giftList}
          renderItem = {this.drawItem.bind(this)}
        />
        <NavigationEvents onWillFocus={() => this.props.screenProps.onTabChange(1)}
          onDidFocus={this.getOpenParam.bind(this)}
        />
      </View>
    );
  }
}
