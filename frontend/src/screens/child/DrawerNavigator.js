import React, { Component } from 'react';
import {createDrawerNavigator} from 'react-navigation-drawer';
import AppTabNavigator from './AppTabNavigator';
import DrawerPanel from './DrawerPanel';
import IntroSlider from '../shared/IntroSlider';

export default DrawerNavigator = createDrawerNavigator({
  ChildTabs:{
    screen: AppTabNavigator,
  },
  Learn: {
    screen: IntroSlider,
  },
},
{
  initialRouteName: 'ChildTabs',
  contentComponent: DrawerPanel,
  drawerWidth: 250,
});
