
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';
import {createSwitchNavigator} from 'react-navigation';
import LoadingScreen from './intro/LoadingSreen';
import IntroNavigator from './intro/IntroNavigator';
import DrawerNavigatorParent from './main/DrawerNavigator';
import DrawerNavigatorChild from './child/DrawerNavigator';
import { NotifReciever } from '../backend/PushNotifs';

const MainNavigator = createSwitchNavigator(
  {
    Loading: LoadingScreen,
    Auth: IntroNavigator,
    AppParent: DrawerNavigatorParent,
    AppChild: DrawerNavigatorChild
  },
  {
    initialRouteName: 'Loading',
  }
);

export default class MainNavWrap extends Component {
  static router = MainNavigator.router;

  render() {
    return (
      <View style={{width: '100%', height: '100%'}}>
        <MainNavigator
          navigation={this.props.navigation}
        />
        <NotifReciever
          navigation={this.props.navigation}
        />
      </View>
    )
  }
}
