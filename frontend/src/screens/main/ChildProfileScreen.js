import React, {Component} from 'react';
import { View, ImageBackground, Image, TouchableOpacity,
        KeyboardAvoidingView, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import _ from 'lodash';
import {getChildDetails, imageUrl} from 'backend/data';
import IconIon from 'react-native-vector-icons/Ionicons';
import ChildProfileNavigator from './ChildProfileNavigator';
import { Text, colors, TaskProgress, GemIcon, LogoTitleBar, backImage, getAvatar, RoundImage } from './../../common';
import { AwardDialog } from 'common/dialogs';
import { TextAlertDialog } from '../../common/dialogs';

export default class ChildProfileScreen extends React.Component {
  static router = ChildProfileNavigator.router;

  static navigationOptions = ({ navigation, screenProps }) => ({
    // title: `${screenProps.childList[navigation.state.params.child_i].name}`,
    // headerTitleStyle : {textAlign: 'center',alignSelf:'center'},
    //   headerStyle:{
    //       backgroundColor:'white',
    //   },
    // headerTitle: HeaderBar({navigation, screenProps}),
    header: null,
  });

  constructor(props){
    super(props)

    this.state = {
      likeVisible: false,
      dislikeVisible: false,
      activeTab: 0,

      alertVisible: false,
      onAlertDone: undefined,
      alertText: '',
      alertSingleButton: false,
    }
  }

  onTabChange(index) {
    console.log('tab changed to', index);
    this.setState({activeTab: index});
  }

  getChildI() {
    let child_i = this.props.navigation.getParam('child_i');
    if (child_i !== undefined){
      return child_i
    }
    else {
      let child_id = this.props.navigation.getParam('child_id');
      return _.findIndex(this.props.screenProps.childList,
                        (el) => el.id === child_id)
    }
  }

  onAdd(){
    let child_i = this.getChildI();
    let item = this.props.screenProps.childList[child_i];

    if (this.state.activeTab === 0){
      if (item.taskLimit == undefined){
        this.props.navigation.navigate('AddTask', {child_i: child_i})
      }
      else if (item.taskLimit > 0) {
        this.setState({
          alertVisible: true,
          alertText: 'Доступно заданий: ' + item.taskLimit + '.\nТекущий лимит 4 задания / 7 дней.\nПродолжить?',
          onAlertDone: () => this.props.navigation.navigate('AddTask', {child_i: child_i}),
          alertSingleButton: false,
        })
      } else {
        this.setState({
          alertVisible: true,
          alertText: 'Достигнут лимит на количество заданий. Вы можете создать задание позже или оформить подписку.\nТекущий лимит 4 задания / 7 дней.',
          onAlertDone: undefined,
          alertSingleButton: true,
        })
      }
    } else if (this.state.activeTab === 1) {
      this.props.navigation.navigate('AddGift', {child_i: child_i})
    }
  }

  render() {
    let child_i = this.getChildI();
    let item = this.props.screenProps.childList[child_i];

    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        {/* <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            flex: 1,
            backgroundColor: 'transparent',
          }}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{flex: 1}}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
        > */}
        <KeyboardAvoidingView
          style={{flex: 1}}
          behavior={Platform.OS === 'ios' ? "height" : ""}
          keyboardVerticalOffset={55}
        >
          <View style={{
            flex: 1,
            borderRadius: 7,
            margin: 8,
            backgroundColor: colors.childCard.background,
            shadowColor: '#777',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            elevation: 5,
          }}>
            { !this.state.hideTop &&
              <View 
                style={{
                  padding: 14,
                  paddingLeft: 20, 
                  paddingRight: 20,
                }}
              >
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                  <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center'}}
                    onPress={() => this.props.navigation.navigate('TaskList')}
                  >
                    <Image source={require('res/icons/Task.png')}
                      style={{width: 21, height: 21, marginRight: 7, resizeMode: 'contain'}}
                    />
                    <Text >{item.tasksDone} / {item.tasks}</Text>
                  </TouchableOpacity>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('res/icons/Like.png')}
                      style={{width: 26, height: 26, marginRight: 7, resizeMode: 'contain'}}
                    />
                    <Text style={{textAlign: 'center'}}>{item.points}</Text>
                  </View>
                  <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}}
                    onPress={() => this.props.navigation.navigate('GiftList')}
                  >
                    <Image source={require('res/icons/Gift.png')}
                      style={{width: 24, height: 24, marginRight: 7, resizeMode: 'contain'}}
                    />
                    <Text style={{textAlign: 'right'}}>{item.gifts}</Text>
                  </TouchableOpacity>
                </View>
                <View style={{
                  backgroundColor: colors.separator,
                  width: '100%',
                  height: 2,
                  alignSelf: 'center',
                  marginTop: 10,
                  marginBottom: 10,
                }}></View>
                <View style={{flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'center',
                  width: '100%',
                  marginTop: 10,
                }}>
                  <View style={{alignItems: 'center', marginTop: 10, flex: 1}}>
                    { [0, 1].includes(this.state.activeTab) &&
                      <TouchableOpacity
                        onPress={() => {
                          console.log('press')
                          this.onAdd();
                        }}
                      >
                        {/* <IconIon style={{}} name='ios-add-circle' size={80} color='#A4D73D' /> */}
                        <Image source={require('res/icons/Add.png')}
                          style={{width: 70, height: 70}}
                        />
                      </TouchableOpacity>
                    }
                    { [0, 1].includes(this.state.activeTab) &&
                      <Text style={{textAlign: 'center', marginTop: 8}}>
                        { this.state.activeTab === 0 &&
                          'Добавить задание'
                        }
                        { this.state.activeTab === 1 &&
                          'Добавить подарок'
                        }
                        { this.state.activeTab === 2 &&
                          'Добавить задание'
                        }
                      </Text>
                    }
                  </View>

                  <View style={{alignItems: 'center', flex: 1}}>
                    <RoundImage
                      style={{width: 110, height: 110, borderRadius: 55}}
                      source={getAvatar(item.image, 'child', item.gender)}
                    />
                    <Text style={{width: 100, textAlign: 'center', marginTop: 6}}>
                    { item.display_name || item.name}
                    </Text>
                  </View>

                  <View style={{alignItems: 'center', marginLeft: 5,
                      justifyContent: 'flex-start',
                      alignSelf: 'flex-start',
                  }}>
                    <TouchableOpacity onPress={() => this.setState({likeVisible: true})}>
                      <Image
                        style={{width: 42, height: 42}}
                        source={require('res/icons/Like.png')}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({dislikeVisible: true})}>
                      <Image
                        style={{width: 42, height: 42, marginTop: 25}}
                        source={require('res/icons/Dislike.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            }
            <ChildProfileNavigator
              navigation={this.props.navigation}
              screenProps={{
                onInputFocus: (v) => this.setState({hideTop: v}),
                child: item,
                child_i: child_i,
                onTabChange: (v) => this.onTabChange(v),
              }}
            />
          </View>
          <AwardDialog visible={this.state.likeVisible}
            setVisible={(likeVisible) => this.setState({likeVisible})}
            child_id={item.id}
            like={true}
          />
          <AwardDialog visible={this.state.dislikeVisible}
            setVisible={(dislikeVisible) => this.setState({dislikeVisible})}
            child_id={item.id}
            like={false}
          />
          <TextAlertDialog visible={this.state.alertVisible}
            setVisible={(v) => this.setState({alertVisible: v})}
            onConfirm={this.state.onAlertDone}
            text={this.state.alertText}
            singleButton={this.state.alertSingleButton}
          />
        </KeyboardAvoidingView>
        {/* </KeyboardAwareScrollView> */}
      </ImageBackground>
    );
  }
}