import React, {Component} from 'react';
import { View, TextInput, Button, Platform,
  ImageBackground, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Ionicon from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import {addGift, editGift, removeGift, imageUrl} from 'backend/data';
import { Text, colors, styles, LogoTitleBar, backImage, RoundImage } from '../../common';
import { PhotoDialog } from '../../common/dialogs';
import { offerGift, rejectGift } from '../../backend/data';

export default class AddGiftScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    header: null,
  });

  constructor(props){
    super(props)

    this.state = {
      photo: null,
      photoDlgVisible: false,
      mode: 'add', // edit, offer, confirm
      title: '',
      desc: '',
      cost: '',
      time_limit: '',
      errors: {},
      sending: false,
    }

    let gift = this.props.navigation.getParam('gift');
    let parent_id = this.props.navigation.getParam('parent_id');
    let confirm_gift = this.props.navigation.getParam('confirm_gift');

    if (gift) {
      _.merge(this.state, {
        gift_id: gift.id,
        title: gift.title,
        desc: gift.desc,
        cost: gift.cost + '',
        prevPhoto: gift.image,
      })
      this.state.mode = 'edit';
      console.log('edit gift', gift)
    }
    else if (parent_id !== undefined) {
      this.state.mode = 'offer';

    }
    else if (confirm_gift !== undefined) {
      let gift = confirm_gift;
      _.merge(this.state, {
        title: gift.title,
        desc: gift.desc,
        cost: gift.cost + '',
        prevPhoto: gift.image_uri,
      })
      this.state.mode = 'confirm'
    }
  }

  onSubmit(){
    if (this.state.sending){
      return
    }

    if (this.state.mode === 'edit') {
      this.onSubmitEdit();
    } else if (this.state.mode === 'offer') {
      this.onSubmitOffer();
    } else if (this.state.mode === 'confirm') {
      this.onSubmitConfirm();
    } else {
      this.onSubmitAdd();
    }
  }

  onDone() {
    this.props.screenProps.refreshData()
    this.props.navigation.goBack()
  }

  onSubmitAdd(){
    let child_i = this.props.navigation.getParam('child_i')
    let {childList} = this.props.screenProps
    let child = childList[child_i]

    let errors = {}
    let data = {
      child_id: child.id,
    }
    this.state.title = this.state.title.trim()
    if (this.state.title.length == 0){
      errors.title = 'Необходимо заполнить'
    }
    else {
      errors.title = undefined
      data.title = this.state.title
    }

    this.state.desc = this.state.desc.trim()
    data.desc = this.state.desc

    this.state.cost = this.state.cost.trim()
    let cost = Number(this.state.cost)
    if (isNaN(cost) || cost <= 0){
      errors.cost = 'Требуется положительное число'
    } else {
      errors.cost = undefined
      data.cost = cost
    }

    let photoUri = this.state.photo && this.state.photo.uri;

    this.setState({errors})

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      addGift(data, photoUri, (res, error) => {
        this.setState({sending: false});
        if (res){
          this.onDone();
        }
      })
    }
  }

  onSubmitConfirm(){
    let confirm_gift = this.props.navigation.getParam('confirm_gift');
    let child_id = confirm_gift.child_id

    let errors = {}
    let data = {
      child_id: child_id,
    }

    this.state.title = this.state.title.trim()
    if (this.state.title.length == 0){
      errors.title = 'Необходимо заполнить'
    }
    else {
      errors.title = undefined
      data.title = this.state.title
    }

    this.state.desc = this.state.desc.trim()
    data.desc = this.state.desc

    this.state.cost = this.state.cost.trim()
    let cost = Number(this.state.cost)
    if (isNaN(cost) || cost <= 0){
      errors.cost = 'Требуется положительное число'
    } else {
      errors.cost = undefined
      data.cost = cost
    }

    let photoUri = this.state.photo && this.state.photo.uri;
    if (!photoUri) {
      data.image_id = confirm_gift.image_id;
    }
    this.setState({errors})

    let notif_id = this.props.navigation.getParam('notif_id');
    if (notif_id !== undefined){
      data.notif_id = notif_id;
    }

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      addGift(data, photoUri, (res, error) => {
        this.setState({sending: false});
        if (res){
          this.onDone();
        }
      })
    }
  }

  onSubmitEdit(){
    let errors = {}
    let data = {
      gift_id: this.state.gift_id,
    }
    this.state.title = this.state.title.trim()
    if (this.state.title.length == 0){
      errors.title = 'Необходимо заполнить'
    }
    else {
      errors.title = undefined
      data.title = this.state.title
    }

    this.state.desc = this.state.desc.trim()
    data.desc = this.state.desc

    this.state.cost = this.state.cost.trim()
    let cost = Number(this.state.cost)
    if (isNaN(cost) || cost <= 0){
      errors.cost = 'Требуется положительное число'
    } else {
      errors.cost = undefined
      data.cost = cost
    }

    let photoUri = this.state.photo && this.state.photo.uri;

    this.setState({errors})

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      editGift(data, photoUri, (res, error) => {
        this.setState({sending: false});
        if (res){
          this.onDone();
        }
      })
    }
  }

  onSubmitOffer(){
    let parent_id = this.props.navigation.getParam('parent_id')

    let errors = {}
    let data = {
      parent_id: parent_id,
    }

    this.state.title = this.state.title.trim()
    if (this.state.title.length == 0){
      errors.title = 'Необходимо заполнить'
    }
    else {
      errors.title = undefined
      data.title = this.state.title
    }

    this.state.desc = this.state.desc.trim()
    data.desc = this.state.desc

    this.state.cost = this.state.cost.trim()
    let cost = Number(this.state.cost)
    if (isNaN(cost) || cost <= 0){
      errors.cost = 'Требуется положительное число'
    } else {
      errors.cost = undefined
      data.cost = cost
    }

    let photoUri = this.state.photo && this.state.photo.uri;

    this.setState({errors})

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      offerGift(data, photoUri, (res, error) => {
        this.setState({sending: false});
        if (res){
          this.onDone();
        }
      })
    }
  }

  onReject() {
    let notif_id = this.props.navigation.getParam('notif_id');
    this.setState({sending: true});
    rejectGift(notif_id, (res, error) => {
      this.setState({sending: false});
      if (!error){
        this.onDone();
      }
    })
  }

  onRemove() {
    this.setState({sending: true});
    removeGift(this.state.gift_id, (res, error) => {
      this.setState({sending: false});
      this.onDone();
    })
  }

  onPickPhoto() {
    console.log('open photo dialog');
    this.setState({photoDlgVisible: true});
  }

  render(){
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            backgroundColor: 'transparent',
          }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
          keyboardShouldPersistTaps="handled"
        >
          <View style={{
            padding: 16,
            flex: 1,
            borderRadius: 7,
            margin: 8,
            backgroundColor: colors.childCard.background,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 0 },
            shadowOpacity: 0.8,
            shadowRadius: 5,
            elevation: 5,
          }}>
            <Text style={styles.fieldTextReg}>
              Название подарка
            </Text>
            <TextInput
              style={[styles.textInput, {fontSize: 14}]}
              onChangeText={title => this.setState({title})}
              value={this.state.title}
            />
            {!!this.state.errors.title &&
              <Text>{this.state.errors.title}</Text>
            }
            <Text style={styles.fieldTextReg}>
              Описание
            </Text>
            <TextInput
              style={[styles.textInput, {
                height: 100, textAlignVertical: 'top',
              }]}
              multiline={true}
              onChangeText={desc => this.setState({desc})}
              value={this.state.desc}
            />
            {!!this.state.errors.desc &&
              <Text>{this.state.errors.desc}</Text>
            }
            <Text style={styles.fieldTextReg}>
              Стоимость
            </Text>
            <TextInput
              style={[styles.textInput, {fontSize: 14}]}
              onChangeText={cost => this.setState({cost})}
              value={this.state.cost}
            />
            {!!this.state.errors.cost &&
              <Text>{this.state.errors.cost}</Text>
            }
            {/* <TextInput
              placeholder='Закончить до'
              onChangeText={time_limit => this.setState({time_limit})}
              value={this.state.time_limit}
            />
            {!!this.state.errors.time_limit &&
              <Text>{this.state.errors.time_limit}</Text>
            } */}
            <View style={{flexDirection: 'row', alignItems: 'center',
              padding: 2,
            }}>
              <Text style={[styles.fieldTextReg, {}]}>
                Добавить фото <Text style={{fontSize: 10, color: '#777'}}>(не обязательно)</Text>
              </Text>
              <TouchableOpacity onPress={() => this.onPickPhoto()} style={{marginLeft: 'auto'}}>
                { !this.state.photo && !this.state.prevPhoto &&
                  <Image source={require('res/icons/Photo.png')}
                    style={{width: 70, height: 70}}
                  />
                }
                { this.state.photo && 
                  <RoundImage source={{uri: this.state.photo.uri}}
                    style={{width: 70, height: 70, borderRadius: 35}}
                  />
                }
                { !this.state.photo && this.state.prevPhoto &&
                  <RoundImage source={{uri: imageUrl(this.state.prevPhoto)}}
                    style={{width: 70, height: 70, borderRadius: 35}}
                  />
                }
              </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}></View>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity
                style={[styles.dlgButton, {
                  marginTop: 10,
                }]}
                onPress={() => this.onSubmit()}
              >
                <Text style={{ 
                  padding: 3,
                  color: 'black',
                }}>
                  { this.state.mode === 'edit' ? 'Сохранить' : 'Отправить' }
                </Text>
              </TouchableOpacity>
              { this.state.mode === 'edit' &&
                <TouchableOpacity
                  style={[styles.dlgButton, {
                    marginTop: 10, borderColor: 'red',
                  }]}
                  onPress={() => this.onRemove()}
                >
                  <Text style={{ 
                    padding: 3,
                    color: 'black',
                  }}>
                    Удалить
                  </Text>
                </TouchableOpacity>
              }
              { this.state.mode === 'confirm' &&
                <TouchableOpacity
                  style={[styles.dlgButton, {
                    marginTop: 10, borderColor: 'red',
                  }]}
                  onPress={() => this.onReject()}
                >
                  <Text style={{ 
                    padding: 3,
                    color: 'black',
                  }}>
                    Отклонить
                  </Text>
                </TouchableOpacity>
              }
            </View>
          </View>
        </KeyboardAwareScrollView>
        <PhotoDialog visible={this.state.photoDlgVisible}
          setVisible={(v) => this.setState({photoDlgVisible: v})}
          setPhoto={(p) => this.setState({photo: p})}
        />
      </ImageBackground>
    )
  }
}