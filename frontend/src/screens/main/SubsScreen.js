import React, {Component} from 'react';
import * as RNIap from 'react-native-iap';
import { View, ImageBackground, Image, TouchableOpacity, Linking, Platform } from 'react-native';
import { Text, colors, styles, LogoTitleBar, backImage } from '../../common';
import { TextAlertDialog } from '../../common/dialogs';
import { ItemSkus, Items, getSubsAndProds } from '../shared/IAP';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import _ from 'lodash';
import { getPerms, LINKS } from '../../backend/data';

export default class SubsScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      deleteDlgVisible: false,
      subs: [],
    }
  }

  fetchData() {
    let [subs, prods] = getSubsAndProds();
    prods = prods.concat(subs)

    subs = [];
    Items.forEach(item => {
      let prod = prods.find(prod => item.sku === prod.productId)
      subs.push(_.merge(item, {
        price: prod && prod.localizedPrice
      }))
    })
    this.setState({subs})
    
    this.fetchAvail()
  }

  fetchAvail() {
    // RNIap.getAvailablePurchases().then(res => {
    //   let subs = this.state.subs;

    //   res.forEach(prod => {
    //     let sub = subs.find(sub => sub.sku === prod.productId)
    //     if (sub){
    //       sub.active = true;
    //     }
    //   })
    //   this.setState({subs})
    // }).catch(error => {
    //   console.log(error);
    //   setTimeout(() => this.fetchAvail(), 500)
    // })

    getPerms((res, error) => {
      if (!error && res) {
        
        let subs = this.state.subs;
        let active_ids = res.perms.subs;
        subs.forEach(sub => {
          if (active_ids.includes(sub.id)) {
            sub.active = true
          }
          else {
            sub.active = false
          }
        })
        this.setState({subs})
      }
    })
  }

  componentDidMount() {
    this.fetchData();
    this._interval = setInterval(() => this.fetchData(), 3000)
  }

  componentWillUnmount() {
    clearInterval(this._interval);
  }

  buy(item) {
    if (!item.active) {
      setTimeout(() => {
        if (!item.isProduct){
          RNIap.requestSubscription(item.sku)
        }
        else {
          RNIap.requestPurchase(item.sku)
        }
      }, 100)
    }
  }

  drawItem({item}) {
    return (
      <View style={{
        marginLeft: 10,
        marginRight: 10,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#777',
        marginBottom: 20,
        height: 60,
        }}
      >
        <View style={{flex: 1}}>
          <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{flex: 1, fontSize: 14, color: 'black', marginRight: 15,
              marginLeft: 15,
            }}>
              {item.name}
            </Text>

            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                paddingLeft: 12,
                paddingRight: 12,
                paddingTop: 8,
                paddingBottom: 8,
                height: '100%',
                
              }}
            >
              { !item.active &&
                <Text style={{fontSize: 14, color: 'white'}}>
                  {item.price + (item.isProduct ? '' : item.period)}
                </Text>
              }
              { item.active &&
                <Text style={{fontSize: 16, color: 'white'}}>
                  активна
                </Text>
              }
            </View>

            <TouchableOpacity
              style={{
                // borderWidth: 3,
                // borderColor: colors.menuButton,
                borderRadius: 30,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                paddingLeft: 12,
                paddingRight: 12,
                paddingTop: 8,
                paddingBottom: 8,
                backgroundColor: item.active ? '#fcb330' : '#b4cc80',
                height: 60,
                position: 'absolute',
                right: -2,
              }}
              onPress={() => this.buy(item)}
            >
              { !item.active &&
                <Text style={{fontSize: 14, color: 'white'}}>
                  {item.price ? 
                    (item.price + (item.isProduct ? '' : item.period))
                    :
                    'купить'
                  }
                </Text>
              }
              { item.active &&
                <Text style={{fontSize: 16, color: 'white'}}>
                  активна
                </Text>
              }
            </TouchableOpacity>
          </View>
          {/* <View style={{width: '100%', height: 2,
            backgroundColor: colors.separator,
            marginTop: 17,
          }}></View> */}
        </View>
      </View>
    )
  }

  render() {
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        <ScrollView style={{
          flex: 1,
          borderRadius: 7,
          margin: 8,
          backgroundColor: colors.childCard.background,
          shadowColor: '#777',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.5,
          shadowRadius: 5,
          elevation: 5,
        }}>
          <Text style={{fontSize: 18, textAlign: 'center', marginBottom: 3,
            marginTop: 5}}
          >
            Подписки
          </Text>
          {/* <FlatList style={{marginTop: 10}}
            data={this.state.subs}
            renderItem = {this.drawItem.bind(this)}
          /> */}
          <View style={{marginTop: 10}}>
            {
              this.state.subs.map(item => this.drawItem({item}))
            }
          </View>
          { Platform.OS === 'ios' &&
            <View>
              <Text style={{marginTop: 10, padding: 10, fontSize: 12, textAlign: 'justify'}}>
                Оплата производится через аккаунт iTunes при подтверждении покупки.
                Подписки автоматически продлеваются в конце срока действия и с учетной записи iTunes списывается плата.
                Подписку можно отменить не позднее чем за 24 часа до окончания текущего периода.
                Можно отключить автоматическое продление и отказаться от подписки в настройках учетной записи iTunes.
                Плата за неиспользованный срок подписки не возвращается.

                Продолжив вы соглашаетесь с
                <Text style={{color: colors.hyperlink}}
                  onPress={() => Linking.openURL(LINKS.RULES)}
                >
                  {" Правилами пользования "}
                </Text>
                и
                <Text style={{color: colors.hyperlink}}
                  onPress={() => Linking.openURL(LINKS.POLICY)}
                >
                  {" Политикой конфиденциальности персональных данных "}
                </Text>
              </Text>
            </View>
          }
        </ScrollView>
        <TextAlertDialog visible={this.state.deleteDlgVisible}
          setVisible={(v) => this.setState({deleteDlgVisible: v})}
          onConfirm={() => this.onDeleteAccount()}
          text={'Вы точно хотите полность удалить свой аккаунт? Это действие нельзя будет оменить!'}
        />
      </ImageBackground>
    )
  }
}
