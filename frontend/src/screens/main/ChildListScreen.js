import React, {Component, useState} from 'react';
import {Platform, StyleSheet, View, Modal, StatusBar,
  FlatList, TextInput, Image, TouchableOpacity, ImageBackground} from 'react-native';
import {getChildList, imageUrl} from 'backend/data';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconMat from 'react-native-vector-icons/MaterialIcons';
import IconMatCom from 'react-native-vector-icons/MaterialCommunityIcons';
import IconIon from 'react-native-vector-icons/Ionicons';
import { Text, colors, styles, LogoTitleBar, backImage } from 'common';
import { AwardDialog } from 'common/dialogs';
import { getAvatar } from '../../common';
import { TextAlertDialog } from '../../common/dialogs';

class ChildListElem extends Component {
  constructor (props) {
    super(props)

    this.state = {
      likeVisible: false,
      dislikeVisible: false,

      alertVisible: false,
      onAlertDone: undefined,
      alertText: '',
      alertSingleButton: false,
    }
  }

  onAdd() {
    let {item, index} = this.props;

    if (item.taskLimit == undefined){
      this.props.navigation.navigate('AddTask', {child_i: index})
    }
    else if (item.taskLimit > 0) {
      this.setState({
        alertVisible: true,
        alertText: 'Доступно заданий: ' + item.taskLimit + '.\nТекущий лимит 4 задания / 7 дней.\nПродолжить?',
        onAlertDone: () => this.props.navigation.navigate('AddTask', {child_i: index}),
        alertSingleButton: false,
      })
    } else {
      this.setState({
        alertVisible: true,
        alertText: 'Достигнут лимит на количество заданий. Вы можете создать задание позже или оформить подписку.\nТекущий лимит 4 задания / 7 дней.',
        onAlertDone: undefined,
        alertSingleButton: true,
      })
    }
  }

  render() {
    let {item, index} = this.props;

    return (
      <TouchableOpacity
        style={{
          flex: 1,
          padding: 14,
          paddingLeft: 20,
          paddingRight: 20,
          borderRadius: 7,
          margin: 4,
          marginBottom: 15,
          backgroundColor: colors.childCard.background,
          shadowColor: '#777',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.5,
          shadowRadius: 5,
          elevation: 5,
        }}
        onPress={() =>
          this.props.navigation.navigate('ChildProfile', {
            child_i: index,
          })
        }
      >
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
          <View style={{ flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('res/icons/Task.png')}
              style={{width: 21, height: 21, marginRight: 7, resizeMode: 'contain'}}
            />
            <Text >{item.tasksDone} / {item.tasks}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('res/icons/Like.png')}
              style={{width: 26, height: 26, marginRight: 7, resizeMode: 'contain'}}
            />
            <Text style={{textAlign: 'center'}}>{item.points}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('res/icons/Gift.png')}
              style={{width: 24, height: 24, marginRight: 7, resizeMode: 'contain'}}
            />
            <Text style={{textAlign: 'right'}}>{item.gifts}</Text>
          </View>
        </View>
        <View style={{
          backgroundColor: colors.separator,
          width: '100%',
          height: 2,
          alignSelf: 'center',
          marginTop: 10,
          marginBottom: 10,
        }}/>
        <View style={{flexDirection: 'row',
          alignItems: 'center',
          alignSelf: 'center',
          width: '100%',
          marginTop: 10,
        }}>
          <View style={{alignItems: 'center', marginTop: 10, flex: 1}}>
            <TouchableOpacity
              onPress={() => {
                this.onAdd()
              }}
            >
              {/* <IconIon style={{}} name='ios-add-circle' size={80} color='#A4D73D' /> */}
              <Image source={require('res/icons/Add.png')}
                style={{width: 70, height: 70}}
              />
            </TouchableOpacity>
            <Text style={{textAlign: 'center', marginTop: 8}}>
              Добавить задание
            </Text>
          </View>

          <View style={{alignItems: 'center', flex: 1}}>
            <Image
              style={{width: 110, height: 110, borderRadius: 55}}
              source={getAvatar(item.image, 'child', item.gender)}
            />
            <Text style={{width: 100, textAlign: 'center', marginTop: 6}}>
              { item.display_name || item.name}
            </Text>
          </View>
          <View style={{alignItems: 'center', marginLeft: 5,
              justifyContent: 'flex-start',
              alignSelf: 'flex-start',
          }}>
            <TouchableOpacity onPress={() => this.setState({likeVisible: true})}>
              <Image
                style={{width: 42, height: 42}}
                source={require('res/icons/Like.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({dislikeVisible: true})}>
              <Image
                style={{width: 42, height: 42, marginTop: 25}}
                source={require('res/icons/Dislike.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <AwardDialog visible={this.state.likeVisible}
          setVisible={(likeVisible) => this.setState({likeVisible})}
          child_id={item.id}
          like={true}
        />
        <AwardDialog visible={this.state.dislikeVisible}
          setVisible={(dislikeVisible) => this.setState({dislikeVisible})}
          child_id={item.id}
          like={false}
        />
        <TextAlertDialog visible={this.state.alertVisible}
          setVisible={(v) => this.setState({alertVisible: v})}
          onConfirm={this.state.onAlertDone}
          text={this.state.alertText}
          singleButton={this.state.alertSingleButton}
        />
      </TouchableOpacity>
    )
  }
}

export default class ChildListScreen extends Component {
  static navigationOptions = {
    header: null
  }

  constructor (props) {
    super(props);
  }

  drawItem = ({item, index}) => {
    return (
      <ChildListElem item={item} index={index} navigation={this.props.navigation} />
    )
  }

  render(){
    let childList = this.props.screenProps.childList
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <View style={{flex: 1}}>
          <LogoTitleBar navigation={this.props.navigation} />
          <FlatList
            data={childList}
            renderItem = {this.drawItem}
            keyboardShouldPersistTaps="handled"
          />
        </View>
      </ImageBackground>
    )
  }
}

