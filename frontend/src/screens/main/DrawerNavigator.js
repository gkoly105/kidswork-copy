import React, { Component } from 'react';
import {createDrawerNavigator} from 'react-navigation-drawer';
import AppTabNavigator from './AppTabNavigator';
import DrawerPanel from './DrawerPanel';
import IntroSlider from '../shared/IntroSlider';

export default DrawerNavigator = createDrawerNavigator({
  ParentTabs:{
    screen: AppTabNavigator,
  },
  Learn: {
    screen: IntroSlider,
  },
},
{
  initialRouteName: 'ParentTabs',
  contentComponent: DrawerPanel,
  drawerWidth: 250,
});
