import React, {Component} from 'react';
import { View, TextInput, Button, Platform,
      ImageBackground, TouchableOpacity,
      CheckBox, Switch, Picker } from 'react-native';
import DateTimePicker from "react-native-modal-datetime-picker";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import moment from 'moment';
import momentRu from 'moment/locale/ru';
moment.locale('ru');
import Ionicon from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import {addTask, editTask, removeTask} from 'backend/data';
import { Text, colors, styles, LogoTitleBar,
        backImage, BoolInput, dateInputAssist, timeInputAssist, datetimeInputAssist, formatDate, formatTime, formatDateTime } from '../../common';
import { offerTask, rejectTask, getPerms } from '../../backend/data';

export default class AddTaskScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    header: null,
  });

  constructor(props){
    super(props)

    this.state = {
      mode: 'add', // edit, offer, confirm
      title: '',
      desc: '',
      reward: '',
      require_image: false,
      limit_date: '',
      limit_time: '',
      notify_time: '',
      notify: '1',
      notify_repeat: '',
      repeat: '1',
      use_limit: false,
      use_limit_time: false,
      use_notify: false,
      use_notify_repeat: false,
      use_repeat: false,
      isLimitDateVisible: false,
      isLimitTimeVisible: false,
      errors: {},
      sending: false,
      full: false, // for premium version
    }

    let task = this.props.navigation.getParam('task');
    let parent_id = this.props.navigation.getParam('parent_id');
    let confirm_task = this.props.navigation.getParam('confirm_task');
    let repeatTask = this.props.navigation.getParam('repeat_task');

    if (task) {
      this.extractTask(task);
      this.state.mode = 'edit';
      console.log('edit task', task)
    }
    else if (parent_id !== undefined) {
      this.state.mode = 'offer';
    }
    else if (confirm_task !== undefined) {
      this.extractTask(confirm_task);
      this.state.mode = 'confirm'
    } else if (repeatTask !== undefined) {
      this.extractTask(repeatTask);
    }

    // check if premium version
    getPerms((res, error) => {
      if (!error && res) {
        let full = false;
        let parent_id = this.props.navigation.getParam('parent_id')
        if (res.role === 'parent') {
          full = res.perms.tasks;
        } else if (res.role == 'child' && parent_id !== undefined) {
          full = res.perms[parent_id].tasks;
        }
        this.setState({full})
      }
    })
  }

  extractTask(task) {
    _.merge(this.state, {
      task_id: task.id,
      title: task.title,
      desc: task.desc,
      reward: task.reward + '',
    })
    this.state.require_image = task.require_image;
    if (task.time_limit){
      this.state.use_limit = true;
      this.state.limit_date = new Date(moment.utc(task.time_limit)
                                        .local().startOf('day').toString());
      let t = new Date(moment.utc(task.time_limit)
                      .local().toString());
      if (t !== '00:00'){
        this.state.use_limit_time = true
        this.state.limit_time = t;
      }
    }
    if (task.repeat){
      this.state.repeat = task.repeat + '';
      this.state.use_repeat = true;
    }
    if (task.notify_time){
      this.state.use_notify = true;
      let diff = moment.utc(task.time_limit)
                            .diff(moment.utc(task.notify_time));

      this.state.notify = moment.duration(diff).asHours() + '';
      if (task.notify_repeat){
        this.state.notify_repeat = task.notify_repeat + '';
        this.state.use_notify_repeat = true;
      }
    }
  }

  onSubmit(){
    if (this.state.sending){
      return
    }

    if (this.state.mode === 'edit') {
      this.onSubmitEdit();
    } else if (this.state.mode === 'offer') {
      this.onSubmitOffer();
    } else if (this.state.mode === 'confirm') {
      this.onSubmitConfirm();
    } else {
      this.onSubmitAdd();
    }
  }

  onDone() {
    this.props.screenProps.refreshData()
    this.props.navigation.goBack()
  }

  onReject() {
    let notif_id = this.props.navigation.getParam('notif_id');
    rejectTask(notif_id, (res, error) => {
      if (!error){
        this.props.screenProps.refreshData()
        this.props.navigation.goBack()
      }
    })
  }

  markError(name){
    if (this.state.errors[name]){
      return {borderColor: 'red', borderWidth: 2}
    }
    return {}
  }

  validate(data, errors) {
    this.state.title = this.state.title.trim()
    if (this.state.title.length == 0){
      errors.title = 'Необходимо заполнить'
    }
    else {
      errors.title = undefined
      data.title = this.state.title
    }

    this.state.desc = this.state.desc.trim()
    data.desc = this.state.desc

    this.state.reward = this.state.reward.trim()
    let reward = Number(this.state.reward)
    if (isNaN(reward) || reward <= 0){
      errors.reward = 'Требуется положительное число'
    } else {
      errors.reward = undefined
      data.reward = reward
    }

    data.require_image = this.state.require_image;

    let limit_time;
    if (this.state.use_limit && this.state.use_limit_time){
      limit_time = moment(this.state.limit_time);
      if (!limit_time.isValid()){
        errors.limit_time = true;
      }
    }

    let limit_date;
    if (this.state.use_limit){
      limit_date = moment(this.state.limit_date);
      if (!limit_date.isValid()){
        errors.limit_date = true;
      }
    }

    if (this.state.limit_time && !this.state.limit_date){
      errors.limit_date = true;
    }

    let notify;
    if (this.state.use_notify){
      notify = Number(this.state.notify);
      if (!this.state.notify.length || isNaN(notify)){
        errors.notify_time = true;
      }
    }

    if (this.state.use_repeat){
      repeat = Number(this.state.repeat)
      if (!this.state.repeat.length || isNaN(repeat)){
        errors.repeat = true;
      }
      else{
        data.repeat = repeat;
      }
    }

    if (JSON.stringify(errors) == '{}'){
      if (this.state.use_limit){
        limit_date = limit_date.startOf('day');
        if (this.state.use_limit_time){
          
          limit_date.add(limit_time.hours(), 'hours');
          limit_date.add(limit_time.minutes(), 'minutes');
        }
        data.time_limit = limit_date.toISOString()

        if (this.state.use_notify) {
          limit_date.subtract(notify, 'hours')
          data.notify_time = limit_date.toISOString();
        }
      }
    }
  }

  onSubmitAdd(){
    let child_id;
    let child_i = this.props.navigation.getParam('child_i')
    if (child_i !== undefined){
      let {childList} = this.props.screenProps
      child_id = childList[child_i].id
    } else {
      child_id = this.props.navigation.getParam('child_id')
    }

    let errors = {}
    let data = {
      child_id: child_id,
    }
    
    this.validate(data, errors);

    this.setState({errors})

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      addTask(data, (res, error) => {
        this.setState({sending: false});
        if (!error){
          this.onDone();
        }
      })
    }
  }

  onSubmitConfirm(){
    let confirm_task = this.props.navigation.getParam('confirm_task');
    let child_id = confirm_task.child_id

    let errors = {}
    let data = {
      child_id: child_id,
    }
    
    this.validate(data, errors);

    let notif_id = this.props.navigation.getParam('notif_id');
    if (notif_id !== undefined){
      data.notif_id = notif_id;
    }

    this.setState({errors})

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      addTask(data, (res, error) => {
        this.setState({sending: false});
        if (!error){
          this.onDone();
        }
      })
    }
  }

  onSubmitEdit(){
    let errors = {}
    let data = {
      task_id: this.state.task_id,
    }

    this.validate(data, errors);

    this.setState({errors})

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      editTask(data, (res, error) => {
        this.setState({sending: false});
        if (!error){
          this.onDone();
        }
      })
    }
  }

  onSubmitOffer() {
    let parent_id = this.props.navigation.getParam('parent_id')

    let errors = {}
    let data = {
      parent_id: parent_id,
    }
    
    this.validate(data, errors);

    this.setState({errors})

    if (JSON.stringify(errors) == '{}'){
      console.log('submit ok')
      this.setState({sending: true});
      offerTask(data, (res, error) => {
        this.setState({sending: false});
        if (!error){
          this.onDone();
        }
      })
    }
  }

  onRemove() {
    removeTask(this.state.task_id, (res, error) => {
      this.onDone();
    })
  }

  inactiveStyle(inactive) {
    return inactive ? {
      opacity: 0.4,
    } : null;
  }

  onChangeLimitDate(date) {
    date = dateInputAssist(this.state.limit_date, date)
    this.setState({limit_date: date});
  }

  onChangeLimitTime(time) {
    time = timeInputAssist(this.state.limit_time, time)
    this.setState({limit_time: time});
  }

  onChangeNotifyTime(time) {
    time = datetimeInputAssist(this.state.notify_time, time)
    this.setState({notify_time: time});
  }

  onLimitDatePicked(date) {
    this.setState({isLimitDateVisible: false, limit_date: date})
    console.log('date picked', date);
  }

  onLimitTimePicked(time) {
    this.setState({isLimitTimeVisible: false, limit_time: time})
    console.log('time picked', time);
  }

  render(){
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        <KeyboardAwareScrollView
          style={{
            backgroundColor: 'transparent',
          }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
          keyboardShouldPersistTaps="handled"
        >
          <View style={{
            padding: 16,
            flex: 1,
            borderRadius: 7,
            margin: 8,
            backgroundColor: colors.childCard.background,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 0 },
            shadowOpacity: 0.8,
            shadowRadius: 5,
            elevation: 5,
          }}>
            <Text style={styles.fieldText}>
              Задание
            </Text>
            <TextInput
              style={[styles.textInput, {fontSize: 14}]}
              onChangeText={title => this.setState({title})}
              value={this.state.title}
            />
            {!!this.state.errors.title &&
              <Text>{this.state.errors.title}</Text>
            }
            <Text style={styles.fieldText}>
              Описание
            </Text>
            <TextInput
              style={[styles.textInput, {
                height: 100, textAlignVertical: 'top',
              }]}
              multiline={true}
              onChangeText={desc => this.setState({desc})}
              value={this.state.desc}
            />
            {!!this.state.errors.desc &&
              <Text>{this.state.errors.desc}</Text>
            }
            <Text style={styles.fieldText}>
              Стоимость выполнения
            </Text>
            <TextInput
              style={[styles.textInput, {fontSize: 14}]}
              onChangeText={reward => this.setState({reward})}
              value={this.state.reward}
            />
            {!!this.state.errors.reward &&
              <Text>{this.state.errors.reward}</Text>
            }
            { this.state.full &&
              <View>
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                  <View style={{}}>
                    <BoolInput
                      value={this.state.use_limit}
                      onValueChange={(use_limit) => this.setState({use_limit})}
                    />
                  </View>
                  <Text style={styles.fieldText}>
                    Выполнить до
                  </Text>
                </View>
                <View style={[{flexDirection: 'row', alignItems: 'center'},
                  
                ]}>
                  <View style={[{flex: 1}, this.inactiveStyle(!this.state.use_limit)]}>
                    <Text style={styles.fieldText}>
                      Дата
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        console.log('press date')
                        this.state.use_limit && this.setState({isLimitDateVisible: true})
                      }}
                      style={[styles.textInput, this.markError('limit_date')]}
                    >
                      <Text style={{fontSize: 14}}>
                        { this.state.limit_date &&
                          formatDate(this.state.limit_date)
                        }
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, marginLeft: 25}}>
                    <Text style={[styles.fieldText, this.inactiveStyle(!this.state.use_limit)]}>
                      Время
                    </Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <BoolInput
                        disabled={!this.state.use_limit}
                        style={{marginTop: 5, marginBottom: 10, marginRight: 7}}
                        value={this.state.use_limit_time}
                        onValueChange={(use_limit_time) => this.setState({use_limit_time})}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          console.log('press date')
                          this.state.use_limit && this.state.use_limit_time && 
                            this.setState({isLimitTimeVisible: true})
                        }}
                        style={[styles.textInput, { flex: 1},
                          this.inactiveStyle(!this.state.use_limit || !this.state.use_limit_time),
                          this.markError('limit_time')]
                        }
                      >
                        <Text style={{fontSize: 14}}>
                          { this.state.limit_time &&
                            formatTime(this.state.limit_time)
                          }
                        </Text>
                      </TouchableOpacity>
                      {/* <TextInput
                        editable={this.state.use_limit && this.state.use_limit_time}
                        style={[styles.textInput, {fontSize: 14, flex: 1},
                          this.inactiveStyle(!this.state.use_limit || !this.state.use_limit_time),
                          this.markError('limit_time')]}
                        onChangeText={this.onChangeLimitTime.bind(this)}
                        value={this.state.limit_time}
                      /> */}
                    </View>
                  </View>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View style={{}}>
                    <BoolInput
                      value={this.state.require_image}
                      onValueChange={(require_image) => this.setState({require_image})}
                    />
                  </View>
                  <Text style={styles.fieldText}>
                    Фото подтверждение
                  </Text>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                  <View style={{flex: 1.4, flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{}}>
                      <BoolInput
                        value={this.state.use_repeat}
                        onValueChange={(use_repeat) => this.setState({use_repeat})}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <Text style={[styles.fieldText, {width: '90%'}]}>
                        Повторять каждый
                      </Text>
                    </View>
                  </View>
                  <View style={{flex: 1}}>
                    {/* <TextInput
                      editable={this.state.use_repeat}
                      style={[styles.textInput, {fontSize: 14},
                        this.inactiveStyle(!this.state.use_repeat),
                        this.markError('repeat')]}
                      onChangeText={(repeat) => this.setState({repeat})}
                      value={this.state.repeat}
                      placeholder='как часто'
                    /> */}
                    {this.state.use_repeat && 
                      <View style={{}}>
                        <Picker
                          selectedValue={this.state.repeat}
                          onValueChange={(repeat) => this.setState({repeat})}
                          style={[{},
                            this.inactiveStyle(!this.state.use_repeat),
                            this.markError('repeat')]}
                          enabled={this.state.use_repeat}
                          itemStyle={{
                            fontSize: 12,
                            color: 'black',
                            // height: 60,
                          }}
                        >
                          <Picker.Item label="день" value="1" />
                          <Picker.Item label="2 дня" value="2" />
                          <Picker.Item label="неделю" value="7" />
                          <Picker.Item label="месяц" value="30" />
                        </Picker>
                      </View>
                    }
                  </View>
                </View>

                { this.state.use_limit &&
                  <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                    <View style={{flex: 1.4, flexDirection: 'row'}}>
                      <View style={{}}>
                        <BoolInput
                          value={this.state.use_notify}
                          onValueChange={(use_notify) => this.setState({use_notify})}
                        />
                      </View>
                      <Text style={styles.fieldText}>
                        Напомнить
                      </Text>
                    </View>
                    <View style={{flex: 1}}>
                      {this.state.use_notify && 
                        <View style={{}}>
                          <Picker
                            selectedValue={this.state.notify}
                            onValueChange={(notify) => this.setState({notify})}
                            style={[{fontSize: 14},
                              this.inactiveStyle(!this.state.use_notify),
                              this.markError('notify_time')]}
                            enabled={this.state.use_notify}
                            itemStyle={{
                              fontSize: 14,
                              color: 'black',
                              // height: 60,
                            }}
                          >
                            <Picker.Item label="час" value="1" />
                            <Picker.Item label="2 часа" value="2" />
                            <Picker.Item label="день" value="24" />
                            <Picker.Item label="2 дня" value="48" />
                          </Picker>
                        </View>
                      }
                      {/* <TouchableOpacity
                        onPress={() => {
                          console.log('press date')
                          this.state.use_notify && 
                            this.setState({isNotifyTimeVisible: true})
                        }}
                        style={[styles.textInput, {fontSize: 14},
                          this.inactiveStyle(!this.state.use_notify),
                          this.markError('notify_time')]
                        }
                      >
                        <Text style={{fontSize: 14}}>
                          { this.state.notify_time &&
                            formatDateTime(this.state.notify_time)
                          }
                        </Text>
                      </TouchableOpacity> */}
                      {/* <TextInput
                        editable={this.state.use_notify}
                        style={[styles.textInput, {fontSize: 14},
                          this.inactiveStyle(!this.state.use_notify),
                          this.markError('notify_time')]}
                        onChangeText={this.onChangeNotifyTime.bind(this)}
                        value={this.state.notify_time}
                        placeholder='дд.мм.гггг чч:мм'
                      /> */}
                    </View>
                  </View>
                }
              </View>
            }

            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity
                style={[styles.dlgButton, {
                  marginTop: 10,
                }]}
                onPress={() => this.onSubmit()}
              >
                <Text style={{ 
                  padding: 3,
                  color: 'black',
                }}>
                  { this.state.mode === 'edit' ? 'Сохранить' : 'Отправить' }
                </Text>
              </TouchableOpacity>
              { this.state.mode === 'edit' &&
                <TouchableOpacity
                  style={[styles.dlgButton, {
                    marginTop: 10, borderColor: 'red',
                  }]}
                  onPress={() => this.onRemove()}
                >
                  <Text style={{ 
                    padding: 3,
                    color: 'black',
                  }}>
                    Удалить
                  </Text>
                </TouchableOpacity>
              }
              { this.state.mode === 'confirm' &&
                <TouchableOpacity
                  style={[styles.dlgButton, {
                    marginTop: 10, borderColor: 'red',
                  }]}
                  onPress={() => this.onReject()}
                >
                  <Text style={{ 
                    padding: 3,
                    color: 'black',
                  }}>
                    Отклонить
                  </Text>
                </TouchableOpacity>
              }
            </View>
          </View>
        </KeyboardAwareScrollView>
        <DateTimePicker
          isVisible={this.state.isLimitDateVisible}
          onConfirm={this.onLimitDatePicked.bind(this)}
          onCancel={() => this.setState({isLimitDateVisible: false})}
        />
        <DateTimePicker
          isVisible={this.state.isLimitTimeVisible}
          onConfirm={this.onLimitTimePicked.bind(this)}
          onCancel={() => this.setState({isLimitTimeVisible: false})}
          mode='time'
        />
      </ImageBackground>
    )
  }
}