
import React, {Component} from 'react';
import {AsyncStorage, View} from 'react-native';
import {createAppContainer, NavigationEvents} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import ChildListScreen from './ChildListScreen';
import ChildProfileScreen from './ChildProfileScreen';
import AddTaskScreen from './AddTaskScreen';
import AddGiftScreen from './AddGiftScreen';
import {getChildList} from 'backend/data';
import { getPerms } from '../../backend/data';
import { PromoSubDialog } from '../../common/dialogs';
import moment from 'moment';

const ChildNavStack = createStackNavigator(
  {
    Home: {
      screen: ChildListScreen,
    },
    ChildProfile: {
      screen: ChildProfileScreen,
    },
    AddTask: {
      screen: AddTaskScreen,
    },
    AddGift: {
      screen: AddGiftScreen,
    },
  },
  {
    initialRouteName: 'Home'
  }
)

export default class ChildNavigator extends Component {
  static router = ChildNavStack.router;

  constructor(props){
    super(props)

    this.state = {
      childList: [],
      empty: false,
      promoVisible: false,
    }

    this.refreshData()

    AsyncStorage.getItem('learned').then(v => {
      if (!v) {
        this.props.navigation.navigate('Learn');
      }
    })
  }

  refreshData(func){
    getChildList((list) => {
      if (list){
        let empty = list.length === 0;
        if (empty && this.props.navigation.isFocused()){
          this.props.navigation.navigate('FamilyMain')
          return
        }

        if (this.mounted){
          // console.log('set state')
          this.setState({childList: list, empty})
        }
        else {
          // console.log('set state directly')
          this.state.childList = list;
          this.state.empty = empty;
        }
      }
    })
  }

  checkPromo() {
    if (moment.utc() > moment.utc('2019-10-26'))
    {
      getPerms((res, error) => {
        if (!error && res) {
          if (res.perms.hasTrial) {
            AsyncStorage.getItem('learned').then(learned => {
              if (learned) {
                AsyncStorage.getItem('trial').then(v => {
                  if (!v || 1) {
                    this.setState({promoVisible: true})
                    AsyncStorage.setItem('trial', '1');
                  }
                })
              }
            })
          }
        }
      })
    }
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 3000);
  }
  
  componentWillUnmount() {
    clearInterval(this._interval);
  }

  onDidFocus() {
    if (this.state.empty){
      this.props.navigation.navigate('FamilyMain')
    }

    setTimeout(() => this.checkPromo(), 300);
  }

  render(){
    return (
      <View style={{flex: 1}}>
        <ChildNavStack 
          navigation={this.props.navigation}
          screenProps={{
            childList: this.state.childList,
            refreshData: () => this.refreshData(),
          }}
        />
        <PromoSubDialog visible={this.state.promoVisible}
          setVisible={v => this.setState({promoVisible: v})}
        />
        <NavigationEvents onDidFocus={this.onDidFocus.bind(this)} />
      </View>
    )
  }
}
