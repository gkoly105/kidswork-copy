import React, {Component} from 'react';
import { Image, View, TouchableOpacity, Animated } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconF5 from 'react-native-vector-icons/FontAwesome5';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconIon from 'react-native-vector-icons/Ionicons';
import { createMaterialTopTabNavigator} from 'react-navigation-tabs';
import GiftListScreen from './GiftListScreen';
import TaskListScreen from './TaskListScreen';
import {imageUrl} from 'backend/data';
import { Text, colors, TaskProgress, cutOne } from 'common';
import ChatScreen from './ChatScreen';

class CustomTabs extends Component {
  constructor(props){
    super(props)

    this.state = {
      scrOffset: 0,
      scrPan: 0,
    }

    // this.props.panX.addListener(({value: x}) => {
    //   // console.log('pan', x)
    //   this.setState({scrPan: -x / 360});
    // });

    // this.props.offsetX.addListener(({value: x}) => {
    //   // console.log('off', x)
    //   this.setState({scrOffset: -x / 360});
    // });
  }

  render(){
    let child = this.props.screenProps.child;
    let item = child;

    const {
      renderIcon,
      getLabelText,
      activeTintColor,
      inactiveTintColor,
      onTabPress,
      onTabLongPress,
      getAccessibilityLabel,
      navigation
    } = this.props;

    const { routes, index: activeRouteIndex } = navigation.state;

    // console.log(this.props);

    // let animVal = this.props.panX.interpolate({
    //   inputRange: [-360, 0],
    //   outputRange: [0.5, 1],
    // });

    return (
      
      <View style={{
          width: '100%',
          flexDirection: "row",
          height: 40,
          paddingLeft: 10,
          paddingRight: 10,
      }}>
        {routes.map((route, routeIndex) => {
          const isRouteActive = routeIndex === activeRouteIndex;
          const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;
          {/* const icon = {
            0: <IconAnt name='gift' size={40} color={tintColor} />,
            1: <IconF5 name='tasks' size={36} color={tintColor} />,
          }[routeIndex]; */}

          const iconBack = {
            0: colors.gift.background,
            1: colors.task.background,
            2: colors.chat.background,
          }[routeIndex];

          const tabText = {
            0: 'Задания',
            1: 'Подарки',
            2: 'Сообщения',
          }[routeIndex]

          let animVal = (this.state.scrOffset + this.state.scrPan);
          let opacity = 0.6 + cutOne(1 - Math.abs(animVal - routeIndex)) * 0.4
          let lineOpacity = cutOne(1 - Math.abs(animVal - routeIndex))

          return (
            <TouchableOpacity
              key={routeIndex}
              style={{flex: 1, justifyContent: "center", alignItems: "center" }}
              onPress={() => {
                onTabPress({ route });
              }}
              onLongPress={() => {
                onTabLongPress({ route });
              }}
              accessibilityLabel={getAccessibilityLabel({ route })}
            >
              <View style={{
                // backgroundColor: iconBack,
                // paddingLeft: 12,
                // paddingRight: 12,
                paddingTop: 7,
                paddingBottom: 7,
                // width: 56,
                // height: 56,
                opacity: opacity,
                justifyContent: 'center', alignItems: 'center',
                // borderRadius: 28,
              }}>
                <Text>{tabText}</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  height: 4,
                  backgroundColor: colors.menuButton,
                  opacity: lineOpacity,
                }}
              />
            
            </TouchableOpacity>
          );
        })}
      </View>
    )
  }
}

export default createMaterialTopTabNavigator(
{
  TaskList: {
    screen: TaskListScreen,
    navigationOptions: () => ({
      title: 'Задания',
    }),
  },
  GiftList: {
    screen: GiftListScreen,
    navigationOptions: () => ({
      title: 'Подарки',
    }),
  },
  ChatScreen: {
    screen: ChatScreen,
    navigationOptions: () => ({
      title: 'Чат',
    })
  }
},
{
  initialRouteName: 'TaskList',
  tabBarComponent: CustomTabs,
    tabBarOptions: {
      activeTintColor: "#000",
      inactiveTintColor: "#000"
    }
});
