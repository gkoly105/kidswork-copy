
import React, {Component} from 'react';
import {Platform, StyleSheet, View, Button, TextInput,
  TouchableHighlight, TouchableOpacity, ImageBackground,
  KeyboardAvoidingView} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {login, checkAuth} from 'backend/auth';
import { Text, LogoTitleBar, backImage, colors, styles } from 'common';
import { ERRORS, addFcmDevice } from '../../backend/data';

export default class LoginScreen extends Component {

  static navigationOptions = () => ({
    header: null,
  });

  constructor(props){
    super(props);
    this.state = {
      login: '',
      password: '',
      error: '',
    }
  }

  onLogin(){
    login(this.state.login, this.state.password, (res, error) => {
      console.log('login');
      if (!error && res.role == 'parent'){
        console.log('logged in as parent')
        addFcmDevice();
        this.props.navigation.navigate('AppParent');
      }
      else if (!error && res.role == 'child'){
        console.log('logged in as child')
        addFcmDevice();
        this.props.navigation.navigate('AppChild');
      }
      else if (error === ERRORS.CREDS){
        this.setState({error: 'Неверный логин или пароль'})
      }
      else {
        this.setState({error: ''})
      }
    })
  }
  
  render(){
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} menu={false}/>
        <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            flex: 1,
            backgroundColor: 'transparent',
          }}
          contentContainerStyle={{flex: 1}}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
          extraHeight={200}
          // enableOnAndroid={true}
          keyboardShouldPersistTaps="handled"
        >
          <View style={{flex: 1, justifyContent: 'center'}}>
            <View style={{
              padding: 16,
              borderRadius: 3,
              backgroundColor: colors.background,
              margin: 20,
              borderWidth: 2,
              borderColor: colors.dialog.border,
              shadowColor: '#777',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.5,
              shadowRadius: 5,
              elevation: 5,
            }}>
              <TextInput
                style={[styles.textInput, {textAlign:'center'}]}
                placeholder='телефон'
                textContentType='telephoneNumber'
                onChangeText={login => this.setState({login})}
              >
                {this.state.login}
              </TextInput>
              <TextInput
                style={[styles.textInput, {textAlign:'center'}]}
                placeholder='пароль'
                textContentType='password'
                secureTextEntry={true}
                onChangeText={password => this.setState({password})}
              >
                {this.state.password}
              </TextInput>
              {!!this.state.error &&
                <Text style={{color: 'red'}}>
                  {this.state.error}
                </Text>
              }
              <TouchableOpacity
                style={{
                  width: 100,
                  height: 50,
                  borderWidth: 3,
                  borderColor: colors.menuButton,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: 20,
                }}
                onPress={() => this.onLogin()}
              >
                <Text style={{ 
                  padding: 5  
                }}>Войти</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <TouchableHighlight
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 50,
          }}
          underlayColor='red'
          onPress={() => this.props.navigation.navigate('ResetPassword')}
        >
          <Text style={{color: '#aaa',
            fontSize: 18, 
            textDecorationLine: 'underline'
          }}>
            Забыли пароль?
          </Text>
        </TouchableHighlight>
      </ImageBackground>
    )
  }
}