import React, {Component} from 'react';
import {
    ActivityIndicator,
    Image,
    View,
    ImageBackground,
  } from 'react-native';
import {networkAlert} from 'common';
import {checkAuth} from 'backend/auth';
import {fetchHostAddr} from 'backend/data';
import { ERRORS } from '../../backend/data';
import { Text, backImage, colors } from 'common';
import { checkApiVersion } from '../../backend/data';
import { UpdateDialog } from '../../common/dialogs';

export default class LoadingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updateDlgVisible: false,
    }
    this.load();
  }

  load() {
    // setTimeout(() => this.props.navigation.navigate('Auth'), 1000);
    fetchHostAddr((res, error) => {
      if (res === true){
        this.checkVersion()
      }
      else {
        this.load()
      }
    })
  };

  checkVersion() {
    checkApiVersion((res, error) => {
      if (!error) {
        this.handleAuth()
      } else if (error === ERRORS.NEEDUPDATE) {
        this.setState({updateDlgVisible: true})
      } else {
        this.checkVersion()
      }
    });
  }

  handleAuth() {
    checkAuth((res, error) => {
      // console.log('check auth res', res);
      // console.log('error', error)
      if (!error && res.role === 'parent'){
        this.props.navigation.navigate('AppParent')
      }
      else if (!error && res.role === 'child'){
        this.props.navigation.navigate('AppChild')
      }
      else if (error == ERRORS.AUTH){
        this.props.navigation.navigate('Auth')
      }
      else {
        this.handleAuth()
      }
    })
  }


  render(){
    return (
      // <View style={{flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ImageBackground source={backImage}
        style={{width: '100%', height: '100%', backgroundColor: colors.background}}
        imageStyle={{resizeMode: 'cover'}}
      >
        <View style={{flex: 1, alignItems: "center", justifyContent: 'center',
          paddingBottom: 150,  
        }}>
          {/* <Text style={styles.titleText}>
            Добро пожаловать в KidsWork!
          </Text> */}
          {/* <View style={{flex: 1, justifyContent: 'center'}}> */}
            <Image source={require('res/icons/Logo_button.png')}
              style={{width: 250, height: 250, marginBottom: 80}}
            />
          {/* </View> */}
          {/* <View style={{flex: 1, justifyContent: 'center'}}> */}
            <ActivityIndicator/>
          {/* </View> */}
        </View>
        <UpdateDialog visible={this.state.updateDlgVisible} />
      </ImageBackground>
    )
  }
}
