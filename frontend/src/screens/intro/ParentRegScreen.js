
import React, {Component, useState} from 'react';
import {Platform, View, Button, TextInput, TouchableHighlight,
        TouchableOpacity, CheckBox, Switch, Image, Linking } from 'react-native';
import {NavigationEvents} from "react-navigation";
import { SegmentedControls } from 'react-native-radio-buttons'
import _ from 'lodash';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import IconMat from 'react-native-vector-icons/MaterialIcons';
import {parentReg, login as loginUser} from 'backend/auth';
import {getProfile, editProfile} from 'backend/data';
import {styles, colors, Text} from 'common';
import {PhotoDialog} from 'common/dialogs';
import { imageUrl, LINKS, addFcmDevice } from '../../backend/data';
import { ConfirmRegDialog, ParentalGateDialog } from '../../common/dialogs';
import { sendRegSMS } from '../../backend/auth';
import { dateInputAssist, RoundImage } from '../../common';


export default class LoginScreen extends Component {
  constructor(props){
    super(props);

    let role = 'parent';
    if (this.props.navigation.getParam('role') === 'child'){
      role = 'child'
    }

    this.initialState = {
      role: role,
      name: '',
      date: '',
      phone: '',
      gender: undefined,
      // username: '',
      password: '',
      password2: '',
      email: '',
      checkRules: false,
      checkAgree: false,
      errors: {},

      photoDlgVisible: false,
      photo: null,

      confirmDlgVisible: false,

      PGDlgVisible: role == 'child',
    }

    this.state = this.initialState;
  }

  refrechData() {
    this.setState(this.initialState);
    if (this.props.edit) {
      console.log('fetch profile')
      getProfile((res, error) => {
        if (!error && res){
          state = _.pick(res.user, 'name', 'date', 'phone', 'gender', 'role', 'email');
          state.prevPhoto = res.user.image;
          state.role = res.role;
          this.setState(state)
        }
      })
    }
  }

  componentDidMount() {
    this.refrechData();
  }

  onSubmit() {
    if (this.props.edit) {
      this.onSubmitEdit();
    } else {
      this.onSubmitReg();
    }
  }

  onLogin(){
    sendRegSMS(this.state.phone, this.state.password, (res, error) => {
      this.setState({confirmDlgVisible: true});
    })
  }

  onConfirm() {
    loginUser(this.state.phone, this.state.password, (res, error) => {
      console.log('login');
      if (!error && res.role == 'parent'){
        console.log('logged in as parent')
        addFcmDevice();
        this.props.navigation.navigate('AppParent');
      }
      else if (!error && res.role == 'child'){
        console.log('logged in as child')
        addFcmDevice();
        this.props.navigation.navigate('AppChild');
      }
    })
  }

  onSubmitReg(){
    let data = _.pick(this.state, 'name', 'date',
                      'phone', 'gender', 'role', 'password',
                      'password2', 'email', 'checkRules', 'checkAgree');
    let errors = {}

    if (!data.name){
      errors.name = true;
    }
    // if (!data.date){
    //   errors.date = true;
    // }
    if (!data.phone){
      errors.phone = true;
    }
    // if (!data.gender) {
    //   errors.gender = true;
    // }
    if (!data.role) {
      errors.role = true;
    }
    // if (!data.username){
    //   errors.username = true;
    // }
    if (!data.password){
      errors.password = true;
    }
    if (!data.password2 || data.password2 !== data.password)
    {
      errors.password2 = true;
    }
    if (!data.checkRules){
      errors.checkRules = true;
    }
    if (!data.checkAgree){
      errors.checkAgree = true;
    }

    this.setState({errors: errors})

    let photoUri = this.state.photo && this.state.photo.uri;

    if (!this.hasErrors(errors)){
      console.log('no errors')
      parentReg(data, photoUri, (res, error) => {
        if (!error){
          this.onLogin();
        }
        else if (res && res.field_errors){
          console.log('field errors', res.field_errors)
          this.setState({errors: res.field_errors})
        }
      })
    }
  }

  onSubmitEdit() {
    let data = _.pick(this.state, 'name', 'date',
                      'gender', 'password',
                      'password2', 'email', 'checkRules', 'checkAgree');
    let errors = {}

    if (!data.name){
      errors.name = true;
    }
    // if (!data.date){
    //   errors.date = true;
    // }
    // if (!data.phone){
    //   errors.phone = true;
    // }
    // if (!data.gender) {
    //   errors.gender = true;
    // }
    // if (!data.username){
    //   errors.username = true;
    // }
    if (!data.password && data.password2){
      errors.password = true;
    }
    if (data.password2 !== data.password)
    {
      errors.password2 = true;
    }

    this.setState({errors: errors})

    let photoUri = this.state.photo && this.state.photo.uri;

    if (!this.hasErrors(errors)){
      console.log('no errors edit')
      editProfile(data, photoUri, (res, error) => {
        if (!error){
          this.props.navigation.navigate('Home')
        }
        else if (res && res.field_errors){
          console.log('field errors', res.field_errors)
          this.setState({errors: res.field_errors})
        }
      })
    }
  }

  hasErrors(errors) {
    for (let e in errors){
      if (errors[e]){
        return true;
      }
    }
    return false;
  }

  markError(name){
    if (this.state.errors[name]){
      return {borderColor: 'red', borderWidth: 2}
    }
    return {}
  }

  onPickPhoto() {
    console.log('open photo dialog');
    this.setState({photoDlgVisible: true});
  }

  onChangeDate(date) {
    date = dateInputAssist(this.state.date, date)
    this.setState({date: date});
  }

  onGenderSelection(opt, index) {
    if (index === 0){
      this.setState({gender: 'M'})
    }
    else if (index === 1) {
      this.setState({gender: 'F'})
    }
    else {
      this.setState({gender: undefined})
    }
  }

  onRoleSelection(opt, index) {
    if (index === 0){
      this.setState({role: 'parent'})
    }
    else if (index === 1) {
      this.setState({role: 'child', PGDlgVisible: true})
    }
    else {
      this.setState({role: 'parent'})
    }
  }

  getGenderIndex() {
    if (this.state.gender === 'M'){
      return 0
    } else if (this.state.gender === 'F') {
      return 1
    }
    else{
      return undefined
    }
  }

  getRoleIndex() {
    if (this.state.role === 'parent'){
      return 0
    } else if (this.state.role === 'child') {
      return 1
    }
    else{
      return 0
    }
  }

  inactiveStyle(inactive) {
    return inactive ? {
      opacity: 0.5,
    } : null;
  }

  render(){
    return (
      <KeyboardAwareScrollView
        style={{ backgroundColor: colors.background,
          paddingLeft: 20, 
          paddingRight: 20,
        }}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={true}
        keyboardShouldPersistTaps='handled'
      >
        <View style={{
          paddingBottom: 20,
          paddingTop: 20,
        }}>
          { !this.props.edit &&
            <View
              style={[this.markError('role'), {padding: 3}]}
            >
              <SegmentedControls
                options={['Взрослый', 'Ребенок']}
                onSelection={this.onRoleSelection.bind(this)}
                selectedIndex={this.getRoleIndex()}
              />
            </View>
          }
          <Text style={styles.fieldTextReg}>
            Имя
          </Text>
          <TextInput
            style={[styles.textInputReg, this.markError('name')]}
            textContentType='name'
            onChangeText={name => this.setState({name})}
            value={this.state.name}
          />
          <Text style={styles.fieldTextReg}>
            Дата рождения <Text style={{fontSize: 10, color: '#777'}}>(не обязательно)</Text>
          </Text>
          <TextInput 
            style={[styles.textInputReg, {width: '60%'}, this.markError('date')]}
            placeholder='дд.мм.гггг'
            onChangeText={date => this.onChangeDate(date)}
            value={this.state.date}
          />
          <Text style={styles.fieldTextReg}>
            Пол <Text style={{fontSize: 10, color: '#777'}}>(не обязательно)</Text>
          </Text>
          <View
            style={[this.markError('gender'), {padding: 3}]}
          >
            <SegmentedControls
              options={['Мужской', 'Женский']}
              onSelection={this.onGenderSelection.bind(this)}
              selectedIndex={this.getGenderIndex()}
            />
          </View>
          <Text style={styles.fieldTextReg}>
            Моб. телефон
          </Text>
          <TextInput 
            editable={!this.props.edit}
            style={[styles.textInputReg, {width: '60%'}, this.markError('phone'),
              this.inactiveStyle(this.props.edit)
            ]}
            textContentType='telephoneNumber'
            placeholder='+7 000 0000000'
            onChangeText={phone => this.setState({phone})}
            value={this.state.phone}
          />
          {/* <Text style={styles.fieldTextReg}>
            Логин
          </Text>
          <TextInput 
            style={[styles.textInputReg, {width: '60%'}, this.markError('username')]}
            textContentType='username'
            onChangeText={username => this.setState({username})}
            value={this.state.username}
          /> */}
          <Text style={styles.fieldTextReg}>
            { this.props.edit ? 
              'Новый пароль'
              :
              'Пароль'
            }
          </Text>
          <TextInput
            style={[styles.textInputReg, {width: '60%'}, this.markError('password')]}
            textContentType='newPassword'
            secureTextEntry={true}
            onChangeText={password => this.setState({password})}
            value={this.state.password}
          />
          <Text style={styles.fieldTextReg}>
            Повторить пароль
          </Text>
          <TextInput
            style={[styles.textInputReg, {width: '60%'}, this.markError('password2')]}
            textContentType='newPassword'
            secureTextEntry={true}
            onChangeText={password2 => this.setState({password2})}
            value={this.state.password2}
          />
          <Text style={styles.fieldTextReg}>
            e-mail <Text style={{fontSize: 10, color: '#777'}}>(не обязательно)</Text>
          </Text>
          <TextInput 
            style={[styles.textInputReg, this.markError('email')]}
            textContentType='emailAddress'
            onChangeText={email => this.setState({email})}
            value={this.state.email}
          />
          <View style={{flexDirection: 'row', alignItems: 'center',
            padding: 2,
          }}>
            <Text style={[styles.fieldTextReg, {}]}>
              Добавить фото <Text style={{fontSize: 10, color: '#777'}}>(не обязательно)</Text>
            </Text>
            <TouchableOpacity onPress={() => this.onPickPhoto()} style={{marginLeft: 'auto'}}>
              { !this.state.photo && !this.state.prevPhoto &&
                <Image source={require('res/icons/Photo.png')}
                  style={{width: 70, height: 70}}
                />
              }
              { this.state.photo && 
                <RoundImage source={{uri: this.state.photo.uri}}
                  style={{width: 70, height: 70, borderRadius: 35}}
                />
              }
              { !this.state.photo && this.state.prevPhoto &&
                <RoundImage source={{uri: imageUrl(this.state.prevPhoto)}}
                  style={{width: 70, height: 70, borderRadius: 35}}
                />
              }
            </TouchableOpacity>
          </View>
          { !this.props.edit && (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View style={[this.markError('checkRules')]}>
                { Platform.OS === 'android' && 
                  <CheckBox
                    value={this.state.checkRules}
                    onValueChange={(checkRules) => this.setState({checkRules})}
                  />
                }
                { Platform.OS === 'ios' && 
                  <Switch
                    style={{transform: [{ scaleX: .8 }, { scaleY: .8 }] }}
                    value={this.state.checkRules}
                    onValueChange={(checkRules) => this.setState({checkRules})}
                  />
                }
              </View>
              <Text style={{fontSize: 11, flex: 1, flexWrap: 'wrap', marginLeft: 10}}>
                {'Я прочитал и согласен с '}
                <Text style={{color: colors.hyperlink}}
                  onPress={() => Linking.openURL(LINKS.RULES)}
                >
                  правилами пользования приложением KidsWork
                </Text>
              </Text>
            </View>
          )}
          { !this.props.edit && (
            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 4}}>
              <View style={[this.markError('checkAgree')]}>
                { Platform.OS === 'android' && 
                  <CheckBox
                    value={this.state.checkAgree}
                    onValueChange={(checkAgree) => this.setState({checkAgree})}
                  />
                }
                { Platform.OS === 'ios' && 
                  <Switch
                    style={{transform: [{ scaleX: .8 }, { scaleY: .8 }] }}
                    value={this.state.checkAgree}
                    onValueChange={(checkAgree) => this.setState({checkAgree})}
                  />
                }
              </View>
              <Text style={{fontSize: 11, flex: 1, flexWrap: 'wrap', marginLeft: 10}}>
                {'Я даю свое согласие на '}
                <Text style={{color: colors.hyperlink}}
                  onPress={() => Linking.openURL(LINKS.AGREEMENT)}
                >
                  обработку персональных данных
                </Text>
              </Text>
            </View>
          )}
          <TouchableOpacity
            style={{
              borderWidth: 3,
              borderColor: colors.menuButton,
              borderRadius: 25,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              marginTop: 20,
            }}
            onPress={() => this.onSubmit()}
          >
            <Text style={{ 
              padding: 10,
              fontSize: 12,
            }}>
              {this.props.edit ? 
                'Сохранить'
                :
                'Зарегистрироваться'
              }
            </Text>
          </TouchableOpacity>
          <PhotoDialog visible={this.state.photoDlgVisible}
            setVisible={(v) => this.setState({photoDlgVisible: v})}
            setPhoto={(p) => this.setState({photo: p})}
          />
          <NavigationEvents onDidFocus={() => this.refrechData()} />
          <ConfirmRegDialog login={this.state.phone}
            password={this.state.password}
            visible={this.state.confirmDlgVisible}
            setVisible={(v) => this.setState({confirmDlgVisible: v})}
            onDone={this.onConfirm.bind(this)}
          />
          { Platform.OS === 'ios' &&
            <ParentalGateDialog
              visible={this.state.PGDlgVisible}
              setVisible={(PGDlgVisible) => this.setState({PGDlgVisible})}
              onReject={() => this.props.navigation.goBack()}
            />
          }
        </View>
      </KeyboardAwareScrollView>
    )
  }
}
