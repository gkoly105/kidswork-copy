
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import IntroScreen from './IntroScreen';
import ParentLoginScreen from './ParentLoginScreen';
import ParentRegScreen from './ParentRegScreen';
import ResetPasswordScreen from './ResetPassword';

export default createStackNavigator(
  {
    Intro: {
      screen: IntroScreen,
      navigationOptions: {
        header: null,
      }
    },
    ParentLogin: {
      screen: ParentLoginScreen,
      navigationOptions: {
        header: null,
      }
    },
    ParentReg: {
      screen: ParentRegScreen,
      navigationOptions: {
        header: null,
      }
    },
    ResetPassword: {
      screen: ResetPasswordScreen,
      navigationOptions: {
        header: null,
      }
    }
  },
  {
    initialRouteName: 'Intro'
  }
)
