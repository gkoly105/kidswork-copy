
import React, {Component} from 'react';
import {Platform, StyleSheet, View, Button, TextInput,
  TouchableHighlight, TouchableOpacity, ImageBackground,
  KeyboardAvoidingView} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {login, checkAuth} from 'backend/auth';
import { Text, LogoTitleBar, backImage, colors, styles } from 'common';
import { ERRORS } from '../../backend/data';
import { sendPasswordSMS, resetPassword } from '../../backend/auth';

export default class ResetPasswordScreen extends Component {

  static navigationOptions = () => ({
    header: null,
  });

  constructor(props){
    super(props);
    this.state = {
      login: '',
      password: '',
      token: '',
      phone: '',
      error: '',
      tokenPassed: false,
      // wrongToken: false,
    }
  }

  onSend(){
    let phone = this.state.phone.trim()
    if (phone.length > 0){
      sendPasswordSMS(this.state.phone, (res, error) => {
        if (error === ERRORS.NOTFOUND){
          this.setState({error:  'Аккаунт не найден', tokenPassed: false})
        }
        else {
          this.setState({error:  ''})
        }
        if (!error){
          this.setState({tokenPassed: true})
        }
      })
    }
    
  }

  onSubmit(){
    if (!this.state.password || !this.state.password2 || !this.state.token){
      this.setState({error: 'не все поля заполнены'})
      return;
    }
    if (this.state.password !== this.state.password2) {
      this.setState({error: 'Пароли не совпадают'})
      return;
    }
    resetPassword(this.state.phone.trim(), this.state.password,
      this.state.token, (res, error) => {
        if (!error){
          this.props.navigation.navigate('ParentLogin')
        }
        else if (error === ERRORS.NOTFOUND){
          this.setState({
            error: 'Код введен неверно',
            tokenPassed: false,
            token: '',
          })
        }
    })
  }
  
  render(){
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} menu={false}/>
        <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            flex: 1,
            backgroundColor: 'transparent',
          }}
          contentContainerStyle={{flex: 1}}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
          extraHeight={200}
          // enableOnAndroid={true}
          keyboardShouldPersistTaps="handled"
        >
          <View style={{flex: 1, justifyContent: 'center'}}>
            <View style={{
              padding: 16,
              borderRadius: 3,
              backgroundColor: colors.background,
              margin: 20,
              borderWidth: 2,
              borderColor: colors.dialog.border,
              shadowColor: '#777',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.5,
              shadowRadius: 5,
              elevation: 5,
            }}>
              { !this.state.tokenPassed &&
                <TextInput
                  style={[styles.textInput, {textAlign:'center'}]}
                  placeholder='Номер телефона'
                  onChangeText={phone => this.setState({phone})}
                >
                  {this.state.phone}
                </TextInput>
              }
              {this.state.tokenPassed &&
                <View>
                  <TextInput
                    style={[styles.textInput, {textAlign:'center'}]}
                    placeholder='пароль'
                    textContentType='password'
                    secureTextEntry={true}
                    onChangeText={password => this.setState({password})}
                  >
                    {this.state.password}
                  </TextInput>
                  <TextInput
                    style={[styles.textInput, {textAlign:'center'}]}
                    placeholder='повторите пароль'
                    textContentType='password'
                    secureTextEntry={true}
                    onChangeText={password2 => this.setState({password2})}
                  >
                    {this.state.password2}
                  </TextInput>
                  <TextInput
                    style={[styles.textInput, {textAlign:'center'}]}
                    placeholder='код из СМС'
                    onChangeText={token => this.setState({token})}
                  >
                    {this.state.token}
                  </TextInput>
                  
                </View>
              }
              {!!this.state.error &&
                <Text style={{color: 'red'}}>
                  {this.state.error}
                </Text>
              }
              {this.state.tokenPassed &&
                <TouchableOpacity
                  style={{
                    width: '100%',
                    height: 50,
                    borderWidth: 3,
                    borderColor: colors.menuButton,
                    borderRadius: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                    marginTop: 20,
                  }}
                  onPress={() => this.onSubmit()}
                >
                  <Text style={{
                    padding: 5  
                  }}>Сохранить</Text>
                </TouchableOpacity>
              }
              <TouchableOpacity
                style={{
                  width: '100%',
                  height: 50,
                  borderWidth: 3,
                  borderColor: colors.menuButton,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: 20,
                }}
                onPress={() => this.onSend()}
              >
                <Text style={{
                  padding: 5  
                }}>Получить СМС код</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    )
  }
}