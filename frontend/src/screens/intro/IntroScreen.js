import React, {Component} from 'react';
import {StyleSheet, View, Button, Image,
  TouchableHighlight, ImageBackground} from 'react-native';
import { Text, backImage, colors } from 'common';

export default class IntroScreen extends Component {
  static navigationOptions = {
    header: null
  }

  render(){
    return (
      <ImageBackground source={backImage}
        style={{width: '100%', height: '100%', backgroundColor: colors.background}}
        imageStyle={{resizeMode: 'cover'}}
      >
        <View style={{flex: 1, alignItems: "center"}}>
          {/* <Text style={styles.titleText}>
            Добро пожаловать в KidsWork!
          </Text> */}
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Image source={require('res/icons/Logo_button.png')}
              style={{width: 250, height: 250, marginTop: 30}}
            />
          </View>
          <View style={{flex: 1, width: '100%'}}>
            <TouchableHighlight
              style={[styles.buttons, {backgroundColor: 'white'}]}
              underlayColor='white'
              onPress={() => this.props.navigation.navigate('ParentReg', {role: 'parent'})}
            >
              <Text style={[styles.btnText]}>Взрослый</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={[styles.buttons, {backgroundColor: 'white'}]}
              underlayColor='white'
              onPress={() => this.props.navigation.navigate('ParentReg', {role: 'child'})}
            >
              <Text style={[styles.btnText]}>Ребенок</Text>
            </TouchableHighlight>
          </View>
          <View style={{width: '100%'}}>
            <TouchableHighlight
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 50,
              }}
              underlayColor='red'
              onPress={() => this.props.navigation.navigate('ParentLogin')}
            >
              <Text style={{color: '#aaa',
                fontSize: 18, 
                textDecorationLine: 'underline'
              }}>
                Уже зарегистрированы?
              </Text>
            </TouchableHighlight>
          </View>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  titleText: {
    margin: 20,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  buttons: {
    margin: 22,
    marginLeft: 26,
    marginRight: 26,
    height: 50,
    borderWidth: 3,
    borderColor: colors.menuButton,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  }
})
