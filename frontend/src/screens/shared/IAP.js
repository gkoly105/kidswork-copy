import React from 'react';
import { Platform } from 'react-native';
import * as RNIap from 'react-native-iap';
import moment from 'moment';
import momentRu from 'moment/locale/ru';
import { setSub } from '../../backend/data';
moment.locale('ru');

const selectOS = (android, ios) => Platform.select({android, ios})

export const Items = [
  {
    id: 'all',
    name: 'Премиум навсегда',
    sku: selectOS('pr_full', 'premium_full'),
    isProduct: true,
  },
  {
    id: '1y',
    name: 'Премиум на год',
    period: ' / год',
    sku: selectOS('sub_full_1y', 'sub_full_1y')
  },
  {
    id: '6m',
    name: 'Премиум на полгода',
    period: ' / 6 мес',
    sku: selectOS('sub_full_6m', 'sub_full_6m')
  },
  {
    id: '1m',
    name: 'Премиум на месяц',
    period: ' / мес',
    sku: selectOS('sub_full_1m', 'sub_full_1m'),
  },
  {
    id: 'tsk',
    name: 'Расширенные задания',
    period: ' / мес',
    sku: selectOS('sub_tasks', 'sub_tasks')
  },
  {
    id: 'fam',
    name: 'Раздел семьи',
    period: ' / мес',
    sku: selectOS('sub_family', 'sub_family')
  },
  {
    id: 'hst',
    name: 'История заданий',
    period: ' / мес',
    sku: selectOS('sub_history', 'sub_history')
  },
]

const mainSub = Items[3];

export const ItemSkus = Items.map(item => item.sku)

// Global store for subs and prods
let SubsAndProds = [[], []];

export const getSubsAndProds = () => {
  return SubsAndProds;
}

export class InitIAP extends React.Component {
  constructor(props) {
    super(props);

    // this.state = {
    //   fetched: false,
    // }
  }

  fetchData(){
    let prods = RNIap.getProducts(ItemSkus)
    console.log('sku list', ItemSkus);

    let subs = RNIap.getSubscriptions(ItemSkus)
    
    // Promise.all([subs, prods]).then(res => {
    //   console.log('fetch subs res', res)

    //   // Store subs and prods globally
    //   SubsAndProds = res

    //   // this.setState({fetched: true})
    //   this.checkPurchases();

    // }).catch(error => {
    //   console.log('fetch subs error', error)

    //   setTimeout(() => this.fetchData(), 1000)
    // })




    subs.then(subs => {
      prods.then(prods => {
        SubsAndProds = [subs, prods]

        this.checkPurchases();
      })
      .catch(error => {
        console.log('fetch prods error', error)
  
        this._timeout = setTimeout(() => this.fetchData(), 1000)
      })
    })
    .catch(error => {
      console.log('fetch subs error', error)

      this._timeout = setTimeout(() => this.fetchData(), 1000)
    })
  }

  checkPurchases(){
    RNIap.getAvailablePurchases().then(res => {
      console.log('avail', res)

      res.forEach(prod => {
        let started = moment(Number(prod.transactionDate)).toISOString()
        
        // send subs
        let item = Items.find(item => item.sku === prod.productId)
        if (item) {
          let type = item.id

          setSub(type, started, (res, error) => {

          })
        }
      })

      this._timeout = setTimeout(() => this.checkPurchases(), 3000)

    }).catch(error => {
      console.log(error);
      this._timeout = setTimeout(() => this.checkPurchases(), 1000)
    })

    // RNIap.getPurchaseHistory().then(res => {
    //   console.log('hist', res)
    // })
  }

  componentDidMount() {
    this.purchaseUpdateSubscription = RNIap.purchaseUpdatedListener((purchase) => {
      console.log('purchaseUpdatedListener', purchase);
      const receipt = purchase.transactionReceipt;
      if (receipt) {
        if (Platform.OS === 'ios') {
          RNIap.finishTransactionIOS(purchase.transactionId);
        } else if (Platform.OS === 'android') {
          // If consumable (can be purchased again)
          // RNIap.consumePurchaseAndroid(purchase.purchaseToken);
          // If not consumable
          console.log('acknowledge')
          RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
        }
        // this.checkPurchases();
      }
    });
    
    this.fetchData()
    // setTimeout(() => this.fetchData(), 2000)


    // setTimeout(() => {
    //   console.log('buy sub')
    //   // RNIap.requestSubscription('sub_full_1m').catch(error => console.warn(error))
    //   // RNIap.consumeAllItemsAndroid()
    // }, 5000)
  }

  componentWillUnmount() {
    clearTimeout(this._timeout)

    if (this.purchaseUpdateSubscription) {
      this.purchaseUpdateSubscription.remove();
      this.purchaseUpdateSubscription = null;
    }
  }

  render() {
    return null;
  }
}

export const getMainSubPrice = () => {
  // return RNIap.getSubscriptions([mainSub.sku])
  //             .then(res => {
  //               console.log('main price res', res);
  //               return res[0] && res[0].localizedPrice
  //             })
  //             .catch(error => console.log('main price error', error))

  let [subs, prods] = getSubsAndProds()
  if (subs.length > 0) {
    let sub = subs.find(sub => sub.productId === mainSub.sku)
    return sub && sub.localizedPrice || ''
  }
  else {
    return ''
  }
}

export const buyMainSub = () => {
  RNIap.requestSubscription(mainSub.sku);
}
