import React, {Component} from 'react';
import { View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { deleteAccount, contactSupport } from '../../backend/data';
import { logout } from '../../backend/auth';
import { Text, colors, styles, LogoTitleBar, backImage } from '../../common';
import { TextAlertDialog } from '../../common/dialogs';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';

export default class SupportScreen extends Component {
  constructor(props) {
    super(props)

    this.state = this.initialState()
  }

  initialState() {
    return {
      wait: false,
      done: false,
      text: '',
      errors: {},
    }
  }

  onSubmit(){
    let errors = {}

    let text = this.state.text.trim()
    
    if (!text){
      errors.text = true;
    }

    this.setState({errors})

    if (JSON.stringify(errors) === '{}') {
      this.setState({wait: true, done: false})
      contactSupport(text, (res, error) => {
        if (!error) {
          this.setState({done: true, wait: false,
            resultText: 'Ваше сообщение отправлено'
          })
        } else {
          this.setState({done: true, wait: false,
            resultText: 'Не удалось отправить сообщение'
          })
        }
      })
    }
  }

  render() {
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            paddingLeft: 20, 
            paddingRight: 20,
          }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
        >
          <View style={{padding: 15}}>
            <Text style={{fontSize: 18, textAlign: 'center', marginBottom: 15}}>
              Обратная связь
            </Text>
            { !this.state.wait && !this.state.done &&
              <View>
                <TextInput
                  style={[styles.textInput, {
                    height: 100, textAlignVertical: 'top',
                  }]}
                  multiline={true}
                  onChangeText={text => this.setState({text})}
                  value={this.state.text}
                />
                <TouchableOpacity
                  style={[styles.dlgButton, {
                    marginTop: 10,
                  }]}
                  onPress={() => this.onSubmit()}
                >
                  <Text style={{ 
                    padding: 3,
                    color: 'black',
                  }}>
                    Отправить
                  </Text>
                </TouchableOpacity>
              </View>
            }
            { this.state.wait &&
              <View>
                <Text style={{textAlign: 'center'}}>
                  Отправка...
                </Text>
              </View>
            }
            { this.state.done &&
              <View>
                <Text style={{textAlign: 'center'}}>
                  {this.state.resultText}
                </Text>
              </View>
            }
          </View>
        </KeyboardAwareScrollView>
        <NavigationEvents onWillFocus={() => this.setState(this.initialState())} />
      </ImageBackground>
    )
  }
}
