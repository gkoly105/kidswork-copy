import React, {Component} from 'react';
import { View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { deleteAccount } from '../../backend/data';
import { logout } from '../../backend/auth';
import { Text, colors, styles, LogoTitleBar, backImage } from '../../common';
import { TextAlertDialog } from '../../common/dialogs';

export default class SettingsScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      deleteDlgVisible: false,
      role: props.navigation.getParam('role') || 'child',
    }
  }

  onDeleteAccount() {
    deleteAccount((res, error) => {
      if (!error) {
        this.props.navigation.navigate('Auth')
      }
    })
  }

  render() {
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        <View style={{padding: 15}}>
          <Text style={{fontSize: 18, textAlign: 'center', marginBottom: 15}}>
            Настройки
          </Text>
          <TouchableOpacity
            style={[styles.settings.button,
            {}]}
            onPress={() => this.props.navigation.navigate('EditProfile')}
          >
            <Text style={{
              fontSize: 16,
              textAlign: 'center',
            }}>
              Профиль
            </Text>
          </TouchableOpacity>
          { this.state.role === 'parent' &&
            <TouchableOpacity
              style={[styles.settings.button,
              {}]}
              onPress={() => this.props.navigation.navigate('Subs')}
            >
              <Text style={{
                fontSize: 16,
                textAlign: 'center',
              }}>
                Подписки
              </Text>
            </TouchableOpacity>
          }
          <TouchableOpacity
            style={[styles.settings.button,
            {
              backgroundColor: '#fcb330',
            }]}
            onPress={() => this.setState({deleteDlgVisible: true})}
          >
            <Text style={{
              fontSize: 16, 
              color: 'white',
              textAlign: 'center',
            }}>
              Удалить аккаунт
            </Text>
          </TouchableOpacity>
        </View>
        <TextAlertDialog visible={this.state.deleteDlgVisible}
          setVisible={(v) => this.setState({deleteDlgVisible: v})}
          onConfirm={() => this.onDeleteAccount()}
          text={'Вы точно хотите полность удалить свой аккаунт? Это действие нельзя будет оменить!'}
        />
      </ImageBackground>
    )
  }
}
