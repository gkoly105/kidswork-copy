import React, {Component} from 'react';
import { View, ImageBackground, Image, TouchableOpacity,
        Animated, PanResponder } from 'react-native';
import { deleteAccount, getNotifList, NOTIFS, prolongConfirm, prolongReject, removeNotif, setNotifRead } from '../../backend/data';
import {  } from '../../backend/auth';
import { Text, colors, styles, LogoTitleBar, backImage, getAvatar, timeDiff } from '../../common';
import { FlatList } from 'react-native-gesture-handler';

import moment from 'moment';
import momentRu from 'moment/locale/ru';
import { TextAlertDialog, AcceptLinkDialog } from '../../common/dialogs';
import { NavigationEvents } from 'react-navigation';
moment.locale('ru');

class SwipeableRow extends Component {
  constructor(props) {
    super(props)

    this.gestureDelay = 20;
    const actionOffset = 120;
    const finishOffset = 500;
    // this.scrollViewEnabled = true;


    const position = new Animated.ValueXY();
    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => false,
      onMoveShouldSetPanResponder: (evt, gestureState) => {
        // console.log('dx dy', gestureState.dx, gestureState.dy)
        if (gestureState.dx > this.gestureDelay) {
          return true;
        }
        return false;
      },
      onPanResponderTerminationRequest: (evt, gestureState) => {
        // console.log('request release')
        return true;
      },
      onPanResponderMove: (evt, gestureState) => {
        if (gestureState.dx > this.gestureDelay) {
          // this.setScrollViewEnabled(false);
          let newX = gestureState.dx - this.gestureDelay;
          position.setValue({x: newX, y: 0});
        }
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (gestureState.dx < actionOffset) {
          Animated.timing(this.state.position, {
            toValue: {x: 0, y: 0},
            duration: 150,
          }).start(() => {
            // this.setScrollViewEnabled(true);
          });
        } else {
          Animated.timing(this.state.position, {
            toValue: {x: finishOffset, y: 0},
            duration: 300,
          }).start(() => {
            // this.setScrollViewEnabled(true);
            this.props.onFinish();
          })
        }
      },
    });

    this.panResponder = panResponder;
    this.state = {position};
  }

  // setScrollViewEnabled(enabled) {
  //   if (this.scrollViewEnabled !== enabled) {
  //     console.log('scroll enabled', enabled);
  //     this.props.setScrollViewEnabled(enabled);
  //     this.scrollViewEnabled = enabled;
  //   }
  // }

  render() {
    return (
      <View>
        <Animated.View
          style={[this.state.position.getLayout()]} 
          {...this.panResponder.panHandlers}
        >
          {this.props.children}
        </Animated.View>
      </View>
    )
  }
}

class NotifListElem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      alertVisible: false,
      onAlertDone: undefined,
      onAlertReject: undefined,
      alertText: '',
      alertSingleButton: false,
      acceptLinkVisible: false,
    }
  }

  styles = {
    text: {
      
    },
    bold: {
      fontWeight: 'bold'
    }
  }

  onItemPress() {
    let notif = this.props.notif;
    let user = notif.from_user;

    if ([NOTIFS.C_ADD_TASK, NOTIFS.C_EDIT_TASK, NOTIFS.C_REMIND_TASK,
        NOTIFS.C_DELETE_TASK, NOTIFS.C_CONFIRM_TASK,
        NOTIFS.C_PROLONG_CONFIRM, NOTIFS.C_PROLONG_REJECT].includes(notif.type)) {
      this.props.navigation.navigate('ParentProfile', {parent_id: user.id});
      let task_id = notif.data.task_id;
      this.props.navigation.navigate('TaskList', {task_id: task_id});
    }
    if ([NOTIFS.C_ADD_GIFT, NOTIFS.C_EDIT_GIFT, NOTIFS.C_DELETE_GIFT,
        NOTIFS.C_CONFIRM_GIFT].includes(notif.type)) {
      this.props.navigation.navigate('ParentProfile', {parent_id: user.id});
      let gift_id = notif.data.gift_id;
      this.props.navigation.navigate('GiftList', {gift_id: gift_id});
    }
    if (notif.type === NOTIFS.C_PENALTY && notif.data.task_id) {
      this.props.navigation.navigate('ParentProfile', {parent_id: user.id});
      let task_id = notif.data.task_id;
      this.props.navigation.navigate('TaskList', {task_id: task_id});
    }

    if ([NOTIFS.P_DO_TASK, NOTIFS.P_REMOVE_TASK].includes(notif.type)) {
      this.props.navigation.navigate('ChildProfile', {child_id: user.id});
      let task_id = notif.data.task_id;
      this.props.navigation.navigate('TaskList', {task_id: task_id});
    }
    if ([NOTIFS.P_BUY_GIFT].includes(notif.type)) {
      this.props.navigation.navigate('ChildProfile', {child_id: user.id});
      let gift_id = notif.data.gift_id;
      this.props.navigation.navigate('GiftList', {gift_id: gift_id});
    }
    if ([NOTIFS.P_OFFER_TASK].includes(notif.type)) {
      let confirm_task = notif.data.task;
      this.props.navigation.navigate('AddTask', {confirm_task, notif_id: notif.id});
    }
    if ([NOTIFS.P_OFFER_GIFT].includes(notif.type)) {
      let confirm_gift = notif.data.gift;
      this.props.navigation.navigate('AddGift', {confirm_gift, notif_id: notif.id});
    }

    if (notif.type === NOTIFS.C_CONFIRM_TASK) {
      this.props.navigation.navigate('TaskHistory', {user_id: user.id});
      let task_id = notif.data.task_id;
      this.props.navigation.navigate('DoneTasks', {task_id: task_id});
    }

    if (notif.type === NOTIFS.P_PROLONG_TASK) {
      this.setState({
        alertVisible: true,
        alertText: 'Продлить выполнения задания?',
        onAlertDone: () => this.prolongConf(notif),
        onAlertReject: () => this.prolongRej(notif),
        alertSingleButton: false,
      })
    }
    if ([NOTIFS.MESSAGE].includes(notif.type)) {
      let user = notif.from_user;
      this.props.navigation.navigate('ChatFullScreen', {user});
    }
    if (notif.type === NOTIFS.OFFER_LINK){
      this.setState({acceptLinkVisible: true})
    }
  }

  prolongConf(notif) {
    prolongConfirm(notif.id, (res, error) => {

    })
  }

  prolongRej(notif) {
    prolongReject(notif.id, (res, error) => {

    })
  }

  renderText() {
    let notif = this.props.notif;
    let user = notif.from_user;

    if (notif.type === NOTIFS.C_ADD_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' добавил '
        }
        <Text style={this.styles.bold}>задание</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.C_EDIT_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' изменил '
        }
        <Text style={this.styles.bold}>задание</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.C_REMIND_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' напоминает о '
        }
        <Text style={this.styles.bold}>задании</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.C_DELETE_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' отменил '
        }
        <Text style={this.styles.bold}>задание </Text>
        {'"' + notif.data.task.title + '"'}
      </Text>
    }
    else if (notif.type === NOTIFS.C_CONFIRM_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' подтвердил выполнение '
        }
        <Text style={this.styles.bold}>задания </Text>
        {'"' + notif.data.task.title + '"'}
      </Text>
    }
    else if (notif.type === NOTIFS.C_REJECT_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' отклонил добавление '
        }
        <Text style={this.styles.bold}>задания </Text>
        {'"' + notif.data.task.title + '"'}
      </Text>
    }
    else if (notif.type === NOTIFS.C_REJECT_GIFT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' отклонил добавление '
        }
        <Text style={this.styles.bold}>подарка </Text>
        {'"' + notif.data.gift.title + '"'}
      </Text>
    }

    if (notif.type === NOTIFS.C_ADD_GIFT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' добавил '
        }
        <Text style={this.styles.bold}>подарок</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.C_EDIT_GIFT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' изменил '
        }
        <Text style={this.styles.bold}>подарок</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.C_DELETE_GIFT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' удалил '
        }
        <Text style={this.styles.bold}>подарок </Text>
        {'"' + notif.data.gift.title + '"'}
      </Text>
    }
    else if (notif.type === NOTIFS.C_CONFIRM_GIFT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' подтвердил вручение '
        }
        <Text style={this.styles.bold}>подарка </Text>
        {'"' + notif.data.gift.title + '"'}
      </Text>
    }

    else if (notif.type === NOTIFS.C_AWARD) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) }
        <Text style={this.styles.bold}>{' премировал'}</Text>
        {notif.data.task_id && ' за'}
        {notif.data.task_id && 
          <Text style={this.styles.bold}>{' задание'}</Text>
        }
        {' на '}
        {notif.data.amount + ''}
      </Text>
    }
    else if (notif.type === NOTIFS.C_PENALTY) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) }
        <Text style={this.styles.bold}>{' оштрафовал'}</Text>
        {notif.data.task_id && ' за'}
        {notif.data.task_id && 
          <Text style={this.styles.bold}>{' задание'}</Text>
        }
        {' на '}
        {notif.data.amount + ''}
      </Text>
    }

    else if (notif.type === NOTIFS.P_DO_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' выполнил '
        }
        <Text style={this.styles.bold}>задание</Text>
      </Text>
    }

    else if (notif.type === NOTIFS.P_BUY_GIFT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' купил '
        }
        <Text style={this.styles.bold}>подарок</Text>
      </Text>
    }

    else if (notif.type === NOTIFS.P_OFFER_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' предложил '
        }
        <Text style={this.styles.bold}>задание</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.P_REQUEST_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' просит придумать '
        }
        <Text style={this.styles.bold}>задание</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.P_OFFER_GIFT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' предложил '
        }
        <Text style={this.styles.bold}>подарок</Text>
      </Text>
    }

    else if (notif.type === NOTIFS.P_PROLONG_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' просит продлить выполнение '
        }
        <Text style={this.styles.bold}>задания</Text>
        {' на '}
        { timeDiff(notif.data.old_limit, notif.data.new_limit) }
      </Text>
    }
    else if (notif.type === NOTIFS.C_PROLONG_CONFIRM) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' продлил выполнение '
        }
        <Text style={this.styles.bold}>задания</Text>
        {' на '}
        { timeDiff(notif.data.old_limit, notif.data.new_limit) }
      </Text>
    }
    else if (notif.type === NOTIFS.C_PROLONG_REJECT) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' отклонил продление '
        }
        <Text style={this.styles.bold}>задания</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.P_REMOVE_TASK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' просит отменить '
        }
        <Text style={this.styles.bold}>задание</Text>
        {' по причине "' + notif.data.reason + '"'}
      </Text>
    }

    else if (notif.type === NOTIFS.MESSAGE) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' отправил '
        }
        <Text style={this.styles.bold}>сообщение</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.TASK_EXPIRED) {
      return <Text style={this.styles.text}>
        <Text style={this.styles.bold}>Задание</Text>
        {notif.from_user.role === 'parent' ? ' от ' : ' для '}
        { (user.display_name || user.name) +
        ' просрочено: '
        }
        {'"' + notif.data.task.title + '"'}
      </Text>
    }

    else if (notif.type === NOTIFS.OFFER_LINK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' приглашает вступить в '
        }
        <Text style={this.styles.bold}>семью</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.ACCEPT_LINK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' принял приглашение в '
        }
        <Text style={this.styles.bold}>семью</Text>
      </Text>
    }
    else if (notif.type === NOTIFS.REJECT_LINK) {
      return <Text style={this.styles.text}>
        { (user.display_name || user.name) +
        ' отклонил приглашение в '
        }
        <Text style={this.styles.bold}>семью</Text>
      </Text>
    }
  }

  render() {
    let notif = this.props.notif;
    let user = notif.from_user;

    return (
        <View style={{
          paddingTop: 10, marginLeft: 10,
          marginRight: 10,
        }}>
          <TouchableOpacity
            style={{flexDirection: 'row', alignItems: 'center'}}
            onPress={() => this.onItemPress()}
          >
            <Image
              style={{width: 40, height: 40,
                borderRadius: 20,
                marginRight: 10,
              }}
              source={getAvatar(user.image, user.role, user.gender)}
            />
            <View style={{flex: 1}}>
              {this.renderText()}
            </View>
            { !notif.read &&
              <View style={{
                width: 8, height: 8, borderRadius: 4,
                backgroundColor: colors.topBar,
                marginRight: 10,
                marginLeft: 10,
              }}></View>
            }
          </TouchableOpacity>
          <View style={{width: '100%', height: 2,
            backgroundColor: colors.separator,
            marginTop: 10,
          }}></View>
          <TextAlertDialog visible={this.state.alertVisible}
            setVisible={(v) => this.setState({alertVisible: v})}
            onConfirm={this.state.onAlertDone}
            onReject={this.state.onAlertReject}
            text={this.state.alertText}
            singleButton={this.state.alertSingleButton}
          />
          { notif.type === NOTIFS.OFFER_LINK &&
            <AcceptLinkDialog visible={this.state.acceptLinkVisible}
              setVisible={(acceptLinkVisible) => this.setState({acceptLinkVisible})}
              user={user}
            />
          }
        </View>
    )
  }
}

export default class Notifications extends Component {
  constructor(props) {
    super(props)

    this.state = {
      notifs: [],
      refresh: true,
      // scrollEnabled: true,
    }

    this.refreshData();
  }

  refreshData() {
    getNotifList((res, error) => {
      if (!error && res && this.state.refresh){
        // console.log('refresh')
        if (this.mounted){
          this.setState({notifs: res})
        } else {
          this.state.notifs = res
        }
      }
    })
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 1000);
  }
  
  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this._interval);
  }

  onRemoveItem(item) {
    let notifs = this.state.notifs.filter((n) => n !== item);
    this.setState({notifs, refresh: false})
    removeNotif(item.id, (res, error) => {
      this.setState({refresh: true})
    })
  }

  renderItem({item}) {
    return (
      <SwipeableRow
        onFinish={() => this.onRemoveItem(item)}
        // setScrollViewEnabled={scrollEnabled => this.setState({scrollEnabled})}
      >
        <NotifListElem notif={item} navigation={this.props.navigation} />
      </SwipeableRow>
    )
  }

  markAsRead() {
    setNotifRead((res, error) => {

    })
  }

  render() {
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        <View style={{
          flex: 1,
          borderRadius: 7,
          margin: 8,
          backgroundColor: colors.childCard.background,
          shadowColor: '#777',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.5,
          shadowRadius: 5,
          elevation: 5,
        }}>
          <FlatList 
            scrollEnabled={this.state.scrollEnabled}
            data={this.state.notifs}
            renderItem={this.renderItem.bind(this)}
          />
        </View>
        <NavigationEvents onDidBlur={this.markAsRead.bind(this)} />
      </ImageBackground>
    )
  }
}
