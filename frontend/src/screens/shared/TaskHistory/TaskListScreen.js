
import React, {Component} from 'react';
import { View, FlatList, TouchableHighlight, TouchableOpacity,
        TouchableWithoutFeedback, Image} from 'react-native';
import {NavigationEvents} from "react-navigation";
import Collapsible from 'react-native-collapsible';
import { Text, colors, TaskProgress } from 'common';
import { confirmTask } from 'backend/data';
import { AwardDialog } from 'common/dialogs';
import { ImageShowDialog, TextAlertDialog, PhotoDialog, RequestRemoveDialog, ProlongDialog } from '../../../common/dialogs';
import { remindTask, doTask } from '../../../backend/data';

import moment from 'moment';
import momentRu from 'moment/locale/ru';
moment.locale('ru');

class TaskListElem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dislikeVisible: false,
      likeVisible: false,
      imageDlgVisible: false,

      alertVisible: false,
      onAlertDone: undefined,
      alertText: '',
      alertSingleButton: false,

      photoDlgVisible: false,
      doDlgVisible: false,
      doDlgText: '',
      attachDlgVisible: false,
      prolongDlgVisible: false,
      removeDlgVisible: false,
    }
  }

  confirmAlert(task_id){
    this.setState({
      alertVisible: true,
      alertText: 'Подтвердить выполнение задания?',
      onAlertDone: () => this.confirm(task_id),
      alertSingleButton: false,
    })
  }

  confirm(task_id){
    confirmTask(task_id, (res, error) => {

    })
  }

  showAttachImage() {
    if (this.props.item.finish_image){
      this.setState({imageDlgVisible: true});
    }
  }

  remindAlert(task_id){
    this.setState({
      alertVisible: true,
      alertText: 'Напомнить о задании?',
      onAlertDone: () => this.remind(task_id),
      alertSingleButton: false,
    })
  }

  remind(task_id) {
    remindTask(task_id, (res, error) => {

    })
  }

  onDislike() {
    if (this.props.item.penalty_time){
      // moment.locale('ru');
      let time = moment.utc(this.props.item.penalty_time)
                       .fromNow();
                      
      let text = 'Вы уже оштрафовали за это задание ' + time;
      this.setState({
        alertVisible: true,
        alertText: text,
        onAlertDone: undefined,
        alertSingleButton: true,
      })
    } else {
      this.setState({dislikeVisible: true})
    }
  }

  onLike() {
    this.setState({likeVisible: true})  
  }

  onDoTask(){
    let text = 'Отметить задание как выполненное?'
    if (this.props.item.require_image) {
      text += ' Требуется фото-подтверждение';
    }
    this.setState({doDlgVisible: true, doDlgText: text});
  }

  onDoConfirm() {
    if (this.props.item.require_image) {
      this.setState({photoDlgVisible: true});
    }
    else {
      this.onDoSend()
    }
  }

  onDoSend(photo) {
    if (photo) {
      doTaskImage(this.props.item.id, photo.uri, (res, error) => {

      })
    }
    doTask(this.props.item.id, (res, error) => {

    })
  }

  onProlongTask() {
    let item = this.props.item;
    if (item.time_limit){
      this.setState({prolongDlgVisible: true})
    }
    else {
      this.setState({
        alertVisible: true,
        alertText: 'Для этого задания лимит времени не установлен',
        onAlertDone: undefined,
        alertSingleButton: true,
      })
    }
  }

  onDeleteTask() {
    this.setState({removeDlgVisible: true})
  }

  render() {
    let {user, item, mode} = this.props;

    return (
      <View style={{
          padding: 10, marginLeft: 10,
          marginRight: 10,
        }}
      >
        <TouchableOpacity
          onPress={() => this.props.onItemPress(item)}
        >
          <View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{flex: 1, fontSize: 14, color: 'black'}}>
                {item.title}
              </Text>
              <View style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                { item.require_image && 
                <TouchableOpacity onPress={this.showAttachImage.bind(this)}>
                  { item.finish_image && 
                    <Image source={require('res/icons/Attach_recieved.png')}
                      style={{width: 26, height: 26, resizeMode: 'contain', marginRight: 12}}
                    />
                  }
                  { !item.finish_image && 
                    <Image source={require('res/icons/Attach_not_send.png')}
                      style={{width: 26, height: 26, resizeMode: 'contain', marginRight: 12}}
                    />
                  }
                </TouchableOpacity>
                }
                {/* <IconAnt name='checksquareo' size={20} color={colors.checkbox.active} /> */}
                <Text style={{fontSize: 14, marginRight: 8}}>
                  {item.reward}
                </Text>
                <Image source={require('res/icons/Like.png')}
                  style={{
                    width: 23, height: 23,
                    marginRight: 10,
                  }}
                />
                { this.props.mode === 'undone' && (item.done ? 
                  <Image source={require('res/icons/CheckBoxConfirm.png')}
                    style={{width: 22, height: 22, resizeMode: 'contain'}}
                  />
                  : 
                  <Image source={require('res/icons/CheckBox.png')}
                    style={{width: 22, height: 22, resizeMode: 'contain'}}
                  />
                )}
              </View>
            </View>

            { this.props.mode === 'undone' && !!item.time_limit &&
              <View style={{flexDirection: 'row', marginTop: 5, alignItems: 'center'}}>
              
                <Text style={{marginLeft: 'auto'}}>
                  {moment.utc(item.time_limit).fromNow(true)}
                </Text>
                <Image source={require('res/icons/time.png')}
                  style={{width: 22, height: 22, resizeMode: 'contain', marginLeft: 10}}
                />
              
              </View>
            }
          </View>
        </TouchableOpacity>
        
        <Collapsible collapsed={this.props.openedId != item.id}>
          <View style={{paddingTop: 10}}>
            {!!item.desc &&
              <View style={{marginLeft: 10, marginRight: 10}}>
                <Text>{item.desc}</Text>
              </View>
            }
            <View style={{
              marginTop: 10,
              width: '100%',
              flexDirection: 'row',
            }}>
              { this.props.user.role === 'child' && this.props.mode !== 'undone' && 
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                  onPress={() => this.props.navigation.navigate('AddTask', {
                    repeat_task: item, child_id: user.id
                  })}
                >
                  {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                  <Image source={require('res/icons/Refresh.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Повторить</Text>
                </TouchableOpacity>
              }
              { this.props.user.role === 'child' && this.props.mode === 'undone' && 
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                  onPress={() => this.confirmAlert(item.id)}
                >
                  {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                  <Image source={require('res/icons/Confirm_grey.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Подтвердить</Text>
                </TouchableOpacity>
              }
              { this.props.user.role === 'child' && this.props.mode === 'undone' && 
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                  onPress={() => this.remindAlert(item.id)}
                >
                  {/* <IconMat style={{}} name='error-outline' size={60}/> */}
                  <Image source={require('res/icons/Remind.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Напомнить</Text>
                </TouchableOpacity>
              }
              { this.props.user.role === 'child' && this.props.mode !== 'undone' && 
                <TouchableOpacity
                  onPress={() => this.onLike()}
                  style={{flex: 1, alignItems: 'center'}}
                >
                  {/* <IconAnt style={{padding: 3}} name='minuscircleo' size={34}/> */}
                  <Image source={require('res/icons/Like_grey.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Наградить</Text>
                </TouchableOpacity>
              }
              { this.props.user.role === 'child' && 
                <TouchableOpacity
                  onPress={() => this.onDislike()}
                  style={{flex: 1, alignItems: 'center'}}
                >
                  {/* <IconAnt style={{padding: 3}} name='minuscircleo' size={34}/> */}
                  <Image source={require('res/icons/Dislike_grey.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Оштрафовать</Text>
                </TouchableOpacity>
              }
              { this.props.user.role === 'child' && this.props.mode === 'undone' && 
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                  onPress={() => this.props.navigation.navigate('AddTask',
                                  {task: item, child_i: undefined})}
                >
                  {/* <IconFeather style={{padding: 2}} name='x-circle' size={36}/> */}
                  <Image source={require('res/icons/Settings.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Изменить</Text>
                </TouchableOpacity>
              }

              { this.props.user.role === 'parent' && this.props.mode === 'undone' && 
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                  onPress={() => this.onDoTask()}
                >
                  {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                  <Image source={require('res/icons/Confirm_grey.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Выполнено</Text>
                </TouchableOpacity>
              }
              { this.props.user.role === 'parent' && this.props.mode === 'undone' && 
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                  onPress={() => this.onProlongTask()}
                >
                  {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                  <Image source={require('res/icons/time.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Продлить</Text>
                </TouchableOpacity>
              }
              { this.props.user.role === 'parent' && this.props.mode === 'undone' && 
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}}
                  onPress={() => this.onDeleteTask()}
                >
                  {/* <IconAnt style={{padding: 3}} name='checkcircleo' size={33}/> */}
                  <Image source={require('res/icons/delete.png')}
                    style={{width: 60, height: 50, resizeMode: 'contain'}}
                  />
                  <Text style={{fontSize: 9, marginTop: 5}}>Отменить</Text>
                </TouchableOpacity>
              }
            </View>
          </View>
        </Collapsible>
        <View style={{width: '100%', height: 2,
          backgroundColor: colors.separator,
          marginTop: 17,
        }}></View>
        <TextAlertDialog visible={this.state.doDlgVisible}
          setVisible={(v) => this.setState({doDlgVisible: v})}
          onConfirm={this.onDoConfirm.bind(this)}
          text={this.state.doDlgText}
        />
        <PhotoDialog visible={this.state.photoDlgVisible}
          setVisible={(v) => this.setState({photoDlgVisible: v})}
          setPhoto={this.onDoSend.bind(this)}
        />
        <TextAlertDialog visible={this.state.alertVisible}
          setVisible={(v) => this.setState({alertVisible: v})}
          onConfirm={this.state.onAlertDone}
          text={this.state.alertText}
          singleButton={this.state.alertSingleButton}
        />
        <AwardDialog visible={this.state.dislikeVisible}
          setVisible={(dislikeVisible) => this.setState({dislikeVisible})}
          child_id={item.child_id}
          like={false}
          task_id={item.id}
        />
        <AwardDialog visible={this.state.likeVisible}
          setVisible={(likeVisible) => this.setState({likeVisible})}
          child_id={item.child_id}
          like={true}
          task_id={item.id}
        />
        <ImageShowDialog visible={this.state.imageDlgVisible}
          setVisible={(imageDlgVisible) => this.setState({imageDlgVisible})}
          image={item.finish_image}
        />
        <ProlongDialog visible={this.state.prolongDlgVisible}
          setVisible={(v) => this.setState({prolongDlgVisible: v})}
          task={item}
        />
        <RequestRemoveDialog visible={this.state.removeDlgVisible}
          setVisible={(v) => this.setState({removeDlgVisible: v})}
          task={item}
        />
      </View>
    )
  }
}

export default class TaskListScreen extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      openedId: undefined,
      dislikeVisible: false,
    }
  }

  onItemPress = item => {
    console.log('collapse')
    this.setState({openedId: this.state.openedId !== item.id ? 
      item.id : undefined})
  }

  drawItem({item}) {
    let {mode} = this.props;
    let user = this.props.screenProps.item.user;
    return <TaskListElem
              user={user}
              item={item}
              mode={mode}
              onItemPress={this.onItemPress.bind(this)}
              openedId={this.state.openedId}
              navigation={this.props.navigation}
            />
  }

  getOpenParam(){
    let task_id = this.props.navigation.getParam('task_id')
    this.setState({
      openedId: task_id
    })
  }

  render() {
    let {mode} = this.props;
    let taskList = [];
    let user = {}
    if (this.props.screenProps.item){
      if (mode === 'done'){
        taskList = this.props.screenProps.item.done;
      } else if (mode === 'undone'){
        taskList = this.props.screenProps.item.undone;
      } else {
        taskList = this.props.screenProps.item.expired;
      }
      user = this.props.screenProps.item.user;
    }
    

    return (
      <View style={{ flex: 1}}>
        <FlatList style={{marginTop: 10}}
          data={taskList}
          renderItem = {this.drawItem.bind(this)}
          keyboardShouldPersistTaps="handled"
        />
        <AwardDialog visible={this.state.dislikeVisible}
          setVisible={(dislikeVisible) => this.setState({dislikeVisible})}
          child_id={user.id}
          like={false}
        />
        <NavigationEvents
          onDidFocus={this.getOpenParam.bind(this)}
        />
        
        {/* <TouchableHighlight
          style={{
            alignItems:'center',
            justifyContent:'center',
            width: 70,
            height: 70,
            borderRadius: 35,
            position: 'absolute', bottom: 30, right: 30,
          }}
          onPress={() => this.props.navigation.navigate('AddTask', {child_i: child_i})}
        >
          <Ionicon name='ios-add-circle' size={70} color='#f00' 
          />
        </TouchableHighlight> */}
      </View>
    );
  }
}