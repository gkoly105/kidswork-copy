import React, {Component} from 'react';
import { View, ImageBackground, Image, TouchableOpacity,
        ActivityIndicator } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import _ from 'lodash';
import {getChildDetails, imageUrl} from 'backend/data';
import { Text, colors, TaskProgress, GemIcon,
  LogoTitleBar, backImage, getAvatar, RoundImage } from './../../../common';
import TaskHistoryNavigator from './TaskHistoryNavigator';
import { getTaskHistory, ERRORS } from '../../../backend/data';
import { FlatList } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';

class UserListElem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {user} = this.props.item;

    let active = this.props.selected;
    return (
      <TouchableOpacity
        onPress={this.props.onItemPress}
        style={{
          marginLeft: 10,
          marginRight: 10,
          marginBottom: 10,
          alignItems: 'center',
        }}
      >
        <RoundImage
          style={{width: 100, height: 100, borderRadius: 50, marginTop: 4}}
          source={getAvatar(user.image, user.role, user.gender)}
        />
        { active && 
          <View
            style={{
              position: 'absolute',
              width: 108, height: 108, borderRadius: 54,
              borderWidth: 5, borderColor: colors.topBar,
            }}
          />

        }
        <Text style={{
          textAlign: 'center', marginTop: 6,
        }}>
          { user.display_name || user.name}
        </Text>
      </TouchableOpacity>
    )
  }
}

export default class TaskHistoryScreen extends React.Component {
  static router = TaskHistoryNavigator.router;

  static navigationOptions = ({ navigation, screenProps }) => ({
    header: null,
  });

  constructor(props){
    super(props)

    this.state = {
      activeTab: 0,
      selectedId: undefined,
      selectedIndex: 0,
      users: [],
      hasPerm: true,
    }

    this.refreshData();
  }

  getOpenParam(){
    let user_id = this.props.navigation.getParam('user_id')
    if (this.state.users.length > 0){
      this.setState({
        selectedIndex: _.findIndex(this.state.users, u => u.user.id === user_id) || 0,
      })
    }
    else {
      this.setState({
        selectedId: user_id,
      })
    }
  }

  refreshData() {
    getTaskHistory((res, error) => {
      if (!error && res){
        if (res.length === 0 && this.props.navigation.isFocused()){
          this.props.navigation.navigate('Family')
          return
        }
        res = res.map(item => _.assign(item, {key: ''+item.user.id}))
        if (this.mounted){
          this.setState({users: res})
          if (this.state.selectedId) {
            this.setState({
              selectedIndex: _.findIndex(this.state.users, u => u.user.id === user_id),
            })
          }
        } else {
          this.state.users = res
        }
        this.setState({hasPerm: true})
      } else if (error === ERRORS.PERM) {
        this.setState({hasPerm: false})
      }
    })
  }

  componentDidMount(){
    this.mounted = true;

    this._interval = setInterval(() => {
      this.refreshData()
    }, 3000);
  }
  
  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this._interval);
  }

  renderItem({item, index}) {
    return <UserListElem item={item}
      onItemPress={() => this.setState({selectedIndex: index})}
      selected={index === this.state.selectedIndex}
    />
  }

  render() {

    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
        <KeyboardAwareScrollView
          style={{ backgroundColor: colors.background,
            flex: 1,
            backgroundColor: 'transparent',
          }}
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{flex: 1}}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
        >
          <View style={{
            flex: 1,
            borderRadius: 7,
            margin: 8,
            backgroundColor: colors.childCard.background,
            shadowColor: '#777',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            elevation: 5,
          }}>
            { this.state.hasPerm && 
              <View style={{flex: 1}}>
                { this.state.users.length > 0 &&
                  <View 
                    style={{
                      padding: 14,
                      paddingLeft: 20, 
                      paddingRight: 20,
                    }}
                  >
                    <View style={{flexDirection: 'row',
                      alignItems: 'center',
                      alignSelf: 'center',
                      width: '100%',
                      marginTop: 10,
                    }}>
                      <FlatList
                        data={this.state.users}
                        renderItem={this.renderItem.bind(this)}
                        horizontal={true}
                      />
                    </View>    
                  </View>
                }
                { this.state.users.length > 0 &&
                  <TaskHistoryNavigator
                    navigation={this.props.navigation}
                    screenProps={{
                      item: this.state.users[this.state.selectedIndex],
                    }}
                  />
                }
                { !this.state.users.length &&
                  <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator/>
                  </View>
                }
              </View>
            }
            { !this.state.hasPerm &&
              <View style={{flex: 1, padding: 15, justifyContent: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                  Доступно только в платной версии
                </Text>
              </View>
            }
          </View>
        </KeyboardAwareScrollView>
        <NavigationEvents
          onDidFocus={this.getOpenParam.bind(this)}
        />
      </ImageBackground>
    );
  }
}