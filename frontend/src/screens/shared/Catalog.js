import React, {Component} from 'react';
import { View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { deleteAccount, contactSupport } from '../../backend/data';
import { logout } from '../../backend/auth';
import { Text, colors, styles, LogoTitleBar, backImage } from '../../common';
import { TextAlertDialog } from '../../common/dialogs';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';

export default class CatalogScreen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <ImageBackground source={backImage} style={{width: '100%', height: '100%'}}>
        <LogoTitleBar navigation={this.props.navigation} />
          <View style={{padding: 15, flex: 1, justifyContent: 'center'}}>
            <View>
              <Text style={{fontSize: 18, textAlign: 'center', marginBottom: 15}}>
                Скоро здесь будет каталог подарков
              </Text>
            </View>
          </View>
      </ImageBackground>
    )
  }
}
