import React from 'react';
import { View, AsyncStorage } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { NavigationActions, NavigationEvents } from 'react-navigation';
import { colors } from '../../common';

const slidesParent = [
  {
    key: '1',
    text: 'Пригласите ребенка в семью',
    textStyle: {height: '20%'},
    image: require('res/learn/AddUser.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '2',
    text: 'Нажмите на фото ребенка, чтобы открыть список заданий и подарков',
    textStyle: {height: '20%'},
    image: require('res/learn/ChildList.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '3',
    text: 'Создайте задание для ребенка',
    textStyle: {height: '20%'},
    image: require('res/learn/AddTask.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '4',
    text: 'Нажмите на задание для просмотра действий',
    textStyle: {height: '20%'},
    image: require('res/learn/OpenTask.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '5',
    text: 'Создайте подарок для ребенка',
    textStyle: {height: '20%'},
    image: require('res/learn/AddGift.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '6',
    text: 'Нажмите на подарок для просмотра действий',
    textStyle: {height: '20%'},
    image: require('res/learn/OpenGift.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '7',
    text: 'Наградите или оштрафуйте ребенка, чтобы изменить баланс',
    textStyle: {height: '20%'},
    image: require('res/learn/Award.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
];

const slidesChild = [
  {
    key: '1',
    text: 'Пригласите родителя в семью',
    textStyle: {height: '20%'},
    image: require('res/learn/AddUser.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '2',
    text: 'Нажмите на фото родителя, чтобы открыть список заданий и подарков',
    textStyle: {height: '20%'},
    image: require('res/learn/ParentList.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '3',
    text: 'Нажмите на кнопку "Заработать", чтобы попросить родителя придумать задание или предложить свое',
    textStyle: {height: '20%'},
    image: require('res/learn/RequestTask.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
  {
    key: '4',
    text: 'Нажмите на задание или подарок для просмотра действий',
    textStyle: {height: '20%'},
    image: require('res/learn/OpenTaskChild.jpg'),
    imageStyle: {width: '90%', height: '70%', resizeMode: 'contain', borderRadius: 15},
    backgroundColor: colors.topBar,
  },
];

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      slides: slidesParent,
      role: 'parent',
    }

    if (this.props.navigation.getParam('role') === 'child') {
      this.state.slides = slidesChild;
    }
  }

  onDone() {
    this.props.navigation.dispatch(NavigationActions.back())
    AsyncStorage.setItem('learned', '1');
  }

  onFocus() {
    if (this.slider){
      this.slider.goToSlide(0)
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <AppIntroSlider
          slides={this.state.slides}
          onDone={this.onDone.bind(this)}
          ref={el => this.slider = el}
          showSkipButton
          nextLabel='Далее'
          skipLabel='Пропустить'
          doneLabel='Готово'
          buttonTextStyle={{fontSize: 12}}
        />
        <NavigationEvents onDidFocus={this.onFocus.bind(this)} />
      </View>
    )
  }
}