import {hostAddr, ERRORS, getError, fetchTimeout} from './data';


export function login(login, password, callback){
  console.log('login')
  fetchTimeout(hostAddr + '/api/login', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify({
      login: login,
      password: password,
    })
  })
  .then(resp => {
    console.log('login', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}

export function sendRegSMS(login, password, callback){
  console.log('send reg sms')
  fetchTimeout(hostAddr + '/api/send_reg_sms', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify({
      login: login,
      password: password,
    })
  })
  .then(resp => {
    console.log('send reg sms resp', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}

export function sendPasswordSMS(phone, callback){
  console.log('send password sms')
  fetchTimeout(hostAddr + '/api/send_password_sms', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify({
      phone: phone,
    })
  })
  .then(resp => {
    console.log('send password sms resp', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}

export function confirmReg(login, password, token, callback){
  console.log('confirm reg sms')
  fetchTimeout(hostAddr + '/api/confirm_reg', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify({
      login: login,
      password: password,
      token: token,
    })
  })
  .then(resp => {
    console.log('confirm reg resp', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}

export function resetPassword(phone, password, token, callback){
  console.log('reset password sms')
  fetchTimeout(hostAddr + '/api/reset_password', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify({
      phone: phone,
      password: password,
      token: token,
    })
  })
  .then(resp => {
    console.log('reset password resp', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}

export function checkAuth(callback){
  // console.log('check auth')
  // Заглушка
  // callback(true)
  // return
  /////////////////////

  fetchTimeout(hostAddr + '/api/check_auth', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    // console.log('check auth resp', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    // console.log('error', error)
    callback(undefined, error);
  })
}

export function logout(callback){
  console.log('logout')
  fetchTimeout(hostAddr + '/api/logout', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    console.log('check auth', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    if (callback)
      callback(res.status == 'ok');
  })
  .catch(error => {
    console.log('error', error)
    if (callback)
    {
      error = getError(error);
      callback(false, error);
    }
  })
}

export function parentReg(data, photoUri, callback){
  console.log('reg')

  let jsondata = JSON.stringify(data);

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetchTimeout(hostAddr + '/api/parent_reg', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
  .then(resp => {
    console.log('reg', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}