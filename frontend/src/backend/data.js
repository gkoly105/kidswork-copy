
import { getFcmToken } from "./PushNotifs";
import {Platform} from 'react-native';

const hostAddrPlace = 'https://api.jsonbin.io/b/5c65475aad5128320afb8221/latest'
const hostAddrPlaceKey = '$2a$10$dyqAWxQNlPmQai0lVhox6.xYWeJqop3WLPzz1aOJVUemevktXsVGW';
export var hostAddr = 'https://kidswork.ru'
const reqTimeout = 7000;

export const LINKS = {
  AGREEMENT: 'https://kidswork.ru/legal/agreement',
  RULES: 'https://kidswork.ru/legal/terms',
  POLICY: 'https://kidswork.ru/legal/policy',
  LICENCE: 'https://kidswork.ru/legal/licence',
  ANDROID: 'market://details?id=ru.kidswork',
  IOS: 'https://apps.apple.com/us/app/kidswork/id1470400237',
  ANDROID_WEB: 'https://play.google.com/store/apps/details?id=ru.kidswork',
  IOS_WEB: 'https://apps.apple.com/us/app/kidswork/id1470400237',
}

export const getStoreLink = () => {
  return Platform.select({
    android: LINKS.ANDROID,
    ios: LINKS.IOS,
  })
}

export const SHARE_TEXT = "Вы получили приглашение в приложение KidsWork! \n" +
                          "Android - " + LINKS.ANDROID + '\n' + 
                          "iPhone - " + LINKS.IOS;

const API_VERSION = 4;

export const ERRORS = {
    NETWORK: 'Network error',
    TIMEOUT: 'Timeout error',
    REQFAILED: 'Network request failed',
    NOTFOUND: 'Not found',
    METHOD: 'Invalid method',
    FORMAT: 'Invalid format',
    ARGS: 'Invalid args',
    CREDS: 'Invalid credentials',
    AUTH: 'Auth required',
    PERM: 'Permission denied',
    NEEDUPDATE: 'Need update',
}

export const NOTIFS = {
  C_ADD_TASK: 1,
  C_EDIT_TASK: 2,
  C_REMIND_TASK: 3,
  C_DELETE_TASK: 4,
  C_CONFIRM_TASK: 5,
  C_ADD_GIFT: 6,
  C_EDIT_GIFT: 7,
  C_DELETE_GIFT: 8,
  C_CONFIRM_GIFT: 9,
  P_DO_TASK: 10,
  P_BUY_GIFT: 11,
  P_OFFER_TASK: 12,
  C_REJECT_TASK: 13,
  P_REQUEST_TASK: 14,
  C_AWARD: 15,
  C_PENALTY: 16,
  P_OFFER_GIFT: 17,
  C_REJECT_GIFT: 18,
  P_PROLONG_TASK: 19,
  C_PROLONG_CONFIRM: 20,
  C_PROLONG_REJECT: 21,
  P_REMOVE_TASK: 22,
  MESSAGE: 23,
  TASK_EXPIRED: 24,
  OFFER_LINK: 25,
  ACCEPT_LINK: 26,
  REJECT_LINK: 27,
}

export function getError(error){
    if (error.message == ERRORS.TIMEOUT ||
        error.message == ERRORS.REQFAILED){
        return ERRORS.NETWORK
    }
    return error.message
}

export function fetchTimeout(url, options, timeout = reqTimeout) {
    return Promise.race([
        fetch(url, options),
        new Promise((_, reject) =>
            setTimeout(() => reject(new Error('Timeout error')), timeout)
        )
    ]);
}

export function fetchHostAddr(callback){
  console.log('fetch host')
  if (!hostAddr){
    fetchTimeout(hostAddrPlace, {
        headers: {
            'secret-key': hostAddrPlaceKey,
        }
    })
    .then(resp => resp.json())
    .then(res => {
        hostAddr = res.host
        console.log(res)
        callback(true)
    })
    .catch(error => {
      console.log("Unable to fetch host addr!")
      callback(false, error)
    })
  }
  else{
    console.log('using local host addr')
    callback(true)
  }
}

export function imageUrl(path){
    return hostAddr + '/api' + path;
}

export function checkApiVersion(callback) {
  // console.log('check API version')
  fetchTimeout(hostAddr + '/api/api_info', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    console.log('check API version resp', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    if (res.status !== 'ok'){
      callback(undefined, res.reason)
    }
    else if (res.data.version > API_VERSION){
      callback(undefined, ERRORS.NEEDUPDATE);
    }
    else {
      console.log('API version is OK');
      callback(true, undefined);
    }
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    // console.log('error', error)
    callback(undefined, error);
  })
}

export function getProfile(callback) {
  // console.log('get profile')
  fetchTimeout(hostAddr + '/api/profile', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      // console.log('get profile resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function getChildList(callback) {
    // console.log('get child list')
    fetchTimeout(hostAddr + '/api/child_list', {
        method: 'GET',
        credentials: 'same-origin',
      })
      .then(resp => {
        // console.log('get child list resp', resp);
        return resp.json();
      })
      .then(res => {
        // console.log('resp json', res)
        callback(res.data, res.reason);
      })
      .catch(error => {
        // console.log('error', error)
        error = getError(error);
        // console.log('error', error)
        callback(undefined, error);
      })
}

export function getParentList(callback) {
  // console.log('get parent list')
  fetchTimeout(hostAddr + '/api/parent_list', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      // console.log('get parent list resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function addTask(data, callback) {
  // console.log('add task')
    fetchTimeout(hostAddr + '/api/add_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('add task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function offerTask(data, callback) {
  // console.log('offer task')
    fetchTimeout(hostAddr + '/api/offer_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('offer task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function offerGift(data, photoUri, callback) {
  console.log('offer gift')
  let jsondata = JSON.stringify(data);

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetchTimeout(hostAddr + '/api/offer_gift', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
    .then(resp => {
      console.log('offer gift resp', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(false, error);
    })
}

export function editTask(data, callback) {
  // console.log('edit task')
    fetchTimeout(hostAddr + '/api/edit_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('edit task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function requestRemoveTask(data, callback) {
  // console.log('edit task')
    fetchTimeout(hostAddr + '/api/request_remove_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('edit task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function requestProlongTask(data, callback) {
  console.log('req prolong task')
    fetchTimeout(hostAddr + '/api/request_prolong_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      console.log('req prolong task resp', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(false, error);
    })
}

export function removeTask(task_id, callback) {
  // console.log('remove task')

  let data = {task_id}

    fetchTimeout(hostAddr + '/api/remove_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('remove task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function requestTask(parent_id, callback) {
  // console.log('request task')

  let data = {parent_id}

    fetchTimeout(hostAddr + '/api/request_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('request task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function rejectTask(notif_id, callback) {
  // console.log('reject task')

  let data = {notif_id}

    fetchTimeout(hostAddr + '/api/reject_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('reject task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function prolongConfirm(notif_id, callback) {
  console.log('prolong confirm task')

  let data = {notif_id}

    fetchTimeout(hostAddr + '/api/prolong_confirm', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      console.log('prolong confirm resp', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(false, error);
    })
}

export function prolongReject(notif_id, callback) {
  console.log('rprolong eject')

  let data = {notif_id}

    fetchTimeout(hostAddr + '/api/prolong_reject', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      console.log('prolong reject resp', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(false, error);
    })
}

export function rejectGift(notif_id, callback) {
  // console.log('reject gift')

  let data = {notif_id}

    fetchTimeout(hostAddr + '/api/reject_gift', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('reject gift resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function remindTask(task_id, callback) {
  console.log('remind task')

  let data = {task_id}

  fetchTimeout(hostAddr + '/api/remind_task', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify(data),
  })
  .then(resp => {
    console.log('remind task resp', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.status == 'ok', res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    console.log('error', error)
    callback(false, error);
  })
}

export function addGift(data, photoUri, callback) {
  console.log('add task')
  let jsondata = JSON.stringify(data);

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetchTimeout(hostAddr + '/api/add_gift', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
    .then(resp => {
      console.log('add task resp', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(false, error);
    })
}

export function editGift(data, photoUri, callback) {
  // console.log('edit gift')
  let jsondata = JSON.stringify(data);

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetchTimeout(hostAddr + '/api/edit_gift', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
    .then(resp => {
      // console.log('edit gift resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function removeGift(gift_id, callback) {
  // console.log('remove gift')

  let data = {gift_id}

    fetchTimeout(hostAddr + '/api/remove_gift', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('remove gift resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function getChildData(callback) {
  // console.log('get child data')
  fetchTimeout(hostAddr + '/api/child_data', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      // console.log('get child data resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}


export function getChat(user_id, callback) {
  // console.log('get chat')

  let data = {'user_id': user_id}

  fetchTimeout(hostAddr + '/api/chat', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('get chat res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function setRead(user_id, callback) {
  // console.log('set read')

  let data = {'user_id': user_id}

  fetchTimeout(hostAddr + '/api/set_read', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('get chat res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function getChatList(callback) {
  // console.log('chat list')
  fetchTimeout(hostAddr + '/api/chat_list', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      // console.log('chat list resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function sendMessage(user_id, text, callback) {
  // console.log('send message')

  let data = {'user_id': user_id, text: text}

  fetchTimeout(hostAddr + '/api/send_message', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('send mess res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}


export function doTask(task_id, callback) {
  // console.log('do task')

  let data = {task_id: task_id}

  fetchTimeout(hostAddr + '/api/do_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('do task res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function doTaskImage(task_id, photoUri, callback){
  // console.log('do task image')

  let jsondata = JSON.stringify({task_id});

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetchTimeout(hostAddr + '/api/do_task_image', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
  .then(resp => {
    // console.log('do task image', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}

export function buyGift(gift_id, callback) {
  // console.log('buy gift')

  let data = {gift_id: gift_id}

  fetchTimeout(hostAddr + '/api/buy_gift', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('buy gift res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function confirmTask(task_id, callback) {
  console.log('confirm task')

  let data = {task_id: task_id}

  fetchTimeout(hostAddr + '/api/confirm_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      console.log('confirm task res', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(undefined, error);
    })
}

export function confirmGift(gift_id, callback) {
  // console.log('confirm gift')

  let data = {gift_id: gift_id}

  fetchTimeout(hostAddr + '/api/confirm_gift', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('confirm gift res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function getFamily(callback) {
  // console.log('get family')
  fetchTimeout(hostAddr + '/api/family', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      // console.log('get family resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function findUser(phone, callback) {
  // console.log('find user')

  let data = {phone: phone}

  fetchTimeout(hostAddr + '/api/find_user', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('find user res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function offerLinkUser(role, user_id, callback) {
  // console.log('link user')

  let data = {role: role, user_id: user_id}

  fetchTimeout(hostAddr + '/api/offer_link_user', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('link user res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function rejectLinkUser(role, user_id, callback) {
  // console.log('link user')

  let data = {role: role, user_id: user_id}

  fetchTimeout(hostAddr + '/api/reject_link_user', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('link user res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function linkUser(role, user_id, callback) {
  // console.log('link user')

  let data = {role: role, user_id: user_id}

  fetchTimeout(hostAddr + '/api/link_user', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('link user res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function unlinkUser(user_id, callback) {
  // console.log('unlink user')

  let data = {user_id: user_id}

  fetchTimeout(hostAddr + '/api/unlink_user', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('unlink user res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function awardChild(child_id, amount, task_id, callback) {
  // console.log('award child')

  let data = {child_id, amount, task_id}

  fetchTimeout(hostAddr + '/api/award', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('award child res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function penaltyChild(child_id, amount, task_id, callback) {
  // console.log('penalty child')

  let data = {child_id, amount}
  if (task_id !== undefined) {
    data.task_id = task_id;
  }

  fetchTimeout(hostAddr + '/api/penalty', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('penalty child res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function editProfile(data, photoUri, callback){
  // console.log('edit_profile')

  let jsondata = JSON.stringify(data);

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetchTimeout(hostAddr + '/api/edit_profile', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
  .then(resp => {
    // console.log('edit profile', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    callback(undefined, error);
  })
}

export function setDisplayPrefs(user_id, name, callback) {
  // console.log('set display prefs')

  let data = {user_id, name}

  fetchTimeout(hostAddr + '/api/set_display_prefs', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify(data),
  })
  .then(resp => {
    // console.log('set display prefs res', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    // console.log('error', error)
    callback(undefined, error);
  })
}

export function deleteAccount(callback) {
  // console.log('delete account')
  fetchTimeout(hostAddr + '/api/delete_account', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      // console.log('delete account resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function getNotifList(callback) {
  // console.log('get notifs list')
  fetchTimeout(hostAddr + '/api/notif_list', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    // console.log('get notif list resp', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    // console.log('error', error)
    callback(undefined, error);
  })
}

export function setNotifRead(callback) {
  // console.log('get child data')
  fetchTimeout(hostAddr + '/api/set_notif_read', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      // console.log('get child data resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function getNotifCount(callback) {
  // console.log('get notifs list')
  fetchTimeout(hostAddr + '/api/notif_count', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    // console.log('get notif list resp', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    // console.log('error', error)
    callback(undefined, error);
  })
}

export function contactSupport(text, callback) {
  console.log('contact support')

  let data = {text}

  fetchTimeout(hostAddr + '/api/contact_support', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      console.log('contact support res', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(undefined, error);
    })
}

export function removeNotif(notif_id, callback) {
  // console.log('remove notif')

  let data = {notif_id}

    fetchTimeout(hostAddr + '/api/remove_notif', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('remove notif resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function getTaskHistory(callback) {
  console.log('get task history')
  fetchTimeout(hostAddr + '/api/task_history', {
      method: 'GET',
      credentials: 'same-origin',
    })
    .then(resp => {
      console.log('get task history resp', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(undefined, error);
    })
}

export function addFcmDevice(callback) {
  // console.log('remove task')
  getFcmToken().then(token => {
    if (token){
      
      let data = {token}

      fetchTimeout(hostAddr + '/api/add_fcm_device', {
        method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify(data),
      })
      .then(resp => {
        // console.log('remove task resp', resp);
        return resp.json();
      })
      .then(res => {
        // console.log('resp json', res)
        if (callback){
          callback(res.data, res.reason);
        }
      })
      .catch(error => {
        // console.log('error', error)
        error = getError(error);
        // console.log('error', error)
        if (callback){
          callback(undefined, error);
        }
      })
    }
  })
}

export function removeFcmDevice(callback) {
  // console.log('remove task')
  getFcmToken().then(token => {
    if (token){
      
      let data = {token}

      fetchTimeout(hostAddr + '/api/remove_fcm_device', {
        method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify(data),
      })
      .then(resp => {
        // console.log('remove task resp', resp);
        return resp.json();
      })
      .then(res => {
        // console.log('resp json', res)
        if (callback){
          callback(res.data, res.reason);
        }
      })
      .catch(error => {
        // console.log('error', error)
        error = getError(error);
        // console.log('error', error)
        if (callback){
          callback(undefined, error);
        }
      })
    }
  })
}

export function getPerms(callback) {
  console.log('get perms')
  fetchTimeout(hostAddr + '/api/get_perms', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    console.log('get perms resp', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    console.log('error', error)
    callback(undefined, error);
  })
}

// Purchases

export function setSub(type, start, callback) {
  console.log('set sub')

  let data = {type, start}

  fetchTimeout(hostAddr + '/api/set_sub', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify(data),
  })
  .then(resp => {
    console.log('set sub res', resp);
    return resp.json();
  })
  .then(res => {
    console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    console.log('error', error)
    error = getError(error);
    console.log('error', error)
    callback(undefined, error);
  })
}
