import React, {Component} from 'react';
import { AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';

export class NotifReciever extends Component {
  constructor(props) {
    super(props)
  }

  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();
  }

  componentWillUnmount() {
    if (this.notificationListener){
      this.notificationListener();
    }
    if (this.notificationOpenedListener){
      this.notificationOpenedListener();
    }
  }
  
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      let token = await getFcmToken();
      console.log('fcm token', token);
    } else {
      this.requestPermission();
    }
  }
  
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      let token = await getFcmToken();
      console.log('fcm token', token);
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const notif = notification;
      
    });
  
    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const notif = notificationOpen.notification;
        
    });
  
    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const notif = notificationOpen.notification;
      this.props.navigation.navigate('Notifications');
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      // console.log(JSON.stringify(message));
    });
  }  

  render() {
    return null;
  }
}

export const getFcmToken = async () => {
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  if (!fcmToken) {
    fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      // user has a device token
      await AsyncStorage.setItem('fcmToken', fcmToken);
      return fcmToken;
    }
  }
  else {
    return fcmToken;
  }
}
